/** TP3-9- Crear los siguientes módulos
a) Calcule los valores de mantisa y exponente de un número real para expresarlo 
en notación punto flotante normalizado.
b) Dada una mantisa normalizada y un exponente entero calcule el número real en 
notación punto fijo. ¿Existe una función pre definida en alguna librería de C 
que pueda ser usada para resolver esta tarea? En el caso de existir y utilizarla 
¿Es necesario construir este módulo?
Utilizando los módulos desarrollados en a) y b) realizar un programa que a través de un menú de
opciones permita al usuario obtener el valor normalizado de un número real u obtener el valor en
punto fijo de una mantisa normalizada y un exponente.   **/

#include <stdio.h>
#include <math.h>

int menu(void);
void RealANot(float,float*,int*);   /* entrada real, mantisa, exponente */
float NotAReal(float,int);

int main(void)
{
	float man, real, maymant, menmant, aux1;
	int expo, n, elecc, i, mayexp, menexp, aux2;
	do
	{
		elecc=menu();
		switch(elecc)
		{
			case 1 :
				printf("\n Se convertira un numero desde punto fijo a notacion cientifica:");
				printf("\n Ingrese un numero real positivo:  ");
				scanf("%f",&real);
				RealANot(real, &man, &expo);
				printf("\n Mantisa:   %f  Exponente:   %d",man,expo);
			break;
			case 2:
				printf("\n Se convertira un numero en notacion cientifica a uno de punto fijo:");
				do  /* chequeo de que la mantiza esta normalizada */
				{
					printf("\n Ingrese la mantiza:   (1<=mantiza<10)\n   ");
					scanf("%f",&real);
				}while(real>9 || real<1);
				printf("\n Ingrese el exponente:");
				scanf("%d",&expo);
				printf("\n El numero convertido es %f",NotAReal(real,expo));
			break;
			case 3:
				printf("\n Se mostrara el mayor y menor numero de una lista de positivos.\n");
				printf("Finalmente se mostrara el promedio de ambos");
				printf("\nIngrese el tamanho de lista:    ");
				scanf("%d",&n);
				printf("\n Ingrese los datos:\n");
				scanf("%f",&real);
				RealANot(real,&menmant,&menexp);  /* toma la primer entrada como el mayor y el menor */
				RealANot(real,&maymant,&mayexp);
				for(i=1; i<=n-1; i++)
				{
					scanf("%f",&real);
					RealANot(real,&aux1,&aux2);  /* aux1 = mantiza, aux2 = exp */
					if(aux2<=menexp)
					{
						if(aux2==menexp)
							if(aux1<menmant)
							{
								menmant=aux1;
								menexp=aux2;
							}
						if(aux2<menexp)
						{
							menmant=aux1;
							menexp=aux2;
						}
					}
					if(aux2>=mayexp)
					{
						if(aux2==mayexp)
						if(aux1>maymant)
						{
							maymant=aux1;
							mayexp=aux2;
						}
						if(aux2>mayexp)
						{
							maymant=aux1;
							mayexp=aux2;
						}
					}
				}
				printf("\n El menor numero ingresado es %f",NotAReal(menmant,menexp));
				printf("\n El mayor numero ingresado es %f",NotAReal(maymant,mayexp));
				printf("\n El promedio entre ellos es %f",(NotAReal(menmant,menexp)+NotAReal(maymant,mayexp))/2);
			break;
			case 0:
				printf("\n <> Fin del Programa <>\n");
			break;
			default:
				printf("\n¡! Opción No Correcta \n");
		} /*fin switch*/
    }
    while(elecc);    /*fin do*/
    return 0;
}
int menu(void)
{
	int rta, band = 0;
	do
	{
		if(band==1) printf("\n * !! Opcion fuera de rango !! * \n");
		printf("\n\n <1> Primer modulo / Real a Not Cient\n");
		printf(" <2> Segundo modulo / Not Cient a Real\n");
		printf(" <3> Programa principal\n");
		printf(" - -  - - -\n");
		printf("-- - - -- - -- \n");
		printf(" <0> SALIR\n");
		printf("Su opcion: ");
		scanf("%d",&rta);
		band = 1;
	}while(rta < 0 || rta > 4);
	return rta;
}
void RealANot(float r, float *m, int *e)  /* el mismo exponente hace de contador */
{
	*e = 0;
	while(r > 9)
	{
		r = r / 10;  /* No hay problema en modificar r porque es local, luego se lo asigna a *m */
		*e = *e + 1;
	}
	while(r<1)
	{
		r = r * 10;
		*e = *e - 1;
	}
	*m=r;
}
float NotAReal(float m,int e)
{
	return m * (pow(10,e));
}