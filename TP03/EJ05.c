/** TP3-5- Crear un módulo que manipule dos números enteros suprimiendo la última cifra del
primero y añadiéndola al final del segundo. Realizar un programa que utilice dicho módulo para
invertir un número.  **/
#include <stdio.h>

void swapDigit(int*, int*);

int main(void) {
	int entero, b = 0;
	printf("Ingrese a: ");
	scanf("%d",&entero);
	while(entero != 0)
		swapDigit(&entero,&b);
	printf("\nEl numero invertido es: %d",b);
	
	return 0;
}
/* modulo que 1- agrega un 0 a la derecha al receptor 2- sumarle al receptor el resto de 10
del donante 3- acortar donante por derecha en un digito*/
void swapDigit(int* donante, int* receptor)
{
	*receptor *= 10;
	*receptor += (*donante % 10);
	*donante /= 10;
}