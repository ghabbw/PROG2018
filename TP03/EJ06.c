/** TP3-6- Desarrollar los siguientes módulos:
a) Crear un módulo que determine la cantidad de divisores positivos de un número natural (sin contar
los divisores triviales) ¿En qué tipo de problemas podría utilizar este módulo? Brinde un ejemplo.
b) Crear un módulo que dado un número natural NUM en base 10 y una base B < 10 calcule el
equivalente de NUM en base B.
Realizar un programa utilizando los módulos anteriores y reutilizando el módulo de determinación de
primo trabajado en el ejercicio 3, en el que para un número natural, calcule su equivalente a base 2 si
la cantidad de divisores del número es primo y calcule su equivalente a base 9 si la cantidad de
divisores no es primo.
Importante: al reutilizar el módulo del inciso a) tener en cuenta que el valor retornado no contempla
todos los divisores del número y esto podría afectar la resolución del problema planteado. **/

#include <stdio.h>

int divSinTrivial(int);
int cambiobase(int, int);
int primo(int);

int main(void)
{
	int nat, cantDiv;
	
	printf("\n Ingrese los numeros naturales a procesar:   (distintos de cero)\n");
	scanf("%d",&nat);
	while(nat > 0)
	{
		if(nat == 1)  
			cantDiv = 1 + divSinTrivial(nat); /* entrada = 1 tiene un solo divisor*/
		else
			cantDiv = 2 + divSinTrivial(nat); /* entrada > 1 tiene 2 divisores mas (triviales) */
		if(primo(cantDiv))
			printf("\n Su equivalente en base 2 es %d:",cambiobase(2,nat));
		else
			printf("\n Su equivalente en base 9 es %d:",cambiobase(9,nat));
		printf("\n Ingrese los numeros naturales a procesar:   (distintos de cero)\n");
		scanf("%d",&nat);
	}

    return 0;
}
int divSinTrivial(int num)
{
	int div = 2, cont = 0;
	while(div <= num / 2)
	{
		if(num % div == 0)
			cont++;
		div++;
	}
	return cont;
}
int cambiobase(int base,int num)
{
	int salida, dig, e;
	salida = 0;
	e = 1;
	while(num != 0)
	{
		dig = num % base;
		salida += (dig * e);
		e *= 10;
		num /= base;
	}
	return salida;
}
int primo(int num)
{
	int pd;
	pd = 2;
	while((pd <= num / 2) && (num % pd != 0))
		pd++;
	if((pd > num / 2) && (num != 1))
		return 1;
	else
		return 0;
}

