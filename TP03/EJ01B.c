/** TP3-1- Para cada problema, crear un programa utilizando la libreria <math.h>
    b) Dados N numeros reales para cada uno de ellos calcular la parte entera del numero.
    Si la parte entera resulta ser par (y no cero) entonces crear un nuevo numero
    calculando la potencia del numero ingresado elevado al primer digito de la parte decimal.
    Cuando la parte entera no sea par, crear un nuevo numero calculando la raiz cuadrada
    de la parte entera. Si la parte entera resulta ser cero, crear un nuevo numero
    calculando el algoritmo del numero ingresado. Mostrar cada numero generado indicando
    el tipo de transformacion.  **/

#include <stdio.h>
#include <math.h>

int main(void)
{
	float entrd;
	int N, i;
	printf("\n Ingrese la cantidad");
	fflush(stdin);
	scanf("%d",&N);
	for(i = 1; i <= N; i++)
	{
		printf("\n Ingrese el numero real:   ");
		fflush(stdin);
		scanf("%f",&entrd);
		printf("\n %f",trunc(entrd));
	}
	
	return 0;
}