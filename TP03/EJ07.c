/**  TP3-7-  Desarrollar los siguientes módulos:
1. Dados dos números naturales determine si éstos son amigos.
Los números son amigos, si cada uno de ellos es igual a la suma de los divisores del otro
(tener en cuenta que no se debe tomar al mismo número como divisor). Por ejemplo, 220 y
284 son amigos, ya que:
Suma de divisores de 284: 1 + 2 + 4 + 71 + 142 = 220
Suma de divisores de 220: 1 + 2 + 4 + 5 + 10 + 11 + 20 + 22 + 44 + 55 + 110 = 284
Nota: Si para la solución de este problema detecta alguna otra tarea que deba ser un módulo
debe implementarse.
2. Dado un número natural determine si es un número deficiente, abundante o perfecto.
Un número natural N se dice que es deficiente si la suma de sus divisores (sin contarse a sí
mismo) es menor a N. Si la suma de sus divisores es mayor a N se dice que el número es
abundante y si es igual a N se dice que es perfecto.
Realizar un programa que genere una lista de N números aleatorios en el rango [A, B] (con A y B
naturales) y muestre aquellos números aleatorios que cumplan con no ser deficientes y posean un
número amigo en el intervalo [A, B].   **/

#include <stdio.h>
#include <stdlib.h>

int menu(void);
int detamigos(int, int);
int sumadiv(int);
int DefAbuPer(int);

int main(void)
{
	int N, A, B, i, numr, amig, div, band, elecc;
	srand(time(NULL));
	do
	{
		elecc=menu();
		switch(elecc)
		{
		case 1 :
			printf("\n Determinar si dos numeros son amigos");
			printf("\n Ingrese el primer numero:");
			scanf("%d",&A);
			printf("\n Ingrese el segundo numero:");
			scanf("%d",&B);
			if(detamigos(A,B) != 0)
				printf("\n A (%d) y B (%d) son amigos",A,B);
			else
				printf("\n A (%d) y B (%d) NO son amigos",A,B);
		break;
		case 2:
			printf("ingrese un numero a evaluar: ");
			scanf("%d",&N);
			if(DefAbuPer(N) == 1)
				printf("\n El numero es deficiente");
			if(DefAbuPer(N) == 2)
				printf("\n El numero es abudante");
			if(DefAbuPer(N) == 3)
				printf("\n El numero es perfecto");
			break;
		case 3:
			printf("\n Ingrese cantidad de numeros: ");
			scanf("%d",&N);
			printf(" Ingrese A: ");
			scanf("%d",&A);
			printf(" Ingrese B: ");
			scanf("%d",&B);
			band=0;
			for(i=1; i<=N; i++)
			{
				numr = rand() % (B - A + 1) + A;
				div = sumadiv(numr);
				amig = detamigos(numr,div);
				if(amig)
					if((amig >= A) && (amig <= B))
					{
						printf("\n %d tiene un numero amigo dentro del rango",numr);
						band=1;
					}
			}
			if(band == 0)
				printf("\n Ningun numero tiene amigos en el intervalo");
		break;
		case 0:
			printf("\n <> Fin del Programa <>\n");
			break;
		default:
		printf("\n¡! Opción No Correcta \n");
		} /*fin switch*/
	}while(elecc);    /*fin do*/
	return 0;
}

int menu(void)
{
	int rta, band = 0;
	do
	{
		if(band == 1)
			printf("\n * !! Opcion fuera de rango !! * \n");
		printf("\n\n <1> Primer modulo // Determinar si 2 nums son amigos\n");
		printf(" <2> Segundo modulo // Det. si un num. es deficiente, abundante o perfecto\n");
		printf(" <3> Programa principal\n");
		printf(" - -  - - -\n");
		printf("-- - - -- - -- \n");
		printf(" <0> SALIR\n");
		printf("Su opcion: ");
		scanf("%d",&rta);
		band=1;
	}while(rta < 0 || rta > 4);
	return rta;
}
int detamigos(int A, int B)
{
	int DivisoresDeA, DivisoresDeB;
	DivisoresDeA = sumadiv(A);
	DivisoresDeB = sumadiv(B);
	if(DivisoresDeA > A)
		if((DivisoresDeA == B) && (DivisoresDeB == A))
			return DivisoresDeA;
		else
			return 0;
	else
		return 0;
}
int sumadiv(int num)
{
	int acu = 0, i;
	for(i = 1; i <= num / 2; i++)
		if(num % i == 0)
			acu += i;
	return acu;
}
int DefAbuPer(int n)
{
	if(sumadiv(n) <n )
		return 1;
	if(sumadiv(n) >n )
		return 2;
	if(sumadiv(n) == n)
		return 3;
}
