/**  TP3-10- Realizar un programa que, para cada una de las N fechas ingresadas por el usuario,
genere una fecha al azar mayor a esta y luego indique la cantidad de días promedio entre cada una
las fechas ingresadas y la fecha generada al azar.  **/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void fecharandom(int*,int*,int*);
void ingresodefecha(int*,int*,int*);
long int calc2f(int,int,int,int,int,int);
int diarandom();

int main(void)
{
	float prom;
	int i, N, acum, diaR, mesR, anhoR, dia, mes, anho;
	srand(time(NULL));
	/* sirve para hacer pruebas (comentando fecharandom()):
	diaR=1;
	mesR=1;
	anhoR=2001;*/
	fecharandom(&diaR, &mesR, &anhoR);
	acum = 0;
	printf("\n\n  Fecha generada al azar (1<anho<2500)... (%d / %d / %d) \n",diaR,mesR,anhoR);
	do  /* do:Evita que N sea 0 como divisor */
	{
		printf("\n Cantidad de fechas a computar...        **Distinta de cero**\n");
		fflush(stdin);
		scanf("%d",&N);
	}while(N == 0);
	printf("\n Ingrese una fecha\n");
	for(i = 1; i <= N; i++)
	{
		ingresodefecha(&dia,&mes,&anho);
		acum = acum + abs(calc2f(dia,diaR,mes,mesR,anho,anhoR));  /* saco valor absoluto para acumular */
	}
	prom = acum / (float)N;   /* convierto a float para sacar promedio */
	printf("\n El promedio de dias es:  %f ",prom);
	return 0;
}

/** A) MODULO QUE DA FECHA AL AZAR **/

void fecharandom(int *diaR, int *mesR, int *anhoR)
{
	*mesR=rand() % (12-1+1) + 1;  /* Da un mes random */
	switch(*mesR)
	{
		case 1 : case 3 : case 5 : case 7 : case 8 : case 10 : case 12 :
			*diaR=diarandom(1);      /* da dia random dependiendo del mes */
		break;
		case 4 : case 6 : case 9 : case 11 :
			*diaR=diarandom(2);
		break;
		default :
			*diaR=diarandom(3);
	}
	*anhoR=rand() % (2500 - 1 + 1) + 1;   /* Da un año random */
}
int diarandom(int opcion)
{
	if(opcion == 1)
		return rand() % (31 - 1 + 1) + 1;
	if(opcion == 2)
		return rand() % (30 - 1 + 1) + 1;
	if(opcion == 3)
		return rand() % (28 - 1 + 1) + 1;
}
/** B) MODULO QUE CALCULA DIAS ENTRE DOS FECHAS **/
long int calc2f(int dia,int diaR,int mes,int mesR,int anho,int anhoR)
{
	int AUX, AUX2, d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11;
	if((anho != anhoR) || (mes != mesR))
	{
		d1=31;
		d2=59;
		d3=90;
		d4=120;
		d5=151;
		d6=181;
		d7=212;
		d8=243;
		d9=273;
		d10=304;
		d11=334;
		switch(mes) /* dependiendo del mes se cuantos dias pasaron desde el 1 de enero en adelante */
		{
			case 1:
				AUX=dia;
			break;
			case 2:
				AUX=d1 + dia;
			break;
			case 3:
				AUX=d2 + dia;
			break;
			case 4:
				AUX=d3 + dia;
			break;
			case 5:
				AUX=d4 + dia;
			break;
			case 6:
				AUX=d5 + dia;
			break;
			case 7:
				AUX=d6 + dia;
			break;
			case 8:
				AUX=d7 + dia;
			break;
			case 9:
				AUX=d8 + dia;
			break;
			case 10:
				AUX=d9 + dia;
			break;
			case 11:
				AUX=d10 + dia;
			break;
			case 12:
				AUX=d11 + dia;
			break;
		} /* Fin de switch */
		switch(mesR)
		{
			case 1:
				AUX2=diaR;
			break;
			case 2:
				AUX2=d1 + diaR;
			break;
			case 3:
				AUX2=d2 + diaR;
			break;
			case 4:
				AUX2=d3 + diaR;
			break;
			case 5:
				AUX2=d4 + diaR;
			break;
			case 6:
				AUX2=d5 + diaR;
			break;
			case 7:
				AUX2=d6 + diaR;
			break;
			case 8:
				AUX2=d7 + diaR;
			break;
			case 9:
				AUX2=d8 + diaR;
			break;
			case 10:
				AUX2=d9 + diaR;
			break;
			case 11:
				AUX2=d10 + diaR;
			break;
			case 12:
				AUX2=d11 + diaR;
			break;
		}  /* Fin de switch */
		if(anho == (anhoR - 1))
			return (365 - AUX) + AUX2;  /* aqui se tiene en cuenta si son 2 anhos contiguos */
		else
			return ((365 - AUX) + AUX2) + (((anhoR - anho) - 1) * 365);  /* si no es el caso se suma diferencias */
	}
	else
		return diaR - dia;   /* para 2 fechas del mismo mes y anho */
}
/** C) MODULO PARA QUE EL USUARIO INGRESE UNA FECHA **/
void ingresodefecha(int *dia, int *mes, int *anho)
{
	int lim;   /* dependiendo el mes darah un limite para el dia ingresado */
	do
	{
		printf("\n Ingrese el anho...           **Entre 1 y 2500**\n");
		fflush(stdin);
		scanf("%d",anho);
	}while(*anho <= 0 || *anho >= 2500);
	do
	{
		printf("\n Ingrese el mes\n");
		fflush(stdin);
		scanf("%d",mes);
	}while(*mes < 1 || *mes > 12);
	switch(*mes)
	{
		case 1 : case 3 : case 5 : case 7 : case 8 : case 10 : case 12 :
			lim=31;
		break;
		case 4 : case 6 : case 9 : case 11 :
			lim=30;
		break;
		default :
			lim=28;
	}
	do
	{
		printf("\n Ingrese el dia\n");
		fflush(stdin);
		scanf("%d",dia);
	}while(*dia < 1 || *dia > lim);
}
