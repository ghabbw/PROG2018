/** Piedra, papel o tijeras: Este es un juego muy antiguo en el que intervienen 
dos personas. Cada jugador hace su elección entre las tres alternativas existentes 
(piedra, papel o tijeras) y el ganador se determina atendiendo a las siguientes reglas: 
I. La piedra gana a las tijeras. 
II. Las tijeras ganan al papel 
III. El papel gana a la piedra. 
IV. Si las dos elecciones son iguales se produce un empate. 
Escribe un programa que permita a la máquina jugar contra un humano todas las veces que 
el quiera. Máquina y humano harán su elección y la máquina será la que decida quién gana. **/
 #include <stdio.h>
#include <math.h>
#include <time.h>

/* Partes de leeCad */
const int tam_max=20;
typedef char Tcadena[20];
int leeCad(Tcadena, int);

/* Prototipos: 2 funciones y 6 procedimientos */
int veredicto(int,int);
int repetir();

void cartel(int);
void banner();
void opciones();
void empate();
void victoria();
void derrota();

int main(void) 
{           
        int pro,elex,elexpc,aux;
        Tcadena cadena;
        
        pro=0;
        srand (time(NULL));
        
        cartel(35);   /* Bienvenida e ingreso del jugador */
        printf("\n\n           Ingrese su nombre\n\n");
        printf("                 ");
        fflush(stdin);
        leeCad(cadena,tam_max);
        do            /* Nivel Facil, la elección de la pc es azarosa */
        {  
          system("cls");
          banner();
          printf("\n       Hola %s , elige una opción \n",cadena);
          printf("\n   Nivel: Facil, obtén más de 14 puntos...PUNTAJE:  %d",pro);
          opciones();
          elexpc=rand() % (3-1+1) + 1;
          printf("         ");
          fflush(stdin);
          scanf("%d",&elex);
          aux=veredicto(elex,elexpc);
          switch(aux){
          case 777:    /* los casos tienen esos numeros para evitar errores al ingresar un elex distinto de 1,2,3 */
              victoria();
              pro+=3;   /* Aumenta el puntaje cuando gana el jugador */
              break;
          case 888:
              derrota();  /* Disminuye el puntaje cuando pierde el jugador */
              pro-=2;
              break;
          case 999:
              empate();
              break;
          }
          if(pro>=14)    /* Si el jugador junta más de 14 puntos es señal para pasar al siguiente nivel*/
              elex=0;          /* Se fuerza la salida del ciclo */
        } while((elex<=3)&&(elex>=1));
        if(pro>=14)    /* Se pregunta si se salió del ciclo anterior por salir o por llegar a 14 puntos */
        {
            elexpc=rand() % (3-1+1) + 1;  /* Se necesita una eleccion de la pc al azar para empezar */
            printf("         ");
            printf("%d\n",elexpc);
            do     /* Nivel Dificil, la pc tiene en cuenta si anteriormente perdió o ganó */
            {  
              system("cls");
              banner();              
              printf("\n  Parece que %s tiene mucha suerte hoy...\n",cadena);
              printf("\n  Nivel: Dificil      PUNTAJE:  %d",pro);
              opciones();
              printf("         ");
              fflush(stdin);
              scanf("%d",&elex);
              aux=veredicto(elex,elexpc);
              switch(aux)
              {
              case 777:
                  victoria();
                  pro+=3;
                  if(!repetir())    /* Si la pc perdió tendrá más posibilidades de repetir la eleccion anterior */
                  elexpc=rand() % (3-1+1) + 1;
              break;
              case 888:
                  derrota();
                  pro-=2;
                  if(repetir())    /* Si la pc ganó tendrá más posibilidades de cambiar la elección anterior */
                      elexpc=rand() % (3-1+1) + 1;
              break;
              case 999:
                  empate();
                  elexpc=rand() % (3-1+1) + 1;
              break;
              }
            } while((elex<=3)&&(elex>=1));
        }  
        if(pro<=5)    /* Despedida, se informa el puntaje y su valoración */
        {
            system("cls");
            cartel(19);
            printf("\n         |||   Más suerte la próxima   |||    ");
            printf("\n   _-_-_-_-_-_-   PUNTAJE:  %d  _-_-_-_-_-_-_-_-_-",pro);
        }
        else
        {
            system("cls");
            cartel(22);
            printf("\n       °=°=°      Un digno rival!    °=°=°");
            printf("\n   _-_-_-_-_-     PUNTAJE:  %d   _-_-_-_-_-_-_-_-_-",pro);
        };
return 0;
}
int repetir(){
           int chance;
           chance=rand() % (10-1+1) + 1;
           if(chance>=4)
               return 1;
           else
               return 0;
}
void cartel(int i){
           int j;
           for(j=1;j<i;j++)
           {
              printf("                            *#-##       ##-## \n");
              printf("                         |#(   )#*    *#(   )#| \n");
              printf("                            \\***        ###/ \n");
              printf("                             \\*$       ##/ \n");
              printf("                              \\**     ##/ \n");
              printf("                ___________    \\$   ## \n");
              printf("               /          /     \\**## \n");
              printf("              /          /       ((O)) \n");
              printf("             /          /        **#$#\\ \n");
              printf("            /          /        *$/ \\## \n");
              printf("           /          /        *$/   \\#\\ \n");
              printf("          /__________/        *$/     \\#\\ \n");
              printf("                            *#$/       \\#\\ \n");
              printf("                           *#$/         \\#\\ \n");
              printf("                           *$/           \\#\\ \n");
              printf("                          $$/|            |#$ \n");
              printf("                           $\\             /$ \n");
              printf("\n                 PIEDRA PAPEL TIJERA       (XD) 2016                 \n");
              system("cls");
           }
}
void opciones(){
             printf("\n         ");
             printf("1: Piedra\n");
             printf("         ");
             printf("2: Papel\n");
             printf("         ");
             printf("3: Tijera\n");
             printf("         ");
             printf("Otra tecla: Salir\n");
}
int veredicto(int jug, int pc){
             if(jug!=pc)
             {
                 if(((jug==1)&&(pc==3))||((jug==2)&&(pc==1))||((jug==3)&&(pc==2)))
                     return 777;
                 if(((pc==1)&&(jug==3))||((pc==2)&&(jug==1))||((pc==3)&&(jug==2)))
                     return 888;
             }
             else
                 return 999;
}
void empate(){
           int j,i;
           i=21;
           for(j=1;j<i;j++)
           {
              system("cls");
              banner();
              printf("\n \n \n \n \n       %c %c %c   Empate %c +0\n\n",2,2,2,26);
              banner();
           }
}
void victoria(){
    int j,i;
    i=21;
    for(j=1;j<i;j++)
    {
        system("cls");
        banner();
        printf("\n \n \n \n \n    %c %c %c        Ganaste!  +3\n\n",2,2,2,26);
        banner();
    }
}
void derrota(){
            int j,i;
            i=21;
            for(j=1;j<i;j++)
            {
               system("cls");
               banner();
               printf("\n \n \n \n \n    %c %c %c        Perdiste!  -2\n\n",2,2,2,26);
               banner();
            }
}
void banner(){
           printf("       ########################################\n");
           printf("       ########################################\n");
           printf("\n \n       °°°°°°°°°°°°°°°°°°°°°°°°°°°° \n");
}
int leeCad(Tcadena cadena, int tam){
          int j, ret;
          char c;
          j=0;
          c=getchar();
          ret=0;
          while (c!=EOF && c!='\n' && j<tam-1)
          {
               cadena[j]=c;
               j++;
               c=getchar();
               ret=1;
          }
          cadena[j]='\0';
    
          while(c!=EOF && c!='\n')
               c=getchar();
return ret;
}
