/** TP3-1- Para cada problema, crear un programa utilizando la libreria <math.h>
    a) Dada una cantidad no conocida de puntos en el plano, distintos del origen, calcular el punto
    mas cercano al origen. Suponer que este punto existe y es unico.  **/
#include <stdio.h>
#include <math.h>

void muestra_punto(int,int);
void ingresa_coord(int*,int*);
float distancia_origen(int,int);

int main(void)
{
	int X, Y, auxX, auxY;
	float d, men;
	ingresa_coord(&auxX,&auxY);
	d = distancia_origen(auxX,auxY);
	men = d;
	if(d != 0)
	{
		do
		{
			ingresa_coord(&X,&Y);
			d = distancia_origen(X,Y);
			if(d < men && (X !=0 || Y != 0))
			{
				auxX = X;
				auxY = Y;
			}
		}while(d != 0);
		muestra_punto(auxX,auxY);
	}
	else
		printf("\n No ingreso ningun punto valido.");
	return 0;
}
void ingresa_coord(int *x, int *y)
{
		printf("\n Ingrese coordenada X:  ");
		fflush(stdin);
		scanf("%d",x);
		printf("\n Ingrese coordenada Y:  ");
		fflush(stdin);
		scanf("%d",y);
		printf("\n---------------------------");
}
float distancia_origen(int x, int y)
{
	return sqrt(pow(x,2) + pow(y,2));
}
void muestra_punto(int x, int y)
{
	printf("\n El punto mas cercano al origen es:  (%d, %d)",x,y);
}