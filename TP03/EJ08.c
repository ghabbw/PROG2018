/** TP3-8- 
a) Para X e i dados, diseñar un módulo que calcule la sig. sucesión: S i = 1 + X + X 2 /2! + ...+ X i /i!
Cada S i es una aproximación de la función e X ; cuanto más grande es el valor de i, más cerca es el
valor de S i del valor real de la función e X .
b) Realizar un módulo en el que dado un valor para X y un error E devuelva el primer valor de i que
cumpla una aproximación de e X con un error máximo de E.
Utilizando los módulos desarrollados en a) y b), realizar un programa que para N errores dados por el
usuario, muestre el valor de i de la aproximación. **/

#define NumeroEuler 2.7182818284
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int menu(void);
float sucesion(int,int);
float factorial(int);
int aprox(int,float);

int main(void)
{
	float err;
	int x, i, elecc;
	do
	{
		elecc = menu();
		switch(elecc)
		{
		case 1 :
			printf("Dados dos numeros naturales X y i, determinar la sucesion segun la formula dada.");
			printf("Introduce un numero para X:\n");
			scanf("%d",&x);
			printf("Introduce un numero para i:\n");
			scanf("%d",&i);
			printf("es: %f",sucesion(x,i));
		break;
		case 2:
			printf("\n Ingrese el numero X a computar");
			scanf("%d",&x);
			printf("\n Ingrese el margen de error");
			scanf("%f",&err);
			printf("\n El primer valor i cuya aprox es menor a %f para e elevado a la %d: %d",err,x,aprox(x,err));
		break;
		case 0:
			printf("\n <> Fin del Programa <>\n");
			break;
		default:
			printf("\n !!! Opcion No Correcta \n");
		} /*fin switch*/
	}while(elecc);    /*fin do*/

	return 0;
}

int menu(void)
{
	int rta, band = 0;
	do
	{
		if(band==1) printf("\n * !! Opcion fuera de rango !! * \n");
		printf("\n\n <1> Primer modulo\n");
		printf(" <2> Segundo modulo\n");
		printf(" - -  - - -\n");
		printf("-- - - -- - -- \n");
		printf(" <0> SALIR\n");
		printf("Su opcion: ");
		scanf("%d",&rta);
		band=1;
	}while(rta < 0 || rta > 3);
	return rta;
}
float sucesion(int x, int i)
{
	float acu;
	acu=0;
	while(i >= 1)
	{
		acu = (pow(x,i) / factorial(i)) + acu;
		i--;
	}
	return acu + 1;
}
float factorial(int num)
{
	int i, fac = 1;
	for(i=1; i<=num; i++)
	{
		fac*=i;
	}
	return fac;
}
int aprox(int num, float err)
{
	float dif, res1, res2;
	int i = 1, band = 0;
	while(band == 0)
	{
		res1 = pow(sucesion(1,i), num);
		res2 = pow(NumeroEuler, num);
		dif = fabs(res2 - res1);
		if(dif <= err)
			band = 1;
		i++;
	}
	return i;
}
