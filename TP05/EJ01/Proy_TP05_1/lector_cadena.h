#ifndef LECTOR_CADENA_H
#define LECTOR_CADENA_H

#include <stdio.h>
#define max 50
typedef char tcad[max];


void leecad(tcad,int);
void copia_cad(tcad,tcad);
void muestra(tcad,int);
int compara(tcad,tcad);

int tam_cad(tcad);

#endif
