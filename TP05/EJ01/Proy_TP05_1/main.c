/** TP5-1- Realizar la especificación (Estructura de datos y algunas operaciones) 
del TAD que maneja una cadena:
Operaciones:
• Leer una cadena
• Mostrar una cadena
• Comparar dos cadenas
• Copiar una cadena.
• Retornar tamaño de una cadena **/

#include <stdio.h>
#include "lector_cadena.h"

int main (void)
{
    tcad nom1,nom2,cpy;
	
	printf("Ingrese un nombre:\n");
	fflush(stdin);
	leecad(nom1,max);
	
	printf("Ingrese otro nombre:\n");
	fflush(stdin);
	leecad(nom2,max);
	
	if(!compara(nom1,nom2))
		printf("\nSon iguales");
	else
		printf("\nNo son iguales\n");
	
	printf("\nNombre a copiar %s\n",nom1);
	copia_cad(cpy,nom1);   /* copia de cpy <- nom1 */
	printf("\nNombre copiado: \n");
	muestra(cpy,max);
	
	printf("\nTamanio del nombre1:%d",tam_cad(nom1));
	printf("\nTamanio del nombre2:%d",tam_cad(nom2));
	
	return 0;
}
