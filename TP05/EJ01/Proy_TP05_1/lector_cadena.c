#include "lector_cadena.h"
#include <stdio.h>
#include <string.h>

void leecad(tcad C, int tam)
{
	int j=0, m;
	
	while(j < tam - 1 && (m = getchar()) != EOF && m != '\n')
	{
		C[j]=m;
		j++;
	}
	C[j] = '\0';
	if(m != EOF && m != '\n')
		while((m = getchar()) != EOF && m != '\n');
}
void muestra(tcad C,int tam)
{
	printf("\n%s",C);
}

void copia_cad(tcad C,tcad B)
{
	int i=0;
	while (B[i] != '\0')
	{
		C[i] = B[i];
		i++;
	}
	C[i + 1] = '\0';
}

int compara(tcad A, tcad B)
{
	int c;
	c = strcmp(A,B);
	return c;
}
int tam_cad(tcad C)
{
	int t;
	t = strlen(C);
	return t;
}
