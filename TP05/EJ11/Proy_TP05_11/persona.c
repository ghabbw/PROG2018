#include "persona.h"
#include <stdio.h>
#include <stdlib.h>

tpersona cargauno(){
	tpersona p;
	printf("\nDNI:");
	scanf("%d",&p.dni);
	fflush(stdin);
	printf("\nApellido y Nombre:");
	leecad(p.apenom,max);
	fflush(stdin);
	printf("\nTelefono Fijo:");
	scanf("%ld",&p.ntelfi);
	fflush(stdin);
	printf("\nTelefono Celular:");
	scanf("%ld",&p.ntelce);
	fflush(stdin);
	return p;
}

void muestrapersona(tpersona p){

	printf("\nApellido y Nombre:%s",p.apenom);
	printf("\nDNI:%ld",p.dni);
	printf("\nNumero de Telefono Celular:%ld",p.ntelce);
	printf("\nNumero de Telefono Fijo:%ld\n",p.ntelfi);
}

void modificatelefono(tpersona *p){
	
	printf("\nNro de Celular:");
	scanf("%ld",p->ntelce);
	
	printf("\nNro de Telefono Fijo:");
	scanf("%ld",p->ntelfi);
}

int comparanombre(tpersona a1,tpersona a2){
	return strcmp(a1.apenom,a2.apenom);
}
