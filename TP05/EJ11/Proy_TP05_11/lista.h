#ifndef LISTA_H
#define LISTA_H
#include "persona.h"
#define max 50
typedef tpersona tvec[max]; 
typedef struct{
	tvec vec;
	int tam;
}tlista;
tlista iniciar_lista();

int lista_vacia(tlista);
int lista_llena(tlista);
void insertar(tlista*,tpersona,int);
void eliminar(tlista*,int);
void eliminarpersona(tlista*,cad);
int buscanom(tlista,cad);
void muestra_lista(tlista);
void ordena_lista(tlista*);
void modificat(tlista*);
#endif
