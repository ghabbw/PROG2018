#include "tad_pers.h"
#include "tad_cadenas.h"
void cambia_fecha_nac(t_pers* p)
{
	printf("\n Ingrese nueva fecha de nac:\n");
	printf("\n Dia de nac:  ");
	fflush(stdin);
	scanf("%d",&p->fecha.dia);
	printf("\n Mes de nac:  ");
	fflush(stdin);
	scanf("%d",&p->fecha.dia);
	printf("\n Anio de nac:  ");
	fflush(stdin);
	scanf("%d",&p->fecha.dia);
}
void muestra_pers(t_pers p)
{
	printf("\n Apellido y nombre: %s",p.ApyNomb);
	printf("\n DNI: %li",p.DNI);
	printf("\n Dia de nac:  %d\n",retorna_dia(p.fecha));
	printf("\n Mes de nac:  %d\n",retorna_mes(p.fecha));
	printf("\n Anio de nac:  %d\n",retorna_anio(p.fecha));
}
t_pers carga_pers(void)
{
	t_pers p;
	printf("\n Ingrese Apellido y nombre de la persona: ");
	fflush(stdin);
	leeCad(p.ApyNomb,CADMAX);
	printf("\n Ingrese DNI: ");
	fflush(stdin);
	scanf("%li",&p.DNI);
	printf("\n Ingrese dia de nacimiento: ");
	fflush(stdin);
	scanf("%d",&p.fecha.dia);
	printf("\n Ingrese mes de nacimiento: ");
	fflush(stdin);
	scanf("%d",&p.fecha.mes);
	printf("\n Ingrese anio de nacimiento: ");
	fflush(stdin);
	scanf("%d",&p.fecha.anio);
	return p;
}
int det_edad(t_pers p, int anio)
{
	return anio - retorna_anio(p.fecha);
}
int compara_nombres(t_pers p01, t_pers p02)
{
	return strcmp(p01.ApyNomb,p02.ApyNomb);
}
int chequea_dni(long dni, t_pers p)
{
	if(p.DNI == dni)
		return 1;
	return 0;
}
int det_dni_menor(t_pers p01, t_pers p02)
{
	if(p01.DNI < p02.DNI)
		return 1;
	return 0;
}
int chequea_may18(t_fecha f, t_pers p01)
{
	if((f.anio - p01.fecha.anio) > 18)
		return 1;
	if(f.mes > p01.fecha.mes && (f.anio - p01.fecha.anio) == 18)
		return 1;
	if(f.dia > p01.fecha.dia && f.mes == p01.fecha.mes && (f.anio - p01.fecha.anio) == 18)
		return 1;
	return 0;
}
