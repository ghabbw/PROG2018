/**  TP5-8- Dada una lista de Personas eliminar de la misma todos aquellos que son mayores de
edad en una fecha dada, luego mostrar ordenados las personas que quedaron en la lista. **/

#include <stdio.h>
#include "tad_cadenas.h"
#include "tad_lista.h"
#include "tad_pers.h"

int main(void)
{
	t_lista lis;
	t_fecha fech;
	lis = nueva_lista();
	carga_lista(&lis);
	muestra_lista(lis);
	fech = nueva_fecha();
	printf("\n ====== Ordenando lista por dni: ======\n");
	ordena_ascXdni_lis(&lis);
	muestra_lista(lis);
	printf("\n ====== Eliminando personas mayores de la lista: =======\n");
	elimina_may18(&lis,fech);
	muestra_lista(lis);
	
	return 0;
}
