#ifndef TAD_FECHA_H
#define TAD_FECHA_H

typedef struct
{
	int dia;
	int mes;
	int anio;
}t_fecha;

t_fecha nueva_fecha();
int retorna_dia(t_fecha);
int retorna_mes(t_fecha);
int retorna_anio(t_fecha);

#endif
