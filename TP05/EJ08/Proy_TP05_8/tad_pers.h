#ifndef TAD_PERS_H
#define TAD_PERS_H
#include <stdio.h>
#include <string.h>
#include "tad_cadenas.h"
#include "tad_fecha.h"

typedef struct
{
	t_cad ApyNomb;
	long DNI;
	t_fecha fecha;
}t_pers;
void cambia_fecha_nac(t_pers*);
void muestra_pers(t_pers);
t_pers carga_pers(void);
int det_edad(t_pers,int);
int compara_nombres(t_pers,t_pers);
int chequea_dni(long,t_pers);
int det_dni_menor(t_pers,t_pers);
int chequea_may18(t_fecha,t_pers);

#endif
