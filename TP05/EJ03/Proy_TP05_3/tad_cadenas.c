#include "tad_cadenas.h"
#include <stdio.h>
#include <string.h>

void leeCad(t_cad C, int tam)
{
	int j = 0, m;
	
	while(j < tam - 1 && (m = getchar()) != EOF && m != '\n')
	{
		C[j]=m;
		j++;
	}
	C[j] = '\0';
	if(m != EOF && m != '\n')
		while((m = getchar()) != EOF && m != '\n');
}
