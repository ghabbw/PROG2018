/** TP5-3- Utilizando el TAD del punto anterior crear un programa que dada 3 personas y dada
la fecha actual se pide mostrar el nombre, la fecha de cumplea�os y la edad que cumple 
en el a�o de la fecha actual, ordenados por el campo nombre de manera ascendente. 
	Adem�s permitir cambiar la fecha de nacimiento de la persona cuyo nombre es 
el menor de todos (se supone �nico). **/

#include <stdio.h>
#include "tad_cadenas.h"
#include "tad_pers.h"

void cargar_fecha(int*,int*,int*);

int main(void)
{
	t_pers per1, per2, per3;
	int dia, mes, anio;
	cargar_fecha(&dia,&mes,&anio);
	per1 = carga_pers();
	per2 = carga_pers();
	per3 = carga_pers();
	ordena_alfab(&per1,&per2,&per3);
	muestra_pers(per1);
	printf("\n----------------\n Este anio cumple: %d\n",det_edad(per1,anio));
	muestra_pers(per2);
	printf("\n----------------\n Este anio cumple: %d\n",det_edad(per2,anio));
	muestra_pers(per3);
	printf("\n----------------\n Este anio cumple: %d\n",det_edad(per3,anio));
	return 0;
}
void cargar_fecha(int *d, int *m, int *a)
{
	printf("\n Ingrese fecha:\n ");
	printf("\n Dia: ");
	fflush(stdin);
	scanf("%d",d);
	printf("\n Mes: ");
	fflush(stdin);
	scanf("%d",m);
	printf("\n Anio: ");
	fflush(stdin);
	scanf("%d",a);
}
