#include "tad_pers.h"
#include "tad_cadenas.h"
void cambia_fecha_nac(t_pers* p)
{
	printf("\n Ingrese nueva fecha de nac:\n");
	printf("\n Dia de nac:  ");
	fflush(stdin);
	scanf("%d",&p->dia_nac);
	printf("\n Mes de nac:  ");
	fflush(stdin);
	scanf("%d",&p->mes_nac);
	printf("\n Anio de nac:  ");
	fflush(stdin);
	scanf("%d",&p->anio_nac);
}
void muestra_pers(t_pers p)
{
	printf("\n Apellido y nombre: %s",p.ApyNomb);
	printf("\n DNI: %li",p.DNI);
	printf("\n Fecha de nac: %d/%d/%d",p.dia_nac,p.mes_nac,p.anio_nac);
}
t_pers carga_pers(void)
{
	t_pers p;
	printf("\n Ingrese Apellido y nombre de la persona: ");
	fflush(stdin);
	leeCad(p.ApyNomb,CADMAX);
	do
	{
		printf("\n Ingrese DNI: ");
		fflush(stdin);
		scanf("%li",&p.DNI);
	}while(p.DNI < 10000000 || p.DNI > 50000000);
	do
	{
		printf("\n Ingrese dia de nacimiento: ");
		fflush(stdin);
		scanf("%d",&p.dia_nac);
	}while(p.dia_nac < 1 || p.dia_nac >31);
	do
	{
		printf("\n Ingrese mes de nacimiento: ");
		fflush(stdin);
		scanf("%d",&p.mes_nac);
	}while(p.mes_nac < 1 || p.mes_nac > 12);
	do
	{
		printf("\n Ingrese anio de nacimiento: ");
		fflush(stdin);
		scanf("%d",&p.anio_nac);
	}while(p.anio_nac < 1900 || p.anio_nac >2019);
	return p;
}
int det_edad(t_pers p, int anio)
{
	return anio - p.anio_nac;
}
int compara_nombres(t_pers p01, t_pers p02)
{
	return strcmp(p01.ApyNomb,p02.ApyNomb);
}
