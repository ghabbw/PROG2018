/** TP5-2- Realizar la especificaci�n e implementaci�n del TAD registro de una persona con los
	siguientes campos: Apellido y Nombre, DNI, D�a de Nacimiento, Mes de Nacimiento, A�o de
	Nacimiento. Y las siguientes operaciones:
	�  Cargar una persona
	�  Mostrar una Persona.
	�  Retornar la edad de una persona en un a�o dado.
	�  Comparar dos nombres
	�  Modificar Fecha de nacimiento.  **/
#include <stdio.h>
#include "tad_pers.h"

int main(void)
{
	t_pers pers01, pers02;
	int anio;
	
	pers01 = carga_pers();
	muestra_pers(pers01);
	do
	{
		printf("\n Ingrese el anio actual para calcular la edad:  ");
		fflush(stdin);
		scanf("%d",&anio);
	}while(det_edad(pers01,anio) < 0);
	printf("\n La edad de es: %d",det_edad(pers01,anio));
	printf("\n ingresando datos de segunda persona: ");
	pers02 = carga_pers();
	muestra_pers(pers02);
	if(!compara_nombres(pers02,pers01))
		printf("\n Se llaman igual.\n");
	else
		printf("\n Se llaman distinto.\n");
	printf("\n Cambiando fecha de nacimiento de la primera persona ingresada:");
	cambia_fecha_nac(&pers01);
	muestra_pers(pers01);
	return 0;
}
