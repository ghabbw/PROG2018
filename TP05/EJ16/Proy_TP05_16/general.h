#ifndef GENERAL_H
#define GENERAL_H
#define MAX_CAD 100
#define MIN_DNI 10000000 /* control de entrada para dni */
#include <stdio.h>

typedef char t_cad[MAX_CAD];
void leecad(t_cad,int);
typedef struct
{
	int hor;
	int min;
}t_tiempo;
typedef long t_dni;
void convert_tiempo(long,int*,int*,int*);
void cargar_dni(t_dni*);
void ini_tiempo(t_tiempo*);
void acumula_tiempo(t_tiempo,t_tiempo*);
void modif_hora(t_tiempo*,int,int);
void modif_dni(t_dni*,t_dni);
t_tiempo prom_tiempo(t_tiempo,int);
t_tiempo resta_hora(t_tiempo,t_tiempo);
t_tiempo resta_minuto(t_tiempo,t_tiempo);
t_tiempo suma_hor_min(t_tiempo,t_tiempo);
int retorna_hora(t_tiempo);
int retorna_min(t_tiempo);
#endif
