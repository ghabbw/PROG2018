#ifndef PERSONA_H
#define PERSONA_H
#include "general.h"
typedef struct
{
	t_dni dni;
	t_cad ApeNom;
	t_tiempo llegada;
	t_tiempo atencion;
	int yaAtendida;   /* 1 si ya fue atendida */
}t_persona;
t_persona nueva_persona(void);
t_dni retorna_dni(t_persona);
t_tiempo resta_tiempo(t_persona);
void muestra_persona(t_persona);
void marca_pers_atendida(t_persona*);
void modif_dni_pers(t_persona*,t_dni);
void ingresa_hora_atencion(t_persona*);
#endif
