#include "general.h"
void convert_tiempo(long tiempo, int * hor, int * min, int * band)
{
	* min = tiempo % 100;
	* hor = tiempo / 100;
	if((*min >= 0 && *min <= 59) && (*hor >= 0 && *hor <= 24))
		*band = 0;
}
void cargar_dni(t_dni * DNI)
{
	do
	{
		printf("\n Ingrese un DNI: ");
		fflush(stdin);
		scanf("%li",DNI);
	}while(*DNI < MIN_DNI);
}
void leecad(t_cad cdn, int tam)
{
	int m, j = 0;
	fflush(stdin);
	while(j < tam - 1 && (m = getchar()) != EOF && m != '\n')
	{
		cdn[j] = m;
		j++;
	}	
	cdn[j] = '\0';
	if(m != EOF && m != '\n')
		while((m = getchar()) != EOF && m != '\n');
}
void ini_tiempo(t_tiempo * time)
{
	time->hor = 0;
	time->min = 0;
}
void acumula_tiempo(t_tiempo time, t_tiempo * acu)
{
	acu->hor = acu->hor + time.hor;
	acu->min = acu->min + time.min;
	if(acu->min >= 60)
	{	/* si hay mas de 60 mins se aumenta una hora y quitan 60 mins */
		acu->hor = acu->hor + 1;
		acu->min = acu->min - 60;
	}
}
t_tiempo prom_tiempo(t_tiempo acu, int tam)
{
	t_tiempo prom;
	prom.hor = acu.hor / tam;
	prom.min = acu.min / tam;
	return prom;
}
t_tiempo resta_hora(t_tiempo segundo, t_tiempo primero)
{	/* atencion - llegada */
	t_tiempo aux;
	int hora, min;
	hora = segundo.hor - primero.hor;
	if(hora != 0)
	{	/* control de la hora */
		min = (60 - primero.min) + (segundo.min);
		if(min > 60)
			min = min - 60;
		if(min == 60)
			min = 0;
	}
	else		/* si la llegada y la atencion son a la misma hora */
	   min = segundo.min - primero.min;
	aux.hor = hora;
	aux.min = min;
	return aux;
}
t_tiempo resta_minuto(t_tiempo primero, t_tiempo segundo)
{
	return primero;
}
t_tiempo suma_hor_min(t_tiempo hora, t_tiempo min)
{
	return hora;
}
int retorna_hora(t_tiempo time)
{
	return time.hor;
}
int retorna_min(t_tiempo time)
{
	return time.min;
}
void modif_hora(t_tiempo * time, int hr, int mn)
{
	time->hor = hr;
	time->min = mn;
}
void modif_dni(t_dni * viejo, t_dni nuevo)
{
	*viejo = nuevo;
}
