/** TP5-16- Un consultorio de un especialista necesita manejar la lista de personas en espera y una
	lista de personas atendidas que llegan en un d�a determinado.
	Crear un programa que utilizando un TAD Lista de Personas y mediante un men� permita manejar la
	lista de espera y la lista de atendidos de un consultorio teniendo en cuenta que de cada paciente se
	almacena el Documento, Apellido y Nombre, Hora de llegada, Hora de atenci�n.
	El programa deber� permitir:
	a) Inicializar la lista de espera.
	b) Agregar un nuevo paciente a la lista de espera. (Siempre al final)
	c) Atender un paciente de la lista espera. (Eliminar el primero de la lista y pasarlo a la lista de 
	pacientes atendidos con la hora de atenci�n)
	d) Mostrar los datos de un paciente (puede estar en la lista de espera o en la lista de atendidos).
	e) Calcular el tiempo promedio de espera por paciente, de los pacientes atendidos. **/
#include <stdio.h>
#include "persona.h"
#include "lista_de_personas.h"
int menu(void);

int main(void)
{
	t_lista espera, atendidos;
	t_dni DNI;
	t_tiempo time;
	int opc, posEspera, pos, posAtendidos;
	ini_tiempo(&time);
	ini_lista(&espera);
	ini_lista(&atendidos);
	do
	{
		opc = menu();
		switch(opc)
		{
			case 1:
				nuevo_pac_espera(&espera);
				muestra_persona_lis(retorna_lis_tam(espera),espera);
			break;
			case 2:  /* control si la lista de espera esta vacia en el modulo */
				nuevo_pac_atendido(&espera,&atendidos);
			break;
			case 3:
				cargar_dni(&DNI);
				
				posEspera = buscar_dni(DNI,espera);
				if(posEspera)
					muestra_persona_lis(posEspera,espera);
				
				posAtendidos = buscar_dni(DNI,atendidos);
				if(posAtendidos)
					muestra_persona_lis(posAtendidos,atendidos);
				
				pos = posEspera + posAtendidos;
				if(pos == 0)
					printf("\n Paciente no encontrado\n");
			break;
			case 4:
				time = tiempo_prom(atendidos);
				printf("\n Tiempo promedio de espera: %d:%d\n",retorna_hora(time),retorna_min(time));
			break;
			case 0:
				printf("\n Fin del Programa.\n");
			break;
			default:
				printf("\n Opcion fuera de rango.\n");
		}
	}while(opc != 0);
	return 0;
}
int menu(void)
{
	int opc;
	printf("\n ------------------------------------\n");
	printf("\n 1- Agregar nuevo paciente a la lista de espera.");
	printf("\n 2- Atender un paciente de la lista de espera.");
	printf("\n 3- Mostrar los datos de un paciento (DNI).");
	printf("\n 4- Mostrar el tiempo de espera por paciente.");
	printf("\n 0- Salir\n   ");
	fflush(stdin);
	scanf("%d",&opc);
	return opc;
}
