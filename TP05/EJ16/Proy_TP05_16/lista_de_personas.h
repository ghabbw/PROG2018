#ifndef LISTA_DE_PERSONAS_H
#define LISTA_DE_PERSONAS_H
#define MAX_VEC 50
#include "general.h"
#include "persona.h"

typedef t_persona vec[MAX_VEC];
typedef struct
{
	vec vecpers;
	int tam;
	int lis_llena;  /* si esta llena = 1 */
	int lis_vacia;  /* si esta vacia = 1 */
}t_lista;
t_tiempo tiempo_prom(t_lista);
int buscar_dni(t_dni,t_lista);
int retorna_lis_tam(t_lista);
void ini_lista(t_lista*);
void muestra_lista(t_lista);
void nuevo_pac_espera(t_lista*);
void nuevo_pac_atendido(t_lista*,t_lista*);
void muestra_persona_lis(int,t_lista);
#endif
