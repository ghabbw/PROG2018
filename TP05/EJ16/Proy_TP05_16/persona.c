#include "persona.h"
/* prototipos funciones privadas: */
void modif_hora_pers_atenc(t_persona*,int,int);
void modif_hora_pers_lleg(t_persona*,int,int);
/* definiciones de funciones */
t_persona nueva_persona(void)
{
	t_persona pers;
	t_dni DNI;
	long tiempo;
	int hora, min, band = 1;
	cargar_dni(&DNI);	/* control del dni realista */
	modif_dni_pers(&pers,DNI);
	printf("\n Ingrese nombre: ");
	leecad(pers.ApeNom,MAX_CAD);
	do
	{	/* convert_tiempo() tambien controla que la hora sea valida */
		printf("\n Ingrese hora de llegada:   <hhmm> sin dos puntos    ");
		fflush(stdin);
		scanf("%ld",&tiempo);
		convert_tiempo(tiempo,&hora,&min,&band);
	}while(band);
	modif_hora_pers_lleg(&pers,hora,min);
	pers.yaAtendida = 0;	/* control para muestra_persona() */
	return pers;
}
t_dni retorna_dni(t_persona pers)
{
	return pers.dni;
}
void muestra_persona(t_persona pers)
{
	printf("\n DNI: %ld",retorna_dni(pers));
	printf("\n Apellido y Nombre: %s",pers.ApeNom);
	printf("\n Hora de llegada:  %d:%d",retorna_hora(pers.llegada),retorna_min(pers.llegada));
	if(pers.yaAtendida)
		printf("\n Hora de atencion:  %d:%d",retorna_hora(pers.atencion),retorna_min(pers.atencion));
	else
		printf("\n Todavia no fue atendido. \n");
}
void modif_hora_pers_lleg(t_persona * pers, int hora, int minuto)
{
	modif_hora(&pers->llegada,hora,minuto);
}
void modif_hora_pers_atenc(t_persona * pers, int hora, int minuto)
{
	modif_hora(&pers->atencion,hora,minuto);
}
void modif_dni_pers(t_persona * pers, t_dni DNI)
{
	modif_dni(&pers->dni,DNI);
}
t_tiempo resta_tiempo(t_persona pers)
{	/* para calcular el tiempo promedio */
	return resta_hora(pers.atencion,pers.llegada);
}
void marca_pers_atendida(t_persona * pers)
{
	pers->yaAtendida = 1;
}
void ingresa_hora_atencion(t_persona * pers)
{
	long tiempo;
	int hora, min, band = 1;
	do
	{
		printf("\n Ingrese hora de atencion:    <hhmm> sin dos puntos    ");
		fflush(stdin);
		scanf("%ld",&tiempo);
		convert_tiempo(tiempo,&hora,&min,&band);
	}while(band);
	modif_hora_pers_atenc(pers,hora,min);
}
