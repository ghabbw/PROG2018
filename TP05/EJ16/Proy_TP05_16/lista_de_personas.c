#include "lista_de_personas.h"
void ini_lista(t_lista * lis)
{
	lis->tam = 0;
	lis->lis_vacia = 1;
	lis->lis_llena = 0;
}
void muestra_lista(t_lista lis)
{
	int i;
	if(lis.lis_vacia != 0)
		for(i = 1; i <= lis.tam; i++)
			muestra_persona(lis.vecpers[i]);
	else
		printf("\n Lista vacia.\n");
}
void nuevo_pac_espera(t_lista * lis)
{
	lis->lis_vacia = 0;
	if(lis->lis_llena != 1)
	{
		lis->tam = lis->tam + 1;
		lis->vecpers[lis->tam] = nueva_persona();
		if(lis->tam == MAX_VEC)	/* si el nuevo ingreso ya lleno la lista */
			lis->lis_llena = 1;
	}
	else
		printf("\n Lista llena.\n");
}
void nuevo_pac_atendido(t_lista * espera, t_lista * atendidos)
{
	int i;
	t_persona aux_pers;
	if(espera->lis_vacia != 1)
	{
		aux_pers = espera->vecpers[1];
		marca_pers_atendida(&aux_pers);
		ingresa_hora_atencion(&aux_pers);
		
		for(i = 1; i < espera->tam; i++)	/* desplazamiento elimina la primer posicion */
			espera->vecpers[i] = espera->vecpers[i + 1];
		espera->tam = espera->tam - 1;
		
		if(espera->tam == 0)	/* actualizacion del estado de las listas */
			espera->lis_vacia = 1;
		atendidos->lis_vacia = 0;
		/* no reviso que la lista de atendidos este llena porque amabas listas tienen el mismo tamanho */
		atendidos->tam = atendidos->tam + 1;
		atendidos->vecpers[atendidos->tam] = aux_pers;	/* insercion del aux_pers */
	}
	else
		printf("\n Error... La lista de espera esta vacia.");
}
int buscar_dni(t_dni DNI, t_lista lis)
{
	int i = 1, band = 0;
	if(lis.lis_vacia != 1)
	{
		while(i <= lis.tam && band == 0)
		{
			if(retorna_dni(lis.vecpers[i]) != DNI)
				i++;
			else
				band = 1;
		}
		if(band)
			return i;
	}
	return 0;	/* la lista comienza en 1, 0 es para no encontrado */
}
void muestra_persona_lis(int pos, t_lista lis)
{
	muestra_persona(lis.vecpers[pos]);
}
int retorna_lis_tam(t_lista lis)
{
	return lis.tam;
}
t_tiempo tiempo_prom(t_lista lis)
{	/* toma la lista de atendidos */
	int i;
	t_tiempo aux_tiempo, acum_tiempo;
	ini_tiempo(&acum_tiempo);
	if(lis.lis_vacia != 1)
	{
		for(i = 1; i <= lis.tam; i++)
		{
			aux_tiempo = resta_tiempo(lis.vecpers[i]);
			acumula_tiempo(aux_tiempo,&acum_tiempo);
		}
		return prom_tiempo(acum_tiempo,lis.tam);
	}
	return acum_tiempo;	/* retornara un t_tiempo inicializado en ceros */
}
