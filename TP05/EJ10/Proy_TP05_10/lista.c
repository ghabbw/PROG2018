#include "lista.h"
#include <stdio.h>

tlista iniciar_lista(){
	tlista aux;
	aux.tam=0;
	return aux;
}
int rettam(tlista lis){
	return lis.tam;
}
int lista_vacia(tlista lis){
	if(lis.tam==0){
		return 0;
	}
	else
	   return 1;
}
int lista_llena(tlista lis){
	if(lis.tam==max)
	   return 0;
	else 
		return 1;
}
int buscadni(tlista lis,long buscado){
	int i=1;
	while(i<=lis.tam&&retdni(lis.vec[i])!=buscado)
		i++;
	if(i<=lis.tam)
	   return i;
	else 
		return -1;
}
void correri(tvec vec,int n,int pos){
	int i; 
	for(i=n;i>=pos;i--)
		vec[i+1]=vec[i];
}
void insertar(tlista *lis,tpersona p,int pos){
	int i;
	if(lista_llena(*lis)!=0){
		correri(lis->vec,lis->tam,pos);
		lis->vec[pos]=p;
		lis->tam=lis->tam+1;
	}
}
void correre(tvec vec,int n,int pos){
	int i;
	for(i=pos;i<=n-1;i++)
		vec[i]=vec[i+1];
}
void eliminar(tlista *lis,int pos){
	if(lista_vacia(*lis)!=0){
		correre(lis->vec,lis->tam,pos);
		lis->tam=lis->tam-1;
	}
}
void eliminarpersona(tlista *lis,long dni){
	int i=1;
	while(i<=lis->tam){
		if(retdni(lis->vec[i])==dni)
		   eliminar(lis,i);
		else
			i++;
	}
}

void muestravector(tvec vec,int n){
	int i;
	for(i=1;i<=n;i++)
		muestrapersona(vec[i]);
}
void muestra_lista(tlista lis){
	muestravector(lis.vec,lis.tam);
}
void ordenaxdni(tvec vec,int n){
	int i,j;
	tpersona aux;
	for(i=1;i<=n-1;i++){
		for(j=i+1;j<=n;j++){
			if(retdni(vec[j])<retdni(vec[i])<0){
				aux=vec[i];
				vec[i]=vec[j];
				vec[j]=aux;
			}
		}
	}
}
void ordena_lista(tlista *lis){
	ordenaxdni(lis->vec,lis->tam);
}

void modificat(tlista *lis){
    modificatelefono(lis->vec);
}