#include "persona.h"
#include <stdio.h>
#include <stdlib.h>

tpersona cargauno(){
	tpersona p;
	printf("\nDNI:");
	scanf("%d",&p.dni);
	fflush(stdin);
	printf("\nApellido y Nombre:");
	leecad(p.apenom,max);
	fflush(stdin);
	printf("\nTelefono Fijo:");
	scanf("%ld",&p.ntelfi);
	fflush(stdin);
	printf("\nTelefono Celular:");
	scanf("%ld",&p.ntelce);
	fflush(stdin);
	return p;
}

void muestrapersona(tpersona p){
	printf("\nDNI:%ld",p.dni);
	printf("\nApellido y Nombre:%s",p.apenom);
	printf("\nNumero de Telefono Celular:%ld",p.ntelce);
	printf("\nNumero de Telefono Fijo:%ld",p.ntelfi);
}

void modifcatelefono(tpersona *p){
	printf("\nNro de Celular:");
	scanf("%ld",p->ntelce);
	
	printf("\nNro de Telefono Fijo:");
	scanf("%ld",p->ntelfi);
}
long retdni(tpersona p) {
	return p.dni;
}
