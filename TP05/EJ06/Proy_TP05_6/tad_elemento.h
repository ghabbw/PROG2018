#ifndef TAD_ELEMENTO_H
#define TAD_ELEMENTO_H
#include <stdio.h>
typedef struct
{
	int elemento;
}t_elemento;
t_elemento nuevo_elemento(void);
void muestra_elemento(t_elemento);
int retorna_elem(t_elemento);
void modif_elem(t_elemento*,t_elemento);
#endif
