#include "tad_conjunto.h"
#include "tad_elemento.h"
#include <stdio.h>

t_conjunto nuevo_conjunto(void)
{
	int i, cant;
	t_conjunto cnjnt;
	printf("\n Ingrese la cantidad de elementos a crear:");
	scanf("%d",&cant);
	for(i = 1; i <= cant; i++)
		cnjnt.conj[i] = nuevo_elemento();
	cnjnt.tam = cant;
	return cnjnt;
}
void ini_conjunto(t_conjunto * cnjnt)  /* Inicializa en cero la cantidad de elementos */
{
	cnjnt->tam = 0;
}
void muestra_conjunto(t_conjunto cnjnt)
{	/* muestra los elementos del vector conj[] uno por uno */
	int i;
	if(cardinal_conjunto(cnjnt))
		for(i = 1; i <= cnjnt.tam; i++)
			printf("\n Elemento <%d>: %d",i,retorna_elem(cnjnt.conj[i]));
	else
		printf("\n Conjunto vacio.");
}
t_conjunto une_conjuntos(t_conjunto cnjntA, t_conjunto cnjntB)
{	/* copia conj A en C, luego pregunta en B si alguno esta repetido en A, caso contrario se copia en C */
	t_conjunto cnjntC;
	int i, j, k = cnjntA.tam + 1, tamC = 0, band;
	cnjntC = cnjntA;
	tamC = cnjntA.tam;
	for(i = 1; i <= cnjntB.tam; i++)
	{
		band = 1;
		for(j = 1; j <= cnjntA.tam; j++)
			if(retorna_elem(cnjntA.conj[j]) == retorna_elem(cnjntB.conj[i]))
				band = 0;
		if(band)
		{
			modif_elem(&cnjntC.conj[k],cnjntB.conj[i]);
			tamC++;
			k++;
		}
	}
	cnjntC.tam = tamC;
	return cnjntC;
}
t_conjunto conj_resta_conj(t_conjunto cnjntA, t_conjunto cnjntB)
{	/* recorre conj A buscando si B lo tiene, caso contrario copia en C */
	t_conjunto cnjntC;
	int i, j, k = 1, tamC = 0, band;
	for(i = 1; i <= cnjntA.tam; i++)
	{
		band = 1;
		for(j = 1; j <= cnjntB.tam; j++)
			if(retorna_elem(cnjntA.conj[i]) == retorna_elem(cnjntB.conj[j]))
				band = 0;
		if(band)
		{
			modif_elem(&cnjntC.conj[k],cnjntA.conj[i]);
			tamC++;
			k++;
		}
	}
	cnjntC.tam = tamC;
	return cnjntC;
}
t_conjunto interseccion_conjuntos(t_conjunto cnjntA, t_conjunto cnjntB)
{
	int i, j, k = 1, tamC = 0;
	t_conjunto cnjntC;
	for(i = 1; i <= cnjntA.tam; i++)
		for(j = 1; j <= cnjntB.tam; j++)
			if(retorna_elem(cnjntA.conj[i]) == retorna_elem(cnjntB.conj[j]))
			{
				cnjntC.conj[k] = cnjntA.conj[i];
				tamC++;
				k++;
			}
	cnjntC.tam = tamC;
	return cnjntC;
}
t_conjunto dif_simetrica_conjuntos(t_conjunto cnjntA, t_conjunto cnjntB)
{
	int i, j, k = 1, tamC = 0, band;
	t_conjunto cnjntC;
	for(i = 1; i <= cnjntA.tam; i++)
	{
		band = 1;
		for(j = 1; j <= cnjntB.tam; j++)
			if(retorna_elem(cnjntA.conj[i]) == retorna_elem(cnjntB.conj[j]))
				band = 0;
		if(band)
		{
			modif_elem(&cnjntC.conj[k],cnjntA.conj[i]);
			tamC++;
			k++;
		}
	}
	for(i = 1; i <= cnjntB.tam; i++)
	{
		band = 1;
		for(j = 1; j <= cnjntA.tam; j++)
			if(retorna_elem(cnjntB.conj[i]) == retorna_elem(cnjntA.conj[j]))
				band = 0;
		if(band)
		{
			modif_elem(&cnjntC.conj[k],cnjntB.conj[i]);
			tamC++;
			k++;
		}
	}
	cnjntC.tam = tamC;
	return cnjntC;
}
int cardinal_conjunto(t_conjunto cnjnt)
{
	return cnjnt.tam;
}
int pertenece_elem_conj(t_conjunto cnjnt, t_elemento elem)
{
	int i, band = 0;
	for(i = 1; i <= cnjnt.tam; i++)
		if(retorna_elem(cnjnt.conj[i]) == retorna_elem(elem))
			band = 1;
	if(band)
		return 1;
	return 0;
}
