#ifndef TAD_CONJUNTO_H
#define TAD_CONJUNTO_H
#define MAX 100
#include "tad_elemento.h"
typedef t_elemento conjunto[MAX];
typedef struct
{
	conjunto conj;
	int tam;
}t_conjunto;
void ini_conjunto(t_conjunto*);
void muestra_conjunto(t_conjunto);
t_conjunto nuevo_conjunto(void);
t_conjunto une_conjuntos(t_conjunto,t_conjunto);
t_conjunto conj_resta_conj(t_conjunto,t_conjunto);
t_conjunto interseccion_conjuntos(t_conjunto,t_conjunto);
t_conjunto dif_simetrica_conjuntos(t_conjunto,t_conjunto);
int cardinal_conjunto(t_conjunto);
int pertenece_elem_conj(t_conjunto,t_elemento);
#endif
