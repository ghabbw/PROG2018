/** TP5-EJ6- Realizar la implementación del TAD del ejercicio 5. **/

#include <stdio.h>
#include "tad_elemento.h"
#include "tad_conjunto.h"
int menu(void);
int main(void)
{
	int op;
	t_elemento elem;
	t_conjunto conjA, conjB, conjC;
	ini_conjunto(&conjA);
	ini_conjunto(&conjB);
		
	do
	{
		op = menu();
		switch(op)
		{
			case 1:
				conjA = nuevo_conjunto();
				conjB = nuevo_conjunto();
			break;
			case 2:
				muestra_conjunto(conjA);
				muestra_conjunto(conjB);
			break;
			case 3:
				conjC = une_conjuntos(conjA,conjB);
				muestra_conjunto(conjC);
			break;
			case 4:
				conjC = interseccion_conjuntos(conjA,conjB);
				muestra_conjunto(conjC);
			break;
			case 5:
				conjC = dif_simetrica_conjuntos(conjA,conjB);
				muestra_conjunto(conjC);
			break;
			case 6:
				conjC = conj_resta_conj(conjA,conjB);
				muestra_conjunto(conjC);
			break;
			case 7:
				elem = nuevo_elemento();
				if(pertenece_elem_conj(conjA,elem))
				{
					muestra_elemento(elem);
					printf("\n Si pertenece al conjunto A.\n");
				}
				else
				{
					muestra_elemento(elem);
					printf("\n No pertenece al conjunto A.\n");
				}
			break;
			case 8:
				printf("\n Cardinalidad del conjunto A: %d \n",cardinal_conjunto(conjA));
				printf("\n Cardinalidad del conjunto B: %d \n",cardinal_conjunto(conjB));
			break;
			case 0:
				printf("\n Fin del programa.");
			break;
			
			default:
				printf("\n Ingreso fuera de rango.\n");
		}
	}while(op != 0);
	
	return 0;
}
int menu(void)
{
	int opc;
	printf("\n -----------------------------------");
	printf("\n 1- Crear conjunto A y conjunto B.");
	printf("\n 2- Mostrar conjuntos.");
	printf("\n 3- Mostrar la union de los conjuntos A y B.");
	printf("\n 4- Mostrar la interseccion de los conjuntos A y B.");
	printf("\n 5- Mostrar la diferencia simetrica de los conjuntos A y B.");
	printf("\n 6- Mostrar el conjunto A menos el conjunto B.");
	printf("\n 7- Crear elemento y preguntar si pertenece al conjunto A.");
	printf("\n 8- Determinar cardinalidad de los conjuntos A y B.");
	printf("\n\n 0- Para salir.\n   ");
	fflush(stdin);
	scanf("%d",&opc);
	return opc;
}
