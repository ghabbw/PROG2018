/** TP5-7- Realizar la especificación y la implementación de un TAD que maneje una lista de
Personas. El TAD ListaPersonas debe permitir inicializar la lista, agregar una persona, eliminar una
persona, ordenar por dni, buscar una persona por dni, determinar si la lista está vacía, determinar si
la lista está llena. Reutilizar el TAD del ejercicio 2. **/

#include <stdio.h>
#include "tad_pers.h"
#include "tad_lista.h"

int main(void)
{
	t_lista lis01;
	t_pers pers01;
	long DNI;
	lis01 = nueva_lista();  /* inicializando lista */
	carga_lista(&lis01);
	printf("\n ------------");
	agrega_pers(&lis01,pers01); /* agregando persona a la lista */
	muestra_lista(lis01);   /*  muestra lista completa */
	printf("\n Ingresar dni a buscar:");
	fflush(stdin);
	scanf("%li",&DNI);
	printf("\n posic: %d\n",busca_lista(lis01,DNI));
	elimina_elemento(&lis01);
	muestra_lista(lis01);
	ordena_ascXdni_lis(&lis01);
	printf("\n Lista ordenada:\n");
	muestra_lista(lis01);
	
	return 0;
}
