#include <stdio.h>
#include "tad_lista.h"

/* prototipos funciones privadas */
int det_lista_llena(t_lista);
int det_lista_vacia(t_lista);

/* definicion de las funciones */
t_lista nueva_lista(void)
{
	t_lista lis;
	lis.tam = 0;
	return lis;
}
int det_lista_llena(t_lista lis)
{
	if(lis.tam == VECMAX - 1) /* uno menos porque lis->lista[0] se usa como bandera */
		return 1;
	return 0;
}
void carga_lista(t_lista *lis)
{
	int resp;
	if(!det_lista_llena(*lis))
	{
		printf("\n Para ingresar la primera persona, ingrese los datos:");
		do
		{
			lis->tam = lis->tam + 1;
			lis->lista[lis->tam] = carga_pers();
			printf("\n Para ingresar otra persona ingrese '1':\n");
			fflush(stdin);
			scanf("%d",&resp);
		}while(resp == 1);
	}
	else
		printf("\n La lista esta completa. No se pueden agregar mas datos.");
}
void muestra_lista(t_lista lis)
{
	int i;
	if (lis.tam > 0)
		for(i = 1; i <= lis.tam; i++)
			muestra_pers(lis.lista[i]);
	else
		printf("\n Lista sin elementos.\n");
}
void agrega_pers(t_lista* lis, t_pers p)
{
	if (!det_lista_llena(*lis))  
	{
		lis->tam = lis->tam + 1;
		lis->lista[lis->tam] = carga_pers();
	}
	else
		printf("\n La lista esta completa. No se pueden agregar mas datos.");
}
int busca_lista(t_lista lis,long dni)
{
	int i = 0, r;
	if(lis.tam > 0)
	{
		while(chequea_dni(dni,lis.lista[i]) != 1 && i <= lis.tam)
			i++;
		if(i > lis.tam)
			return 0;
		r = i;
	}
	else
		printf("\n No se encuentra, lista sin elementos.\n");
	return r;
}
int det_lista_vacia(t_lista lis)
{
	if(lis.tam == 0)
		return 1;
	return 0;
}
void elimina_elemento(t_lista *lis)
{
	long dni;
	int pos, i;
	if(!det_lista_vacia(*lis))
	{
		printf("\n Ingrese DNI de la persona a eliminar: ");
		fflush(stdin);
		scanf("%li",&dni);
		if((pos = busca_lista(*lis,dni)) != 0)
		{
			for(i = pos; i <= (lis->tam - 1); i++)
				lis->lista[i] = lis->lista[i+1];
			lis->tam = lis->tam - 1;
		}
		else
			printf("\n DNI no encontrado en la lista.\n");
	}
	else
		printf("\n La lista esta vacia, no hay nada para borrar.\n");
}
void ordena_ascXdni_lis(t_lista *lis)
{
	int i, j;
	t_pers aux;
	if(lis->tam > 1)
	{
		for(i = 2; i <= lis->tam; i++)
		{
			aux = lis->lista[i];
			lis->lista[0] = aux;
			j = i - 1;
			while(det_dni_menor(aux,lis->lista[j]))
			{
				lis->lista[j + 1] = lis->lista[j];
				j--;
			}
			lis->lista[j + 1] = aux;
		}
	}
}
