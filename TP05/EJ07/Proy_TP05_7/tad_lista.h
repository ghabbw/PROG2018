#ifndef TAD_LISTA_H
#define TAD_LISTA_H

#include "tad_pers.h"
#define VECMAX 33

typedef struct
{
	int tam;
	t_pers lista[VECMAX];
}t_lista;

t_lista nueva_lista(void);
int busca_lista(t_lista,long);
void agrega_pers(t_lista*, t_pers);
void muestra_lista(t_lista);
void carga_lista(t_lista*);
void elimina_elemento(t_lista*);
void ordena_ascXdni_lis(t_lista*);

#endif
