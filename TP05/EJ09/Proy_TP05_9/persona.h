#ifndef PERSONA_H
#define PERSONA_H

#include <stdio.h>
#define max 50
typedef char cad[max];
typedef struct{
	long dni;
	cad apenom;
	long ntelfi;
	long ntelce;
}tpersona;

tpersona cargauno(void);
void muestrauno(tpersona);
void modificapersona(tpersona*);
#endif
