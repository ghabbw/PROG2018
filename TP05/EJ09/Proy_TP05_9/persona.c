#include "persona.h"
#include <stdio.h>

tpersona cargauno(){
	tpersona p;
	printf("\nDNI:");
	scanf("%ld",&p.dni);
	
	printf("\nApellido y Nombre:");
	leecad(p.apenom,max);
	
	printf("\nNro. Telefono Fijo:");
	scanf("%ld",&p.ntelfi);
	
	printf("\nNro. Telefono Celular:");
	scanf("%ld",&p.ntelce);
	return p;
	
}

void muestrauno(tpersona p){
	printf("\nDNI:%ld",p.dni);
	printf("\nApellido y Nombre:%s",p.apenom);
	printf("\nNumero de Telefono Celular:%ld",p.ntelce);
	printf("\nNumero de Telefono Fijo:%ld",p.ntelfi);
}

void modificapersona(tpersona *p){
	printf("\n Ingrese los nuevos datos a Modificar:");
	printf("\nDNI:");
	scanf("%ld",p->dni);
	
	printf("\nApellido y Nombre:");
	leecad(p->apenom,max);
	
	printf("\nNro de Celular:");
	scanf("%ld",p->ntelce);
	
	printf("\nNro de Telefono Fijo:");
	scanf("%ld",p->ntelfi);
}
