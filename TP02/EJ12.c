/** Dada una lista de N numeros enteros y dos valores enteros, A y B. Se pide
informar cuantos numeros naturales de la lista son solo multiplos de A y cuantos
son solo multiplos de B. **/

#include <stdio.h>

int main(int argc, char *argv[]) {
	int i, cantN, a, b, contMulA = 0, contMulB = 0, ent, bandA, bandB;
	printf("\n Ingrese la cantidad de numeros enteros:  ");
	scanf("%d",&cantN);
	printf("\n Ingrese A:  ");
	scanf("%d",&a);
	printf("\n Ingrese B:  ");
	scanf("%d",&b);
	
	for(i = 1; i <= cantN; i++)
	{
		bandA = 0;
		bandB = 0;
		printf("\n Ingrese numero entero: ");
		scanf("%d",&ent);
		if((ent%a) == 0)
			bandA = 1;
		if((ent%b) == 0)
			bandB = 1;
		if(bandA == 1 && bandB == 0)
			contMulA++;
		if(bandA == 0 && bandB == 1)
			contMulB++;
	}
	printf("\n\n  Cantidad de numeros multiplos solo de A: %d",contMulA);
	printf("\n  Cantidad de numeros multiplos solo de B: %d",contMulB);
	
	return 0;
}