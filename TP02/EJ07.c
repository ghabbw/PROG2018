/** TP2-7- Calcular la cantidad de horas, minutos y segundos que existen en una cantidad de
segundos ingresada por el usuario. **/

#include <stdio.h>

int main(void)
{
    long int seg = 0;
    printf(" Ingrese la cantidad de segundo:  ");
    scanf("%d",&seg);
    
    printf("\n Cantidad de horas:  %d",seg / 3600);
    seg = seg % 3600;
    printf("\n Cantidad de minutos:  %d",seg / 60);
    seg = seg % 60;
    printf("\n Cantidad de segundos:  %d",seg);
    
    return 0;
}