/** TP2-9- Se desea calcular el importe que debe pagar un automovilista al estacionar su
vehículo en la playa, si es motocicleta, paga el importe correspondiente a la tarifa básica según
cantidad de horas que estacionó en la playa; si es automóvil paga el doble de la tarifa básica y si es
camioneta paga el triple Dada la hora de entrada y de salida de un vehículo indicar el importe a
pagar. La fracción de hora se paga como hora entera. **/

#include <stdio.h>
#define TARIFA 50

int main(void)
{
	int hrEntrada, hrSalida, importe, tipo;
	do{
		do{
			printf("\n Ingrese la opcion:  ");
			printf("\n Cero para salir");
			printf("\n Tipo de vehiculo:  1->moto 2->auto 3->camioneta    ");
			scanf("%d",&tipo);
			if(tipo == 0)
				return 0;
		}while(tipo <0 || tipo > 3);
		printf("\n Ingrese hora de entrada:  ");
		scanf("%d",&hrEntrada);
		printf("\n Ingrese hora de salida:  ");
		scanf("%d",&hrSalida);
		importe = (hrSalida - hrEntrada) * TARIFA * tipo;
		printf("\n Total a pagar %d \n\n----------------",importe);
	}while(1);
	
	return 0;
}