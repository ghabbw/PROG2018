/** TP2-14- Dada una lista de N números, se desea obtener el promedio de los números que
estén formados únicamente con dígitos pares.
a-Considerar que la lista es de números enteros.
b-Considerar que la lista es de números reales positivos.

Rta: Para una lista de numeros reales hay que hacer cambios porque el operador % no funciona
con numeros reales a tal punto que el compilador da error.

Nota: ¿Existe alguna diferencia en el diseño de la solución entre el inciso a y b?. ¿Cómo es el
comportamiento del componente para separar dígitos en el inciso b?. **/

#include <stdio.h>

int main(void)
{
	int N, dig, band, sum = 0, i, entero, cant = 0, aux;
	printf("\n Ingrese la cantidad de numeros a revisar: ");
	scanf("%d",&N);
	for(i=1; i <= N; i++)
	{
		band = 0;
		printf("\n Ingrese el numero entero (%d)", i);
		scanf("%d",&entero);
		aux = entero;
		if(entero != 0)
		{
			dig = entero % 10;
			entero = entero / 10;
			if(dig % 2 != 0)
				band = 1;
			if(band == 1)
				entero = 0;
		}
		if(band == 0)
		{
			sum = sum + aux;
			cant++;
		}
	}
	if(cant != 0)
		printf("\n\n El promedio de los numeros de digitos pares: %.3f", (float) sum/cant);
	else
		printf("\n\n Ningun entero de la lista cumple las condiciones de digitos pares");
	return 0;
}
