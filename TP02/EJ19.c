/** Dados dos números naturales A y B (A<B) y una cantidad no determinada de
números naturales, mostrar:
El menor y el mayor número primo que encuentra dentro del intervalo [A, B].
El porcentaje de números constituidos solamente por dígitos primos.  **/

#include <stdio.h>

int main(void)
{
    int A, B, nat, mayorPrimo, menorPrimo, dig, band, pd, cantNat = 0, cantDigPrim = 0, aux;
	
	printf("\n Ingrese A: ");
	scanf("%d",&A);
	printf("\n Ingrese B: ");
	scanf("%d",&B);
	/* si A < B y los naturales estan en [A y B] */
	mayorPrimo = A - 1;
	menorPrimo = B + 1;
	/* valores ridiculos para mayorPrimo y menorPrimo, listos para el primer ingreso */
	/* tomando a cero como no natural y fin de ingreso */
	do{
		printf("\n Ingrese num natural: ");
		scanf("%d",&nat);
		cantNat++;
		if(nat <= B && nat >= A)
		{
			pd = 2;
			while((pd <= nat / 2) && (nat % pd != 0))
				pd++;
			if((pd > nat / 2) && (nat != 1))
			{
				if(nat > mayorPrimo)
					mayorPrimo = nat;
				if(nat < menorPrimo)
					menorPrimo = nat;
			}
		}
		band = 0;
		aux = nat;
		while(aux != 0)
		{
			dig = aux % 10;
			pd = 2;
			if(dig != 2 && dig != 3 && dig != 5 && dig != 7)
			{
				aux = 0;
				band = 1;
			}
			aux = aux / 10;
		}
		if(band == 0)
			cantDigPrim++;
	}while(nat > 0);
	cantNat--;
	cantDigPrim--;
	printf("\n Menor numero primo entre A y B: %d", menorPrimo);
	printf("\n Mayor numero primo entre A y B: %d", mayorPrimo);
	printf("\n Porcentaje de nums formados solo por primos: %.2f", (cantDigPrim * 100)/(float)cantNat);
	
	return 0;
}