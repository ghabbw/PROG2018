/** 5- Analiza el siguiente programa e indicar y explicar las salidas que produce**/

#include <stdio.h>

int main(void)
{
    int num;
    int* ptr1;
    char a;
    char *ptr2, *ptr3;
    num=25;
    ptr1 = &num;
    printf(" dirección de num %p\n",&num);
    printf("valor de num = %d\n",num);
    printf("direccion de ptr1 = %p\n",&ptr1);
    printf("valor de ptr1 = %p\n",ptr1);
    printf("dato contenido en dirección de ptr1 = %d\n",*ptr1);
    a='L';
    ptr2=&a;
    ptr3=ptr2;
    *ptr3='Z';
    printf("Dato en dirección en ptr3: %c\n",*ptr3);
    printf("Dato en a: %c\n",a);
    return 0;
}