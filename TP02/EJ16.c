/** TP2-16- Dados N caracteres que representan un párrafo, conformados solamente por letras y
espacios en blanco. Se pide informar la cantidad de vocales y la cantidad de consonantes que tiene
el párrafo, también la cantidad de palabras que se encontraron.
Nota: Entre las letras del párrafo no hay vocales acentuadas y las palabras se separan por un
solo espacio en blanco. Considerar que el ingreso se realiza de a un carácter por vez. **/

#include <stdio.h>

int main(void)
{
	int i, cant, cantPal = 0, cantVol = 0, cantCons = 0;
	char letra;
	printf("\n Ingrese cantidad de letras: \n");
	scanf("%d",&cant);
	printf("\n Ingrese la frase, letra por letra:\n");
	for(i = 1; i <= cant; i++)
	{
		fflush(stdin);
		letra = getchar();
		if(letra <= 90)
			letra = letra + 32;
		if(letra == 97 || letra == 101 || letra == 105 || letra == 111 || letra == 117)
			cantVol++;
		else
			if(letra == 64)
				cantPal++;
			else
				cantCons++;
	}
	if(letra != 64)
		cantPal++;
	printf("\nCantidad de palabras: %d, cant de vocales: %d, cant de consonantes: %d", cantPal, cantVol, cantCons);	
	
	return 0;
}