/** Dados N caracteres que representan un párrafo, conformados por letras, dígitos y
espacios en blanco. Se pide informar la cantidad números naturales que se encuentran en el párrafo.
Nota:. Considerar que el ingreso se realiza de a un carácter por vez.  **/