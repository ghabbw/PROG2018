/** TP2-2a- Escribir un programa para cada una de las siguientes situaciones problematicas:
Dado el lado mayor y el lado menor de un rectangulo, mostrar su perimetro y superficie  **/

#include <stdio.h>

int main()
{
    int LadoA, LadoB, resultado;
    printf(" Ingrese un lado del rectangulo:\n");
    scanf("%d",&LadoA);
    printf("\n Ahora ingrese el otro lado:\n");
    scanf("%d",&LadoB);
    
    resultado = (LadoA * 2) + (LadoB * 2);
    printf("\n El perimetro es: %d",resultado);
    resultado = LadoA * LadoB;
    printf("\n La superficie es: %d",resultado);
    return 0;
}

