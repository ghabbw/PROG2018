TP2-6a- Considerando que las variables a y b son enteras y que a contiene el valor 5 y b el valor 17,
indicar los datos resultantes en las variables a y b, luego de la asignación, en cada uno de
los casos:

a=5;b=17;  a=b                      a=17  b=17
a=5;b=17;  a+=b                     a=22  b=17

a=5;b=17;  b+=a                     a=5  b=22
a=5;b=17;  a+=a                     a=10  b=17

a=5;b=17;  b=a++                    a=6  b=5
a=5;b=17;  a/=b                     a=0  b=17

a=5;b=17;  a*=b/a                   a=15  b=17
a=5;b=17;  a-=b                     a=-12  b=17

a=5;b=17;  ++a                      a=6  b=17
a=5;b=17;  a%=b-a*3                 a=1  b=17

a=5;b=17;  a+=b+=a                  a=27  b=22
a=5;b=17;  b=++a                    a=6  b=6

a=5;b=17;  b=a++                    a=6  b=5
a=5;b=17;  a=a<b                    a=1  b=17

a=5;b=17;  a=b<a && b>0 || a!=5     a=0  b=17
a=5;b=17;  b=b>a && (a=7)           a=7  b=1