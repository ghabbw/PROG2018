/** TP2-18- Dada una lista de numeros naturales, se pide contabilizar cuantos de ellos 
son capicuas e indicar los capicuas que estan formados solo por digitos impares**/

#include <stdio.h>
#include <math.h>

int main(void)
{
    int dig, rev, nat, cantCap = 0, cantCapImp = 0, aux, cantDig, band;
    do{
        band = 0;
        rev = 0;
        cantDig = 0;
        printf("\n Ingrese numero natural: ")
        scanf("%d",&nat);
        aux = nat;
        while(aux != 0)
        {
            dig = aux % 10;
            cantDig++;
            aux = aux / 10;
        }
        aux = nat;
        while(aux != 0)
        {
            cantDig--;
            dig = aux % 10;
            rev = rev + (dig * pow(10, cantDig));
            aux = aux / 10;
        }
        aux = nat;
        if(rev == nat)
        {
            cantCap++;
            while(aux != 0)
            {
                dig = aux % 10;
                if(dig % 2)
                    band = 1;
                aux = aux / 10;
                if(band == 1)
                    aux = 0;
            }
            if(band == 0)
                cantCapImp++;
        }
    while(nat != 0);

    return 0;
}