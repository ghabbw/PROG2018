/** TP2-8- Dados tres números naturales que representan los ángulos internos de un triángulo,
se pide determinar si el triángulo es Rectángulo (tiene un ángulo recto, de 90º), Obtusángulo (tiene
un ángulo obtuso, más de 90º) o Acutángulo (tiene tres ángulos agudos, menos de 90º).
Nota: Se debe chequear que la suma de los ángulos interiores sea 180° para procesar el pedido.  **/

#include <stdio.h>

int main(void)
{
    int ang, band = 0, i = 1, sum = 0;
    while(i<=3)
    {
        printf(" Ingrese el %d° angulo:  ",i);
        scanf("%d",&ang);
        i++;
        if(ang > 90 && ang < 180)
            band = 2;
        if(90 == ang)
            band = 1;
        if(ang >= 180 || ang <= 0)
        {
            printf(" Ingreso incorrecto, intentelo nuevamente...\n");
            i--;
        }
        else
            sum = sum + ang;
    }
    if(sum == 180)
    {
        if(band == 1)
            printf("\n Es un rectangulo.");
        if(band == 2)
            printf("\n Es un obtusangulo.");
        if(band == 0)
            printf("\n Es un acutangulo,");
    }
    else
        printf("\n La suma de los angulos interiores no da 180°");
	
	return 0;
}