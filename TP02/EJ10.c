/** TP2-10- Se desea calcular el importe a pagar por el consumo de agua de una familia,
sabiendo que dicho importe se calcula de la siguiente manera:
-un valor básico que cubre los primeros 1000 mts cúbicos o menos, siendo éste un valor único.
-los metros cúbicos que exceden de 1000 tienen una tarifa que se calcula en función del valor de
exceso por cada metro cúbico mayor a 1000, siempre que sea menor a 2000. En caso de que la
cantidad de metros cúbicos consumida exceda o sea igual a 2000 metros cúbicos, el valor por
cada metro cúbico excedido se calcula como el doble del valor de exceso por metro cúbico de
1000.   **/

#include <stdio.h>
#define BASICO 500
#define EXCESO 10

int main(void)
{
	int consumo, tramo2;
	printf("\n Ingrese metros cubicos consumidos:  ");
	scanf("%d",&consumo);
	/* Primer Tramo : basico plano*/
	if(consumo <= 1000)
		printf("Importe a pagar: %d", BASICO);
	/* Segundo Tramo :  basico + consumo */
	if(consumo > 1000 && consumo < 2000)
	{
		tramo2 = BASICO + (consumo - 1000) * EXCESO;
		printf("importe a pagar: %d", tramo2);
	}
	/* Tercer Tramo :  basico + segundo tramo + consumo */
	if(consumo >= 2000)
	{
		tramo2 = BASICO + (999 * EXCESO);
		consumo = consumo - 1999;
		printf("importe a pagar: %d", tramo2 + (consumo * EXCESO * 2));
	}
	
	return 0;
}