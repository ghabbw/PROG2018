/** TP2-11 Dada una lista con las edades de N deportistas, se necesita informar la edad promedio de los
deportistas de la lista; cuál es la menor y mayor edad ingresada.  **/

#include <stdio.h>

int main(void)
{
    int edd, may = 0, men = 100, cantDep, sumEdd = 0,i;
    float prom;
    printf(" \n Ingrese la cantidad de deportistas: ");
    scanf("%d",&cantDep);
    for(i=1; i<=cantDep; i++)
    {
        printf("\n Ingrese edad del alumno(%d) : ",i);
        scanf("%d",&edd);
        if(edd < men)
            men = edd;
        if(edd > may)
            may = edd;
        sumEdd = sumEdd + edd;
    }
    prom = sumEdd / cantDep;
    printf("\n\n  Promedio de edad: %f",prom);
    printf("\n  Edad mayor: %d   ------ Edad Menor: %d",may,men);
	return 0;
}
