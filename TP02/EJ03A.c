/** TP2-3a- Dados los siguientes programas, indicar:
-Datos de entrada y de salida:
    DE: int x,  natural ingresado por el usuario
    DS: int c,  cantidad de naturales que cumplen las condiciones
-Qué hace el programa
    Dada una cantidad no determinada de numeros naturales ingresados por el usuario
    mostrar la cantidad que cumplen la condicion de que todos sus digitos den resto
    cero al dividir la suma de todos los digitos previamente calculada
-Identificar los elementos que lo componen, mediante el uso de comentarios
-Identificar las partes del programa (estructura), utilizando comentarios
-Identificar componentes **/

#include <stdio.h>  /* directivas del preprocesador */

int main()   /* modulo principal */
{
    int x, aux, s, d, b, c;    /* declaracion de variables */
    c=0;     /* inicializacion */
    printf("\n Ingrese natural");   /* Primer ingreso antes del bucle */
    scanf("%d",&x);
    while (x > 0)    /* bucle que controla el ingreso */
    {
        aux = x;     /* inicializacion de aux para no perder el valor ingresado*/
        s = 0;        /* inicializacion de la suma total de los digitos de UNA entrada*/
        while(x != 0)   /* ciclo para descomponer en digitos un numero */
        {
            d = x % 10;
            s = s + d;      /* la suma se va guardando en s */
            x = x / 10;
        }
        x = aux;
        b = 0;            /* inicializacion de bandera para UNA entrada*/
        while ((x != 0) && (b == 0))  /* ciclo que descompone una entrada en digitos*/
        {           /* mientras cumpla la condicion de resto cero para s%d */
            d = x % 10;
            if (d != 0 && s % d != 0)
                b = 1;
            else 
                x = x / 10;
        }
        if (b == 0)   /* comprobacion de bandera */
            c = c + 1;   /* contador */
        printf("\n Ingrese natural");
        scanf("%d",&x);   /* ingreso nuevo */
    }
    if (c == 0)       /* salidas posibles */
        printf("\n Ninguno cumple");
    else
        printf("\n Cumplen %d",c);  /* salida en pantalla de la cantidad */
    return 0;
}