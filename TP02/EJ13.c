/** TP2-3- Dados N números enteros, se desea calcular el promedio de los que terminan en 0 o
en 1, el promedio de los que terminan en 2 o en 3, el promedio de los que terminan en 4 o en 5 y así
sucesivamente hasta el promedio de los que terminan en 8 o en 9.  **/

#include <stdio.h>

int main(void)
{
	int N, entrada, i, sum01 = 0, sum23 = 0, sum45 = 0, sum67 = 0, sum89 = 0,
		cant01 = 0, cant23 = 0, cant45 = 0, cant67 = 0, cant89 = 0;
	printf("\n Ingrese cantidad de enteros:  ");
	scanf("%d",&N);
	
	for(i=1; i<=N; i++)
	{
		printf("\n  Ingrese entero ( %d ) :", i);
		scanf("%d",&entrada);
		if(entrada < 0)
			entrada = entrada * -1;
		if(entrada % 10 == 0 || entrada % 10 == 1)
		{
			sum01 = sum01 + entrada;
			cant01++;
		}
		if(entrada % 10 == 2 || entrada % 10 == 3)
		{
			sum23 = sum23 + entrada;
			cant23++;
		}
		if(entrada % 10 == 4 || entrada % 10 == 5)
		{
			sum45 = sum45 + entrada;
			cant45++;
		}
		if(entrada % 10 == 6 || entrada % 10 == 7)
		{
			sum67 = sum67 + entrada;
			cant67++;
		}
		if(entrada % 10 == 8 || entrada % 10 == 9)
		{
			sum89 = sum89 + entrada;
			cant89++;
		}
	}
	printf("\n------------------------");
	printf("\n Promedio de numeros terminados en 0 y 1: %f", (float)sum01/cant01);
	printf("\n Promedio de numeros terminados en 2 y 3: %f", (float)sum23/cant23);
	printf("\n Promedio de numeros terminados en 4 y 5: %f", (float)sum45/cant45);
	printf("\n Promedio de numeros terminados en 6 y 7: %f", (float)sum67/cant67);
	printf("\n Promedio de numeros terminados en 8 y 9: %f", (float)sum89/cant89);
	
	return 0;
}