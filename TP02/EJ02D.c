/** TP2-2d- Escribir un programa para cada una de las siguientes situaciones problematicas:
    Modificar  el  programa  anterior  para  permitir  calcular  el  salario  de  tres  obreros  y  el  importe 
    total a pagar por el empleador. No utilizar ciclos.  **/
    
    #include <stdio.h>
    #define DIASDELMES 30
    
    int main(void)
    {
        int CantHoras, ValorHora, Acum;
        printf("\n\n PRIMER TRABAJADOR, calculo:");
        printf(" Ingrese cantidad de horas trabajadas por dia:\n");
        scanf("%d",&CantHoras);
        printf("\n Ingrese valor de la hora laboral:\n");
        scanf("%d",&ValorHora);
        printf("\n El importe mensual a pagar (primer trabajador): %d",CantHoras * ValorHora * DIASDELMES);
        Acum = CantHoras * ValorHora * DIASDELMES;
        
        printf("\n\n SEGUNDO TRABAJADOR, calculo:");
        printf(" Ingrese cantidad de horas trabajadas por dia:\n");
        scanf("%d",&CantHoras);
        printf("\n Ingrese valor de la hora laboral:\n");
        scanf("%d",&ValorHora);
        printf("\n El importe mensual a pagar (segundo trabajador): %d",CantHoras * ValorHora * DIASDELMES);
        Acum = (CantHoras * ValorHora * DIASDELMES) + Acum;
        
        printf("\n\n TERCER TRABAJADOR, calculo:");
        printf(" Ingrese cantidad de horas trabajadas por dia:\n");
        scanf("%d",&CantHoras);
        printf("\n Ingrese valor de la hora laboral:\n");
        scanf("%d",&ValorHora);
        printf("\n El importe mensual a pagar (tercer trabajador): %d",CantHoras * ValorHora * DIASDELMES);
        Acum = (CantHoras * ValorHora * DIASDELMES) + Acum;
        
        printf("\n\n El importe total a pagar por los 3 trabajadores: %d",Acum);
        return 0;
    }

}