/** Dado un número natural K de dos cifras, se pide mostrar los números naturales
primos que le anteceden. Por ejemplo si K= 12 la salida será {2, 3, 5, 7, 11). **/

#include <stdio.h>

int main(void)
{
	int i, numK, pd;
	do{
		printf("\n Ingrese num natural K: ");
		scanf("%d",&numK);
	}while(numK < 10 || numK < 0 || numK >= 100);
	printf("\n Lista de primos menores a K: ");
	printf("\n 2");
	for(i =3; i <= numK; i=i+2)
	{
		pd = 2;
		while((pd <= i / 2) && (i % pd != 0))
			pd++;
		if(pd > i / 2)
			printf("\n %d",i);
	}
	
	return 0;
}
