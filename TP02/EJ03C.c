/** Dados los siguientes programas, indicar:
-Datos de entrada y de salida:
    DE: int x,  natural ingresado por el usuario
    DS: mensaje de texto mostrando el mayor y menor numero primo ingresado
-Qué hace el programa
    Dada una cantidad no determinada de numeros naturales ingresados por el usuario,
    evaluar si es un numero primo y determinar el mayor y menor de los ingresados
-Identificar los elementos que lo componen, mediante el uso de comentarios
-Identificar las partes del programa (estructura), utilizando comentarios
-Identificar componentes **/

#include <stdio.h>

int analiza(int);   /* prototipo del modulo analiza */

int main(void)   /* modulo principal */
{
    int x, may, men, b;  /*  declaracion de variables */
    b=0;            /* bandera para guardar la primer entrada */
    printf("Ingrese un número natural: \n");  /* solicitud de numeros naturales */
    scanf("%d",&x);
    while (x>0)   /* ciclo condicionado de entrada */
    {
        if (analiza(x)==1)  /* llamada del modulo analiza */
        {
            if (b==0)           /* chequeo de bandera, primer entrada */
            {
                may=x;
                men=x;
                b=1;
            }
            else            /* actualizacion del mayor y menor primo ingresado */
                if (may<x)
                    may=x;
                else
                    if (men>x)
                        men=x;
        }
        printf("Ingrese un número natural: \n");
        scanf("%d",&x);
    }
    if (b==0)
        printf("\n No se ingresaron primos");    /* salidas, la primera si no hubo primos  */
    else
        printf("El mayor primo natural: %d, el menor primo natural es: %d",may,men);
    return 0;
}

int analiza(int num)    /* modulo determinar de primos, devuelve 1 si lo es*/
{
    int pd, ret;
    pd=2;
    while(pd<=num/2 && num%pd!=0)
        pd++;
    if (pd>num/2 && num!=1)
        ret=1;      /* salidas, 1 si es primo y 0 si no */
    else
        ret=0;
    return ret;
}