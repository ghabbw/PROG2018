/** TP2-2c- Escribir un programa para cada una de las siguientes situaciones problematicas:
    Dada la cantidad de horas que trabaja un obrero por dia y el valor de la hora trabajada,
    indicar el importe que debe cobrar en el mes. Considerar que el mes tiene 30 dias y que los
    dias feriados y los fines de semana se pagan como dias laborales **/

#include <stdio.h>
#define DIASDELMES 30

int main()
{
    int CantHoras, ValorHora;
    printf(" Ingrese cantidad de horas trabajadas por dia:\n");
    scanf("%d", &CantHoras);
    printf("\n Ingrese valor de la hora laboral:\n");
    scanf("%d", &ValorHora);
    
    printf("\n El importe a cobrar en el mes: %d", CantHoras * ValorHora * DIASDELMES);
    return 0;
}