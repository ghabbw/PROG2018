/** TP2-17- Dada una lista de números enteros, determinar la cantidad de sublistas ascendentes
que se encuentran en ella. No se debe considerar un solo elemento como sublista. No se deben
contar sublistas superpuestas.
Ejemplo: 7-5-6-7-4-5-6-7. Tiene 2 (dos) sublistas 5-6-7 y 4-5-6-7   **/

#include <stdio.h>

int main(void)
{
	int entero, cantListas = 0, ant, band = 1;
	do{
		printf("\n Ingrese entero (distinto de cero): ");
		scanf("%d",&entero);
		if(band == 1)
		{
			ant = entero;
			band++;
		}
		else
		{
			if(ant + 1 == entero)
				band++;
			else
				band = 1;
			if(band == 3)
			{
				cantListas++;
				band++;
			}
			ant = entero;
		}
	}while(entero != 0);
	printf("\n Cantidad de listas ascendentes: %d",cantListas);
	
	return 0;
}
