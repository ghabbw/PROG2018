/** Dados los siguientes programas, indicar:
-Datos de entrada y de salida:
    DE: int n,  cantidad de ingresos
        int x,  entero ingresado
    DS: int may,  digito mayor de UN ingreso  
-Qué hace el programa
    Dada una cantidad determinada (n) de numeros enteros ingresados por el 
    usuario, mostrar el digito mayor de cada entrada
-Identificar los elementos que lo componen, mediante el uso de comentarios
-Identificar las partes del programa (estructura), utilizando comentarios
-Identificar componentes **/

#include <stdio.h>              /* directivas del preprocesador */
#include <stdlib.h>      /* libreria para usar abs() */

int may(int);           /*  prototipo del modulo may  */

int main(void)              /* modulo principal */
{
    int i, n, x;        /*  declaracion de variables  */
    printf("Ingrese cantidad de datos: \n");   /* ingreso de cantidad de datos */
    scanf("%d",&n);
    for(i=1;i<=n;i++)       /* ciclo incondicionado */
    {
        printf("Ingrese un dato: \n");      /* solicitacion de datos */
        scanf("%d",&x);
        printf("Digito mayor: %d", may(x));     /* salida por pantalla del
                                                digito mayor de UNA entrada*/
    }

    return 0;
}

int may(int num)     /* implementacion del modulo may */
{
    int d, my=-1;   /* declaracion de variables, my se inicializa 
                        a proposito con un valor ridiculo que sera reemplazado 
                        con el primer digito descompuesto*/
    num=abs(num);   /* conversion a numero positivo para usar el descomponedor */
    while (num!=0)      /* ciclo descomponedor de digitos */
    {
        d=num%10;
        num=num/10;
        if (d>my)       /* reemplaza a my por d si el digito es mayor */
            my=d;
    }
    return my;  /* retorna el digito mayor encontrado */
}