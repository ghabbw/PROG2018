/** TP2-2b- Escribir un programa para cada una de las siguientes situaciones problematicas: 
    Dado el radio de un circulo, mostrar su perimetro y superficie  **/

#include <stdio.h>

#define PI 3.1416

int main()
{
    int radio, resultado;
    printf(" Ingrese el radio del circulo :\n");
    scanf("%d",&radio);
    resultado = radio * 2 * PI;
    printf("\n El perimetro es: %d",resultado);
    resultado = PI * radio * radio;
    printf("\n La superficie es: %d",resultado);
    return 0;
}