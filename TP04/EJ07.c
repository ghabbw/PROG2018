/** TP4-7- Realizar un programa que mediante una estructura de arreglo de dos dimensiones
(NxM) permita almacenar las notas de M exámenes finales de N alumnos, posteriormente retorne la
nota más alta, (se supone única) y por último, calcule la nota promedio de todos los exámenes.  **/

#include <stdio.h>
#define MAXCOL 12
#define MAXFIL 12

typedef int Tmatriz[MAXFIL][MAXCOL];

void cargarMat(int,int,Tmatriz);
void mostrarMat(int,int,Tmatriz)
int mayNota(Tmatriz,int,int);
float promTotal(Tmatriz,int,int);

int main(void)
{
	Tmatriz matA;
	int m, n, opc, entr;
	printf("\n Ingrese datos en la tabla:  (max 10fil y 10col)\n");
	do
	{
		printf("\n Ingrese cantidad de alumnos:  ");
		scanf("%d",&n);
	}while(n <= 0);
	do
	{
		printf("\n Ingrese cantidad de notas finales:  ");
		scanf("%d",&m);
	}while (m <= 0);
	printf("\n Se llenara por columnas\n");
	cargarMat(n,m,matA);
	printf("\n   - - -- - - - - -- -  \n");
	mostrarMat(n,m,matA);
	printf("\n La nota más alta es: %d",mayNota(matA,m,n));
	printf("\n El promedio de las notas es: %f",promTotal(matA,m,n));
	
    return 0;
}
int mayNota(Tmatriz A, int m, int n)
{
	int may = 0, i, j;
	for(i = 1; i <= n; i++)
		for(j = 1; j <= m; j++)
			if(may < A[i][j])
				may = A[i][j];
	return may;
}
float promTotal(Tmatriz A, int m, int n)
{
	int i, j, sum = 0;
	for(i = 1; i <= n; i++)
		for(j = 1; j <= m; j++)
			sum = sum + A[i][j];
	return (float) sum / (n * m);
}
void cargarMat(int n, int m, Tmatriz mat)
{
	int i, j;
	for(i = 1; i <= n; i++)
		for(j = 1; j <= m; j++)
			scanf("%d",&mat[i][j]);
}
void mostrarMat(int n, int m, Tmatriz mat)
{
	int i, j;
	for(i = 1; i <= n; i++)
	{
		printf("\n");
		for(j = 1; j <= m; j++)
			printf("\t  %d ",mat[i][j]);
	}
}