/** TP4-23- Dada una matriz de caracteres que representa una sopa de letras, crear un programa
que permita encontrar un conjunto dado de palabras.
Las palabras en la sopa pueden estar escritas en forma vertical (de izquierda a derecha o al revés) u
horizontal (de arriba hacia abajo o al revés).  **/

#include <stdio.h>
#include <string.h>
#define tamCad 22
#define maxMat 5

typedef char Tmatriz[maxMat][maxMat];
typedef char tcadena[tamCad];

void revCad(tcadena,tcadena,int);
void cargarMat(int,Tmatriz);
void mostrarMat(int,Tmatriz);
void reset(int *full,int *found,int *fil,int *col);
void leeCad(tcadena,int);
int busqCadxC(int,tcadena,Tmatriz,int,int,int);
int busqCadxF(int,tcadena,Tmatriz,int,int,int);
void mostxF(int,int,Tmatriz,int,int);
void mostxC(int,int,Tmatriz,int,int);
int xcolumna(int,tcadena,int,int,int,Tmatriz);
int xfila(int,tcadena,int,int,int,Tmatriz);

int main(void)
{
    tcadena cadena, dac;  /* dac es CADena alreves */
    Tmatriz matA;
    int band = 0, fil = 0, col = 0, full, len, found = 0, lim;
    
    printf("\n Ingrese datos en la tabla:    (max 5f y 5c)\n");
    reset(&full,&found,&fil,&col);
    printf("\n Se llenara por columnas\n");
    cargarMat(maxMat,matA);
    printf("\n   - - -- - - - - -- -  \n");
    mostrarMat(maxMat,matA);
    do
    {
        if(band == 1)
              printf("\n ** La palabra excede el ancho y alto de la matriz **");
        printf("\n Escriba la palabra a buscar: ");
        leeCad(cadena,tamCad);
        len=strlen(cadena);
        band = 1;
    }while(len > maxMat);
    revCad(cadena,dac,len);
    lim = (maxMat - (len - 1)) + 1;  /* limite para no llegar hasta la ultima letra de la fila innecesariamente */
    found=xfila(len,cadena,lim,fil,col,matA);
    found=xfila(len,dac,lim,fil,col,matA);
    found=xcolumna(len,cadena,lim,fil,col,matA);
    found=xcolumna(len,dac,lim,fil,col,matA);
    if(found == 0)
        printf("\n La palabra buscada no fue encontrada");
    return 0;
}
void cargarMat(int n, Tmatriz mat)
{
    int i, j;
    for(i = 0; i < n; i++)
        for(j = 0; j < n; j++)
              {
                  fflush(stdin);
                  scanf("%c",&mat[i][j]);
              }
}
void mostrarMat(int n, Tmatriz mat)
{
    int i, j;
    for(i = 0; i < n; i++)
    {
        printf("\n");
        for(j = 0; j < n; j++)
            printf("\t  %c ",mat[i][j]);
    }
}
void leeCad(tcadena cad, int n)
{
    int j, m;
    m = getchar();
    if(m != EOF)
    {
        j = 0;
        if(m != '\n')
        {
            cad[j] = m;
            j++;
        }
        m = getchar();
        while(m != EOF && m != '\n' && j < n - 1)
        {
            cad[j] = m;
            j++;
            m = getchar();
        }
        cad[j] = '\0';
        while(m != EOF && m != '\n')
            m = getchar();
    }
    else
        cad[0] = '\0';
}
int busqCadxF(int len, tcadena cad, Tmatriz M, int lim, int fil, int col)
{
    int i, aux, band = 0;
        if(M[fil][col] == cad[0])
            {
                i = 1;
                aux = col + 1;   /* +1 y i=1 porque ya se ha evaluado las primeras letras de cad y la fila */
                while(i < len && band == 0 && aux != lim)  /* lleva el paso en cad y en la fila con un aux y una band de corte */
                {
                    if(M[fil][aux] != cad[i])
                        band = 1;
                    else
                    {
                        i++;
                        aux++;
                    }
                }
                if(band == 0 && aux != col + 1)  /* 2 condiciones porque sale del while evaluando 3 */
                    return 1;
            }
    return 0;
}
void revCad(tcadena cad, tcadena dac, int len)
{
    int i = 0,j = len - 1;
    while(cad[i] != '\0')
    {
        dac[j] = cad[i];
        i++;
        j--;
    }
    dac[len] = '\0';
}
int busqCadxC(int len, tcadena cad, Tmatriz M, int lim, int fil, int col)
{
        int i, aux, band = 0;
        if(M[fil][col] == cad[0])
            {
                i = 1;
                aux = fil + 1;   /* +1 y i=1 porque ya se ha evaluado las primeras letras de cad y la fila */
                while(i < len && band == 0 && aux != lim)  /* lleva el paso en cad y en la fila con un aux y una band de corte */
                {
                    if(M[aux][col] != cad[i])
                        band = 1;
                    else
                    {
                        i++;
                        aux++;
                    }
                }
                if(band == 0 && aux != fil+1)  /* 2 condiciones porque sale del while evaluando 3 */
                    return 1;
            }
     return 0;
}
void reset(int *full, int *found, int *fil, int *col)
{
    *full = 0;
    *found = 0;
    *fil = 0;
    *col = 0;
}
void mostxF(int fil, int col, Tmatriz M, int len, int n)
{
    int i, j, k = 1;
    for(i = 0; i < n; i++)
    {
        printf("\n");
        for(j = 0; j < n; j++)
        {
            if(i == fil && j == col && k <= len)
            {
                printf("\t %c ",M[i][j]);
                col++;
                k++;
            }
            else
                printf("\t X ");
        }
    }
    printf("\n  ---------------------------------------------");
}
void mostxC(int fil, int col, Tmatriz M, int len, int n)
{
    int i, j,k = 1;
    for(i = 0; i < n; i++)
    {
        printf("\n");
        for(j = 0; j < n; j++)
        {
            if(i == fil && j == col && k <= len)
            {
                printf("\t %c ",M[i][j]);
                fil++;
                k++;
            }
            else
                printf("\t X ");
        }
    }
    printf("\n  ---------------------------------------------");
}
int xfila(int len, tcadena cadena, int lim, int fil, int col, Tmatriz matA)
{
    int band = 0;
    do
    {
        if(busqCadxF(len,cadena,matA,lim,fil,col) == 1)
        {
            mostxF(fil,col,matA,len,maxMat);
            band = 1;
        }
        col++;
        if(col == lim)
        {
            fil++;
            col = 0;
        }
    }while(fil < maxMat);
    if(band == 1)
        return 1;
    else
        return 0;
}
int xcolumna(int len, tcadena cadena, int lim, int fil, int col, Tmatriz matA)
{
    int band = 0;
    do
    {
        if(busqCadxC(len,cadena,matA,lim,fil,col) == 1)
        {
            mostxC(fil,col,matA,len,maxMat);
            band = 1;
         }
         fil++;
         if(fil == lim)
         {
             col++;
             fil = 0;
         }
    }while(col < maxMat);
    if(band == 1)
        return 1;
    else
        return 0;
}
