/** TP4-13- Dados dos números grandes, tan grandes que no pueden almacenarse en variables de
tipo long se sugiere introducir cada número como una cadena de caracteres y realizar la suma
extrayendo los dígitos de ambas cadenas. **/

#include <stdio.h>
#include <windows.h>
#include <string.h>
#include <stdlib.h>
#define MAX 40

typedef int t_numero[MAX];
const int tam_max=39;
void ingresa_numero(t_numero,int*);
void leeNum(t_numero,int,int *);
void mostrarRestd(t_numero,t_numero,int,int,int);
void sumar(t_numero,int,t_numero,int,t_numero);

int main(void)
{
	char c;
	int cant1, cant2;
	t_numero num1, num2, resultado;

	do
	{
		ingresa_numero(num1,&cant1);
		ingresa_numero(num2,&cant2);
		printf("\n El resultado de la suma es: ");
		sumar(num1,cant1,num2,cant2,resultado);
		printf("\n Desea hacer otra suma?    >S< para confirmar: ");
		c=getchar();
	}while(c == 's' || c == 'S');
	return 0;
}
void ingresa_numero(t_numero num, int *TAM)
{
	printf("\n Introduzca el numero: ");
	fflush(stdin);
	leeNum(num,tam_max,TAM);
}
void leeNum(t_numero numero, int tam, int *tamV)
{
	int j = 0;
	char c;
	c=getchar();
	while (c != EOF && c != '\n' && j < tam - 1)
	{
		numero[j] = c - '0';
		j++;
		c=getchar();
	}
	*tamV = j;
	while(c != EOF && c != '\n')
		c=getchar();
}
void mostrarRestd(t_numero resultado, t_numero num, int cant, int ac, int dif)
{
	int i;
	if(ac == 0)
	{
		for(i = 0; i < dif; i++)
			printf("%d",num[i]);
		for(i = dif + 1; i < cant + 1; i++)
			printf("%d",resultado[i]);
	}
	else
	{
		resultado[0] = 1;
		for(i = 0; i < cant + 1; i++)
			printf("%d",resultado[i]);
	}
}
void sumar(t_numero num1, int cant1, t_numero num2, int cant2, t_numero resultado)
{
	int dif, i, Dig, acarreo;
	acarreo = 0;
	dif = cant1 - cant2;
	if(dif >= 0)
	{
		for(i = cant2 - 1; i >= 0; i--)
		{
			Dig = num1[i+dif] + num2[i] + acarreo;
			if(Dig > 9)
			{
				Dig = Dig % 10;
				acarreo = 1;
			}
			else
				acarreo = 0;
			resultado[i + dif + 1] = Dig;
		}
		if(dif > 0 && acarreo == 1)
		{
			for(i = (dif-1); i >= 0; i--)
			{
				Dig = num1[i] + acarreo;
				if(Dig > 9)
				{
					Dig = Dig % 10;
					acarreo = 1;
				}
				else
					acarreo = 0;
				resultado[i + 1] = Dig;
			}
			dif = dif - 1;
		}
		mostrarRestd(resultado,num1,cant1,acarreo,dif);
	}
	else
	{
		dif = dif * -1;
		for(i = cant1 - 1; i >= 0; i--)
		{
			Dig = num2[i + dif] + num1[i] + acarreo;
			if(Dig > 9)
			{
				Dig = Dig % 10;
				acarreo = 1;
			}
			else
				acarreo = 0;
			resultado[i + dif + 1] = Dig;
		}
		if(dif > 0 && acarreo == 1)
		{
			for(i = (dif - 1); i >= 0; i--)
			{
				Dig = num2[i] + acarreo;
				if(Dig > 9)
				{
					Dig = Dig % 10;
					acarreo = 1;
				}
				else
					acarreo = 0;
				resultado[i + 1] = Dig;
			}
			dif = dif - 1;
		}
		mostrarRestd(resultado,num2,cant2,acarreo,dif);
	}
}