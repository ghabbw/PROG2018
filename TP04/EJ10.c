/** TP4-10- Dado un un número telefónico como cadena, con el siguiente formato: “54 9 387 5 222
222”. Escribir un programa que convierta dicha cadena en dos números de tipo long integer. El primer
número contendrá el código de país y el código de área (549387). El segundo número contendrá el
resto de la cadena (5222222). No usar las funciones de conversión. **/

#include <stdio.h>
/* tuve problemas usando pow() con la libreria math.h porque 10 elevado a 3 me da 999 */
#define cadmax 50

typedef char tcad[cadmax];

void leeCad(tcad,int);
void genera_cads(tcad,tcad,tcad);
int cont_digs(tcad);
long int transformacion(tcad,int);
int eleva(int,int);

int main(void)
{
	long int num1, num2;
	tcad ingreso, cad1, cad2;
	leeCad(ingreso,cadmax);
	printf("%s",ingreso);
	genera_cads(ingreso,cad1,cad2);
	num1 = transformacion(cad1,cont_digs(cad1));
	num2 = transformacion(cad2,cont_digs(cad2));
	printf("\n%li",num1);
	printf("\n%li",num2);
	
	return 0;
}
void leeCad(tcad cdn,int tam)
{
	int j = 0, m;
	while(j < tam -1 && (m = getchar()) != EOF && m != '\n')
	{
		cdn[j] = m;
		j++;
	}
	cdn[j] = '\0';
	if(m != EOF && m != '\n')
		while((m = getchar()) != EOF && m != '\n');
}
void genera_cads(tcad ing,tcad c1,tcad c2)
{
	int i = 0, j = 0, k = 0, m = 0;
	while(i < 3)
	{
		if(ing[j] == ' ')
			i++;
		else
			c1[k++]= ing[j];
		j++;
	}
	c1[k] = '\0';
	while(ing[j] != '\0')
	{
		c2[m++] = ing[j];
		j++;
	}
	c2[m] = '\0';
}
long int transformacion(tcad cdn, int i)
{
	int j = 0, lint = 0;
	while(i > 0)
	{
		i--;
		lint = lint + ((cdn[j] - 48) * eleva(10,i));
		j++;
	}
	return lint;
}
int cont_digs(tcad cdn)
{
	int cant = 0, i = 0;
	while(cdn[i] != '\0')
	{
		cant++;
		i++;
	}
	return cant;
}
int eleva(int base, int exp)
{
	if (exp == 0)
		return 1;
	int res = eleva(base,exp / 2);
	res *= res;
	if (exp % 2 == 1)
		res *= base;
	return res;
}

