/** TP4-6- Dada una tabla de números naturales de M filas y N columnas, realizar un programa que
cargue la tabla y luego, mediante un menú, permita:
a) Eliminar una fila de la matriz.
b) Insertar una nueva fila en una posición p.
c) Insertar una nueva columna en una posición p.
d) Ordenar sólo una fila de la matriz.
e) Ordenar una fila preservando las columnas.
f)  Mostrar la tabla.
Importante: ¿Que pasa si se eliminan todas las filas? **/

#include <stdio.h>
#define MAXCOL 12
#define MAXFIL 12

typedef int Tmatriz[MAXFIL][MAXCOL];

int menu();
void cargarMat(int,int,Tmatriz);
void mostrarMat(int,int,Tmatriz);
void eliminarFila(int*,int,int,Tmatriz);
void agregandoFila(int*,int,int,Tmatriz);
void agregandoColumna(int,int*,int,Tmatriz);
void ordFila(int,int,Tmatriz);
void ordFilaConMat(int,int,int,Tmatriz);
void IntElCambioMat(int,int,int,Tmatriz);

int main(void)
{
	Tmatriz matA,matB;
	int m, n, opc, entr;
	printf("\n Ingrese datos en la tabla:  (max 10fil y 10col)\n");
	do
	{
		printf("\n Ingrese cantidad de filas:  ");
		scanf("%d",&n);
	}
	while(n <= 0);
	do
	{
		printf("\n Ingrese cantidad de columnas:  ");
		scanf("%d",&m);
	}
	while (m <= 0);
	printf("\n Se llenara por columnas\n");
	cargarMat(n,m,matA);
	printf("\n   - - -- - - - - -- -  \n");
	mostrarMat(n,m,matA);
	do
	{
		opc=menu();
		switch(opc)
		{
		case 1:
			if(n>0)
			{
				do
				{
					printf("\n Ingrese la fila a eliminar:");
					scanf("%d",&entr);
				}while(entr > n || entr <= 0);
				eliminarFila(&n,m,entr,matA);
				printf("\n   - - -- - - - - -- -  \n");
				mostrarMat(n,m,matA);
			}
			else printf("\n ***** La matriz ha quedado vacia, imposible mostrar elementos.");
		break;
		case 2:
			do
			{
				printf("\n donde agregar la fila nueva?\n");
				scanf("%d",&entr);
			}while(entr > n + 1 || entr <= 0);
			agregandoFila(&n,m,entr,matA);
			printf("\n   - - -- - - - - -- -  \n");
			mostrarMat(n,m,matA);
		break;
		case 3:
			do
			{
				printf("\n donde agregar la columna nueva?\n");
				scanf("%d",&entr);
			}
			while(entr > m + 1 || entr <= 0);
			agregandoColumna(n,&m,entr,matA);
			printf("\n   - - -- - - - - -- -  \n");
			mostrarMat(n,m,matA);
			break;
		case 4:
			do
			{
				printf("\n Cual fila ordenar?\n");
				scanf("%d",&entr);
			}while(entr > n || entr <= 0);
			ordFila(entr,m,matA);
			printf("\n   - - -- - - - - -- -  \n");
			mostrarMat(n,m,matA);
			break;
		case 5:
			do
			{
				printf("\n Cual fila ordenar (con su respectiva columna)?\n");
				scanf("%d",&entr);
			}while(entr > n || entr <= 0);
			ordFilaConMat(entr,n,m,matA);
			printf("\n   - - -- - - - - -- -  \n");
			mostrarMat(n,m,matA);
			break;
			case 0:
			printf("\n fin del programa");
		}
	}while(opc);

    return 0;
}

int menu()
{
	int opc,band;
	do
	{
		if(band==1) printf("\n Opcion fuera de rango");
		printf("\n Opciones:\n");
		printf("\n 1- Eliminar una fila de la matriz");
		printf("\n 2- Insertar una nueva fila");
		printf("\n 3- Insertar una nueva columna\n");
		printf("\n 4- Ordenar solo una fila de la matriz");
		printf("\n 5- Ordenar una fila preservando las columnas");
		printf("\n 0- SALIR\n");
		printf("\n Ingrese la opcion deseada:  ");
		scanf("%d",&opc);
		band=1;
	}while(opc < 0 || opc > 5);
	return opc;
}
void cargarMat(int n, int m, Tmatriz mat)
{
	int i, j;
	for(i = 1; i <= n; i++)
		for(j = 1; j <= m; j++)
			scanf("%d",&mat[i][j]);
}
void mostrarMat(int n, int m, Tmatriz mat)
{
	int i, j;
	for(i = 1; i <= n; i++)
	{
		printf("\n");
		for(j = 1; j <= m; j++)
			printf("\t  %d ",mat[i][j]);
	}
}
void eliminarFila(int *n, int m, int elim, Tmatriz mat)
{
	int i, j;
	for(i = elim; i <= (*n)-1; i++)
		for(j = 1; j <= m; j++)
			mat[i][j] = mat[i+1][j];
	*n = *n - 1;
}
void agregandoFila(int *n, int m, int agre, Tmatriz mat)
{
	int i, j;
	for(i = (*n); i >= agre; i--)
		for(j = 1; j <= m; j++)
			mat[i+1][j] = mat[i][j];
	printf("\n Ingrese la nueva fila:   (izq->der)\n");
	for(j = 1; j <= m; j++)
		scanf("%d",&mat[agre][j]);
	*n = *n + 1;
}
void agregandoColumna(int n, int *m, int agre, Tmatriz mat)
{
	int i, j;
	for(j = (*m); j >= agre; j--)
		for(i = 1; i <= n; i++)
			mat[i][j+1] = mat[i][j];
	printf("\n Ingrese la nueva columna:   (arriba->abajo)\n");
	for(i = 1; i <= n; i++)
		scanf("%d",&mat[i][agre]);
	*m = *m + 1;
}
void ordFila(int fil, int tam, Tmatriz mat)
{
	int j, p, k, aux;
	for(j = 1; j < tam; j++)
	{
		p = j;
		for(k = j + 1; k <= tam; k++)
			if(	mat[fil][k] < mat[fil][p])
				p = k;
		aux = mat[fil][p];
		mat[fil][p] = mat[fil][j];
		mat[fil][j] = aux;
	}
}
void ordFilaConMat(int fil, int n, int m, Tmatriz mat)
{
	int j, k, aux, i;
	for(i = 1; i < m; i++)
	{
		for(j = i + 1; j <= m; j++)
			if(mat[fil][j] < mat[fil][i])
				for(k = 1; k <= n; k++)
				{
					aux = mat[k][j];
					mat[k][j] = mat[k][i];
					mat[k][i] = aux;
				}
	}
}
