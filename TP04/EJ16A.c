/** TP4-16A- Dados los datos de una canción en formato MP3: Artista, Título y Duración (en
segundos). Realizar un programa que permita: a) Almacenar los datos y modificar la duración de la
canción. **/

#include <stdio.h>
#define cadmax 55

typedef char t_cad[cadmax];

typedef struct
{
	t_cad artista;
	t_cad titulo;
	int duracion;
}t_cancion;

void leeCad(t_cad,int);
t_cancion nueva_can(void);
void mostrar_can(t_cancion);
void cambia_dur(t_cancion*);

int main(void)
{
	t_cancion can01;
	can01 = nueva_can();
	mostrar_can(can01);
	printf("\n\n Ahora puede cambiar la duracion de la cancion:\n");
	cambia_dur(&can01);
	mostrar_can(can01);
	return 0;
}
void leeCad(t_cad cdn, int tam)
{
	int m, j = 0;
	while(j < tam - 1 && (m = getchar()) != EOF && m != '\n')
	{
		cdn[j] = m;
		j++;
	}
	cdn[j] = '\0';
	if(m != EOF && m != '\n')
		while((m = getchar()) != EOF && m != '\n');
}
t_cancion nueva_can(void)
{
	t_cancion c;
	printf("\n Ingrese datos de la cancion: \n");
	printf("\n Ingrese el nombre del artista: \n");
	fflush(stdin);
	leeCad(c.artista,cadmax);
	printf("\n Ingrese el titulo: \n");
	fflush(stdin);
	leeCad(c.titulo,cadmax);
	printf("\n Ingrese la duracion en seg: \n");
	fflush(stdin);
	scanf("%d",&c.duracion);
	return c;
}
void mostrar_can(t_cancion c)
{
	printf("\n Nombre del artista: %s",c.artista);
	printf("\n Titulo de la cancion: %s",c.titulo);
	printf("\n Duracion (seg): %d",c.duracion);
}
void cambia_dur(t_cancion* c)
{
	printf("\n Ingrese la nueva duracion en segundos:\n");
	fflush(stdin);
	scanf("%d",&c->duracion);
}
