/** TP4-1- Dada una lista de N números enteros, se solicita:
a) Cargar la lista en un vector.
b) Dado un elemento por el usuario, indicar cuántas veces se encuentra.
c) Dado un elemento por el usuario, eliminarlo del vector todas las veces que se encuentre.
d) Dado un elemento por el usuario, agregarlo al final de la lista.
e) Mostrar todos los elementos del vector.  **/

#include <stdio.h>
#include <stdlib.h>

#define TAM 50

typedef int tVec[TAM];

void mostrarVec(tVec,int);
void cargarVec(tVec,int);
int freqEnt(tVec,int,int);
void eliminaEnt(tVec,int*,int);
void agregarFinal(tVec,int*,int);

int main(void)
{
	int N, aux;
	tVec listaEnt;
	printf("\n Ingrese cantidad de enteros: ");
	scanf("%d",&N);
	cargarVec(listaEnt, N);
	mostrarVec(listaEnt, N);
	
	printf("\n Ingrese entero a mostrar frequencia: ");
	fflush(stdin);
	scanf("%d",&aux);
	printf("\n Frecuencia del entero pedido: %d",freqEnt(listaEnt, N, aux));
	
	printf("\n Ingrese entero a eliminar: ");
	fflush(stdin);
	scanf("%d",&aux);
	eliminaEnt(listaEnt, &N, aux);
	mostrarVec(listaEnt, N);
	
	printf("\n Ingrese entero para agregar al final: ");
	fflush(stdin);
	scanf("%d",&aux);
	agregarFinal(listaEnt, &N, aux);
	mostrarVec(listaEnt, N);
	
	return 0;
}
void mostrarVec(tVec V, int tamN)
{
	int i;
	for(i = 1; i <= tamN; i++)
	{
		printf(" %d",V[i]);
	}
}
void cargarVec(tVec V, int tamN)
{
	int i;
	for(i = 1; i <= tamN; i++)
	{
		printf("\n Ingrese entero: ");
		scanf("%d",&V[i]);
	}
}
int freqEnt(tVec V, int tamN, int busc)
{
	int i, freq = 0;
	for(i = 1; i <= tamN; i++)
	{
		if(V[i] == busc)
			freq++;
	}
	return freq;
}
void eliminaEnt(tVec V, int *tamN, int elim)
{
	int i = 1, aux;
	while( i <= *tamN)
	{
		if(V[i] == elim)
		{
			aux = i;
			while(aux <= *tamN)
			{
				V[aux] = V[aux + 1];
				aux++;
			}
			*tamN = *tamN - 1;
			i--;
		}
		i++;
	}
}
void agregarFinal(tVec V, int *tamN, int agreg)
{
	*tamN = *tamN + 1;
	V[*tamN] = agreg;
}
