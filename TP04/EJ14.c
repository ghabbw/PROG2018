/** TP4-14- Dadas las coordenadas de un punto en el plano (x,y). Realizar un programa 
que permita la carga de coordenadas de un punto, muestre las coordenadas y permita modificarlas **/

#include <stdio.h>

typedef struct  /* con typedef creamos un tipo de dato nuevo: Coord, en este caso un registro */
{
	int X;
	int Y;
}Coord;

Coord nueva_xy(void);
void modifica_xy(Coord*);
void muestra_xy(Coord);
/* un modulo ingresa() pudo ser agregado para el control, pero el programa es muy simple, es opcional */

int main(void)
{
	Coord c01;    /* c01 es una instancia de Coord, el 01 es para recordar que es solo un item */
	c01 = nueva_xy();  /* c01 recibe los datos desde el modulo nueva_xy() */
	muestra_xy(c01);
	printf("\n\n Modificar mientras las coordenadas sean distintas de (0, 0)\n");
	do
	{
		modifica_xy(&c01);
		muestra_xy(c01);
	}while(c01.X != 0 && c01.Y != 0);
	
	return 0;
}
void modifica_xy(Coord *c)  /* el registro llega al modulo por referencia, modifica el original */
{
	printf("\n Ingrese las NUEVAS coordenadas :");
	printf("\n Nuevo valor de 'x' :  ");
	scanf("%d",&c->X);  /* se usan -> porque el registro a modificar no esta en el modulo */
	printf("\n Nuevo valor de 'y' :  ");
	scanf("%d",&c->Y);
}
void muestra_xy(Coord c)
{
	printf("\n ###  Coordenadas: (%d, %d)  ###\n",c.X,c.Y);
}
Coord nueva_xy(void)  
{
	Coord c;
	printf("\n\n Ingrese las coordenadas del plano:");
	printf("\n Valor de 'x' :  ");
	scanf("%d",&c.X);     /* se usa puntos porque el registro esta en el mismo modulo */
	printf("\n Valor de 'y' :  ");
	scanf("%d",&c.Y);
	return c;    /* retorna el primer registro completo */
}
