/** TP04-3- Dadas dos listas, de números enteros, que contienen la altura en cm de los niños de
primer grado, sala A y sala B, se solicita calcular la altura que más se repite de cada sala (altura
modal) suponiendo que es única, luego determinar, en cada sala, cuántos niños tienen dicha altura.
Posteriormente eliminar de las listas la mayor altura modal, todas las veces que aparece. **/

#include <stdio.h>

#define TAM 50

typedef int tVec[TAM];

void muestraVec(tVec,int);
void mayFreq(tVec,int,int*,int*);
void purgaVec(tVec,int*,int);

int main(void)
{
	int salaA = 7, salaB = 8, mayFreqA, mayFreqB, altFreqA, altFreqB;
	int listaAltA[] = {135,122,135,111,107,122,135}, listaAltB[] = {111,133,120,142,123,103,114,111};
	
	printf("\n");
	muestraVec(listaAltA, salaA);
	printf("\n");
	muestraVec(listaAltB, salaB);
	printf("\n");
	mayFreq(listaAltA,salaA,&mayFreqA,&altFreqA);
	mayFreq(listaAltB,salaB,&mayFreqB,&altFreqB);
	printf("\n Altura que mas se repite en el salon A: %d cms",altFreqA);
	printf("\n Se repite unas %d veces.",mayFreqA);
	printf("\n Altura que mas se repite en el salon B: %d cms",altFreqB);
	printf("\n Se repite unas %d veces.",mayFreqB);
	purgaVec(listaAltA,&salaA,altFreqA);
	purgaVec(listaAltB,&salaB,altFreqB);
	printf("\n");
	muestraVec(listaAltA, salaA);
	printf("\n");
	muestraVec(listaAltB, salaB);
	
	return 0;
}
void muestraVec(tVec V, int tamN)
{
	int i;
	for(i = 0; i < tamN; i++)
	{
		printf(" %d",V[i]);
	}
}
void mayFreq(tVec V, int tamV, int *may, int *alt)
{
	int i, num, pos, j, posMay;
	int aux[TAM];
	for(i = 0; i < tamV; i++)
		aux[i] = 0;
	for(i = 0; i < tamV; i++)
	{
		num = V[i];
		pos = i;
		for(j = i; j < tamV; j++)
			if(V[j] == num)
				aux[pos]++;
	}
	*may = aux[0];
	posMay = 0;
	for(i = 0; i < tamV; i++)
	{
		if(aux[i] > *may)
		{
			posMay = i;
			*may = aux[i];
		}
	}
	*alt = V[posMay];
}
void purgaVec(tVec V,int *tamN, int purg)
{
	int i = 0,j;
	while(i < *tamN)
	{
		if(V[i] == purg)
		{
			j = i;
			while(j < *tamN)
			{
				V[j] = V[j + 1];
				j++;
			}
			*tamN = *tamN - 1;
		}
		i++;
	}
}
