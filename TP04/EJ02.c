/** TP4-2- Dada una lista de N caracteres, se solicita:
a) Cargar la lista en un vector.
b) Ordenar el vector de menor a mayor.
c) Dado un caracter por el usuario, buscarlo en la lista e informar su posición.
d) Dado un caracter por el usuario, insertarlo donde corresponda manteniendo el orden.
e) Mostrar todos los elementos del vector que no sean una vocal.  **/

#include <stdio.h>
#include <ctype.h>

#define TAM 50

typedef char tVec[TAM];

void muestraVec(tVec,int);
void cargaVec(tVec,int);
int buscaCaracter(tVec,int,char);
void ordenaCharsAsc(tVec,int);
void insertaVec(tVec,int*,char);
void muestraVec_sin_voc(tVec,int);
int confirmaVocal(char);

int main(void)
{
	int N;
	char c;
	tVec listaChar;
	printf("\n Ingrese cantidad de enteros: ");
	fflush(stdin);
	scanf("%d",&N);
	cargaVec(listaChar, N);
	muestraVec(listaChar, N);
	
	printf("\n Ordenando de menor a mayor: ");
	ordenaCharsAsc(listaChar, N);
	muestraVec(listaChar, N);
	
	printf("\n Ingrese caracter a buscar: ");
	fflush(stdin);
	scanf("%c",&c);
	printf("\n Posicion del caracter pedido: %d   < cero es NO hallado >",buscaCaracter(listaChar, N, c));
	
	printf("\n Ingrese caracter a insertar: ");
	fflush(stdin);
	scanf("%c",&c);
	insertaVec(listaChar, &N, c);
	muestraVec(listaChar, N);
	
	printf("\n Mostrando lista sin vocales:");
	muestraVec_sin_voc(listaChar, N);
	
	return 0;
}
void muestraVec(tVec V, int tamN)
{
	int i;
	for(i = 1; i <= tamN; i++)
	{
		printf(" %c",V[i]);
	}
}
void cargaVec(tVec V, int tamN)
{
	int i;
	for(i = 1; i <= tamN; i++)
	{
		printf("\n Ingrese caracter: ");
		fflush(stdin);
		scanf("%c",&V[i]);
	}
}
int buscaCaracter(tVec V, int tamN, char busc)
{
	int i = 1, band = 0;
	while( i <= tamN && band == 0)
	{
		if(V[i] == busc)
		{
			band = 1;
			i--;
		}
		i++;
	}
	if(band == 1)
		return i;
	return 0;
}
void ordenaCharsAsc(tVec V, int tamN)
{
	int i, j, aux;
	if(tamN > 1)
	{
		for(i = 2; i <= tamN; i++)
		{
			aux = V[i];
			V[0] = aux;
			j = i - 1;
			while(aux < V[j])
			{
				V[j + 1] = V[j];
				j--;
			}
			V[j + 1] = aux;
		}
	}
}
void insertaVec(tVec V, int *tamN, char c)
{
	*tamN = *tamN + 1;
	V[*tamN] = c;
	ordenaCharsAsc(V, *tamN);
}
void muestraVec_sin_voc(tVec V,int tamN)
{
	int i;
	for(i = 1; i <= tamN; i++)
	{
		if(!confirmaVocal(V[i]))
			printf(" %c",V[i]);
	}	
}
int confirmaVocal(char c)
{
	c = toupper(c);
	if(c == 65 || c == 69 || c == 73 || c == 79 || c == 85)
		return 1;
	return 0;
}
