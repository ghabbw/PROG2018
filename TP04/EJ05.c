/** TP4-5- Dada una lista que contiene notas, entre 0 y 100, que obtuvieron N alumnos en un
examen parcial. Se solicita compactarla eliminando los repetidos, pero insertando a derecha de cada
número su frecuencia de aparición. Ejemplo: si la lista es 0, 90, 76, 0, 67, 90, 90; la lista compactada
será 0, 2, 90, 3, 76, 1, 67, 1.
A partir de la lista compactada calcular la moda. Si la moda es única, indicar si es mayor, menor o
igual a la media aritmética.
Moda: es el valor con mayor frecuencia
Media Aritmética: es el promedio de un conjunto de números, a 1 , a 2 , a 3 , . . ., a n , obtenida sumando
todos los números y dividiéndola entre la cantidad de números sumados. **/

#include <stdio.h>
#define TAM 150

typedef int tvec[TAM];

void CargaVec(tvec, int);
void ElimElem(tvec, int, int*);
int BusquedaSec(tvec, int, int);
int Frecuencia(tvec, int);
void Inserta(tvec, int,int, int*);
void mostrar(tvec, int);
float MediaAritmetica(tvec, int);
void Depuracion(tvec,int,int,int*);
int cantModa(tvec,int);
int UnicaModa(tvec,int,int);
int Moda(tvec,int,int);

int main(void)
{
	int N, i = 1, moda;
	float mediaArit;
	tvec	 notas;
	printf(" \n Ingrese cantidad: ");
	scanf("%d",&N);
	CargaVec(notas, N);
	while(i <= N)
	{
		Depuracion(notas,notas[i],i,&N);
		i = i + 2;
	}
	mostrar(notas,N);
	if(UnicaModa(notas,N,cantModa(notas,N)) == 1)
	{
		moda = Moda(notas,N,cantModa(notas,N));
		mediaArit = MediaAritmetica(notas,N);
		printf("\n La media aritmetica es: %f",mediaArit);
		printf("\n La moda es: %d",moda);
		if(mediaArit > moda)
			printf("\n La moda es menor a la media aritmetica");
		if(mediaArit < moda)
			printf("\n La moda es mayor a la media aritmetica");
		else
			printf("\n La moda es igual a la media aritmetica");
	}
	else
		printf("\n La moda no es unica.");
	return 0;
}
void CargaVec(tvec V, int N)
{
	int i;
	for(i = 1; i <= N; i++)
	{
		printf("...ingrese: ");
		scanf("%d",&V[i]);
	}
}
void mostrar(tvec V, int N)
{
	int i;
	for(i=1; i<=N; i++)
	{
		printf(" %d",V[i]);
	}
}
void ElimElem(tvec V, int pos, int *N)
{
	int i;
	for(i = pos; i <= (*N)-1; i++)
		V[i] = V[i + 1];
	*N-=1;
}
void Inserta(tvec V, int pos, int ins, int* N)
{
	int i;
	for(i = (*N); i >= pos; i--)
	{
		V[i + 1]=V[i];
	}
	V[pos]=ins;
	(*N)++;
}
void Depuracion(tvec notas,int bus,int ind,int *N)
{
	int cont,aux;
	cont=0;
	aux=ind;
	while(ind <= (*N))
	{
		if(bus == notas[ind])
		{
			if(cont>0)
			{
				ElimElem(notas,ind,N);
				ind--;
			}
			cont++;
		}
		ind++;
	}
    Inserta(notas,aux+1,cont,N);
}
float MediaAritmetica(tvec V, int tam)
{
	int i, ac = 0, cant = 0;
	for(i = 1; i <= tam; i++)
	{
		if(i % 2 == 1)
			ac = ac + (V[i] * V[i + 1]);
		else
			cant = cant + V[i];
	}
	return ac/(float)cant;
}
int cantModa(tvec V,int tam)
{
	int may, band = 0, i;
	for(i = 2; i <= tam; i = i + 2)
	{
		if(band == 0)
		{
			may = V[i];
			band = 1;
		}
		else
		{
			if(V[i] > may)
				may = V[i];
		}
	}
	return may;
}
int UnicaModa(tvec V, int tam, int cantMod)
{
	int i = 2, cont = 0;
	while(i <= tam)
	{
		if(V[i] == cantMod)
			cont++;
		if(cont == 2)
			i = tam;
		i = i + 2;
	}
	if(cont == 1)
		return 1;
	return 0;
}
int Moda(tvec V, int tam, int cantMod)
{
	int i = 2, band = 0;
	while(i <= tam && band == 0)
	{
		if(V[i] == cantMod)
			band = 1;
		i = i + 2;
	}
	return V[i - 3];
}