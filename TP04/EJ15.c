/** TP4-15- Realizar un programa que solicite los siguientes datos de 3 personas: nombre, día de
nacimiento, mes de nacimiento y año de nacimiento. Después deberá preguntar un número de mes y
mostrar en pantalla los datos de las personas que cumplan año durante ese mes. **/

#include <stdio.h>
#define cadmax 50

typedef char t_cad[cadmax];

typedef struct
{
	t_cad nombre;
	int dia_nac;
	int mes_nac;
	int anio_nac;
}t_pers;

void leeCad(t_cad,int);
t_pers ini_pers(void);
int check_cumplemes(t_pers,int);
void muestra_datos(t_pers);

int main(void)
{
	int mes, band = 0;
	t_pers pers01, pers02, pers03;
	
	pers01 = ini_pers();
	muestra_datos(pers01);
	pers02 = ini_pers();
	muestra_datos(pers02);
	pers03 = ini_pers();
	muestra_datos(pers03);
	
	printf("\n Ingrese un mes:  (en numeros)\n");
	fflush(stdin);
	scanf("%d",&mes);
	band = check_cumplemes(pers01,mes);
	band = check_cumplemes(pers02,mes);
	band = check_cumplemes(pers03,mes);
	if(band == 0)
		printf("\n Nadie cumplia anios en el mes %d",mes);

	return 0;
}
void leeCad(t_cad cdn, int tam)
{
	int j = 0, m;
	while(j < m - 1 &&  (m = getchar()) != EOF && m != '\n')
	{
		cdn[j] = m;
		j++;
	}
	cdn[j] = '\0';
	if(m != EOF && m != '\n')
		while((m = getchar()) != EOF && m != '\n');
}
t_pers ini_pers(void)
{
	t_pers aux;
	printf("\n Ingrese datos:\n");
	printf("\n Ingrese nombre:   ");
	fflush(stdin);
	leeCad(aux.nombre,cadmax);
	printf("\n Ingrese dia de nac:   ");
	fflush(stdin);
	scanf("%d",&aux.dia_nac);
	printf("\n Ingrese mes de nac:   ");
	fflush(stdin);
	scanf("%d",&aux.mes_nac);
	printf("\n Ingrese anio de nac:   ");
	fflush(stdin);
	scanf("%d",&aux.anio_nac);
	return aux;
}
int check_cumplemes(t_pers p, int mes)
{
	if(p.mes_nac == mes)
	{
		muestra_datos(p);
		return 1;
	}
	return 0;
}
void muestra_datos(t_pers p)
{
	printf("\n\n%s",p.nombre);
	printf("\n%d",p.dia_nac);
	printf("\n%d",p.mes_nac);
	printf("\n%d\n",p.anio_nac);
}
