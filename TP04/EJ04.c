/** TP4-4- El profe de computación le ha pedido a sus alumnos de primer grado que ingresen N
letras del abecedario en la computadora (uno a la vez). Realiza un programa que permita para cada
alumno: cargar las N letras, ordenar el listado, verificar si ingresó la letra “h” y contar cuántos lo
hicieron, por último permitir agregar una nueva letra en el orden correcto. **/

#include <stdio.h>
#include <stdlib.h>
#define tammax 50

typedef char vect[tammax];

void ordena_lista(vect,int);
void mostrar_lista(vect,int);
void ins_lista(vect,int,char);
int cont_caract(vect,int,char);

int main(void)
{
	int N, resp,m = 1, k, contH = 0;
	char nue, c = 'h';
	vect V;
	printf("\n Ingrese la cantidad de letras por alumno: \n");
	scanf("%d",&N);
	do
	{
		system("CLS");
		printf("\n Alumno (%d) ingresa las siguientes letras:\n",m);
		for(k = 1; k <= N; k++)
		{
			fflush(stdin);
			printf("-------\n");
			scanf("%c",&V[k]);
		}
		printf("\n Lista SIN ordenar: ");
		mostrar_lista(V,N);
		printf("\n Lista ordenada: ");
		ordena_lista(V,N);
		mostrar_lista(V,N);
		printf("\n Fueron ingresadas, %d haches.",cont_caract(V,N,c));  /* habia entendido mal la consigna, 
		esta pide cuantos alumnos ingresaron alguna H, y no cuantas Hs ingresó cada alumno */
		if(cont_caract(V,N,c))  /* sin embargo, reutilizo el modulo creado, ya que devuelve un numero 
		distinto de cero si hay por lo menos una H, entonces lo puedo usar como una bandera para un contador */
		    contH++;
		printf("\n El alumno puede ingresar un nuevo caracter (se ordenara)");
		fflush(stdin);
		scanf("%c",&nue);
		ins_lista(V,N,nue);
		mostrar_lista(V,N + 1);
		
		printf("\n\n Otro alumno? ingrese 1 para continuar:  ");
		fflush(stdin);
		scanf("%d",&resp);
		m++;
	}while(resp == 1);
	printf("\n\n Un total de %d alumnos ingresaron por lo menos una H.",contH);
	
	return 0;
}
void ordena_lista(vect V, int N)
{
	int i, j, aux;
	for(i = 2; i <= N; i++)
	{
		aux = V[i];
		V[0] = aux;
		j = i - 1;
		while(aux < V[j])
		{
			V[j + 1] = V[j];
			j--;
		}
		V[j + 1] = aux;
	}
}
void mostrar_lista(vect V,int N)
{
	int i;
	for(i = 1; i <= N; i++)
		printf(" %c",V[i]);
}
void ins_lista(vect V, int N, char nuevo)
{
	int i, j, aux;
	N++;
	V[N] = nuevo;
	for(i = 2; i <= N; i++)
	{
		aux = V[i];
		V[0] = aux;
		j = i - 1;
		while(aux < V[j])
		{
			V[j + 1] = V[j];
			j--;
		}
		V[j + 1] = aux;
	}
}
int cont_caract(vect V, int N, char bus)
{
	int i, cont = 0;
	for(i = 1; i <= N; i++)
		if(V[i] == bus)
			cont++;
	return cont;
}
