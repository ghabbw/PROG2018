/** TP4-8- Escriba un programa que simule la reservación y cancelación de asientos en un teatro.
La sala cuenta con 50 asientos. Cada asiento será identificado por un número dentro de la fila y la
fila la que se encuentre (ej., asiento 3 fila 4); una vez reservado el asiento, no estará disponible para
reserva (a no ser que se cancele la reservación). Las opciones mínimas que debe presentar el menú
son: a). Reservación de Asientos, b). Cancelación de Reservación, c). Estado Actual de la
Disposición (asientos libres y asientos reservados), d). Salir.  
NOTA: no usé una matriz, sino un solo vector porque todavia no habian dado ese tema todavia... **/

#include <stdio.h>
#include <stdlib.h>
#define tammax 51  /* por comenzar el vector en 1 */

typedef char vect[tammax];

void inicia_asientos(vect,char);
void mostrar_asientos(vect);
void cancelacion(vect,char,char);
int menu(void);
int asientos_libres(vect,char);
void reservacion(vect,char,char);

int main(void)
{
	int op;
	char c = 'O', x = 'X';
	vect V;
	inicia_asientos(V,c);
	mostrar_asientos(V);
	do
	{
		switch(op = menu())
		{
			case 1:
				printf("\n Opcion 1:\n");
				if(asientos_libres(V,c)) /* uso asientos_libres() como bandera, si devuelve 0 esta lleno */
				{
					reservacion(V,c,x);
					mostrar_asientos(V);
				}
				else
					printf("\n >Teatro lleno< no se pueden reservar mas asientos.\n");
			break;
			case 2:
				printf("\n Opcion 2:\n");
				cancelacion(V,c,x);
				mostrar_asientos(V);
			break;
			case 3:
				printf("\n Opcion 3:");
				system("CLS");
				mostrar_asientos(V);
				printf("\n Quedan %d/50 asientos libres.\n",asientos_libres(V,c));
			break;
			case 4:
				printf("\n Fin del programa.");
			break;
			default:
				printf("\n !!! Opcion incorrecta\n");
		}
	}while(op != 4);

	return 0;
}
void mostrar_asientos(vect V)
{
	int i, col, fil = col = 1;
	printf("\n FILA-COLUMNA[estado]    estado: O disponible\n");
	for(i = 1; i < tammax; i++)
	{
		printf("%d-%d[%c] ",fil,col,V[i]);
		col++;
		if(i % 10 == 0)
		{
			printf("\n");
			fil++;
			col = 1;
		}
	}
}
void inicia_asientos(vect V, char c)
{
	int i;
	for(i = 1; i <= tammax; i++)
		V[i] = c;
}
int menu(void)
{
	int elex;
	printf("\n Opciones: ");
	printf("\n <1> Reservar asiento.");
	printf("\n <2> Cancelar reservacion.");
	printf("\n <3> Estado actual.");
	printf("\n <4> SALIR.");
	printf("\n\n Ingrese su eleccion: ");
	scanf("%d",&elex);
	
	return elex;
}
void reservacion(vect V, char c, char x)
{
	int fil, col, aux;
	do
	{
		printf("\n Infrese fila: ");
		fflush(stdin);
		scanf("%d",&fil);
	}while(fil < 1 || fil > 5);
	do
	{
		printf("\n Infrese columna: ");
		fflush(stdin);
		scanf("%d",&col);
	}while(col < 1 || col > 10);
	aux = col + ((--fil) * 10);
	if(V[aux] == c)
		V[aux] = x;
	else
		printf("\n <!> Asiento ya reservado <!>\n");
}
int asientos_libres(vect V, char c)
{
	int i, cant = 0;
	for(i = 1; i <= tammax; i++)
		if(V[i] == c)
			cant++;
	return cant;
}
void cancelacion(vect V, char c, char x)
{
	int aux, fil, col;
	printf("\n Ingrese el lugar a cancelar");
	do
	{
		printf("\n Infrese fila: ");
		fflush(stdin);
		scanf("%d",&fil);
	}while(fil < 1 || fil > 5);
	do
	{
		printf("\n Infrese columna: ");
		fflush(stdin);
		scanf("%d",&col);
	}while(col < 1 || col > 10);
	aux = col + ((--fil) * 10);
	if(V[aux] == x)
		V[aux] = c;
	else
		printf("\n El asiento ya estaba libre.\n");
}