/** TP4-17- Un banco mantiene la información de sus clientes en un array de registros, ordenados
por número de cuenta. De cada cliente se almacena la siguiente información:
●  Número de cuenta,
●  Nombre del cliente,
●  Domicilio (de tipo cadena de caracteres),
●  Saldo de la cuenta.
Se solicita escribir un programa que permita mediante un menú:
a) Realizar un depósito (es decir, dado un monto a depositar y una cuenta, actualizar el saldo).
b) Realizar un retiro (es decir, dado un monto a retirar y una cuenta, actualizar el saldo).  **/

#include <stdio.h>
#include <math.h>   /* Para usar fabs() */
#define cadmax 55
#define listamax 20   /* cantidad max de clientes que la lista maneja */

typedef char t_cad[cadmax];

typedef struct
{
	int Num_cuenta;
	t_cad Nomb_cliente;
	t_cad Dom_cliente;
	float saldo;
}t_cliente;

typedef t_cliente t_lista[listamax];

void leeCad(t_cad,int);
void llena_lista(t_lista,int*);
void muestra_lista(t_lista,int);
void muestra_cliente(t_cliente);
void ordena_selex(t_lista,int);
void depositando(t_lista,int);
void extrayendo(t_lista,int);
int busca_cliente(t_lista,int,int);
t_cliente carga_cliente(void);
int menu(void);

int main(void)
{
	int tamLista = 0, opc;
	t_lista list;

	printf("\n Creando lista de clientes: \n");
	llena_lista(list,&tamLista);
	ordena_selex(list,tamLista);
	muestra_lista(list,tamLista);
	do
	{
		opc=menu();
		switch(opc)
		{
			case 1:
				printf("\n Haciendo un deposito:\n");
				depositando(list,tamLista);
			break;
			case 2:
				printf("\n Haciendo una extraccion:\n");
				extrayendo(list,tamLista);
			break;
			default:
				printf("\n  Fin del Programa \n");
		}
	}while(opc);
		
	return 0;
}
void leeCad(t_cad cdn, int tam)
{
	int m, j = 0;
	while(j < tam - 1 && (m = getchar()) != EOF && m != '\n')
	{
		cdn[j] = m;
		j++;
	}
	cdn[j] = '\0';
	if(m != EOF && m != '\n')
		while((m = getchar()) != EOF && m != '\n');
}
void llena_lista(t_lista list, int* tam)
{
	int entrd;
	printf("\n Ingresando datos de la lista:\n");
	do
	{
		if(*tam < listamax) /* cuida que la lista no se sature */
		{
			*tam = *tam + 1;
			list[*tam] = carga_cliente();
			printf("\n Ingrese el numero '1' para agregar un nuevo cliente:\n");
			fflush(stdin);
			scanf("%d",&entrd);
		}
		else
		{
			printf("\n !!! La lista esta en su maxima capacidad. !!!\n");
			entrd = 0;
		}
	}while(entrd == 1);
}
t_cliente carga_cliente(void)
{
	t_cliente c;
	printf("\n Ingrese numero de cuenta:   ");
	fflush(stdin);
	scanf("%d",&c.Num_cuenta);
	printf("\n Ingrese nombre del cliente:   ");
	fflush(stdin);
	leeCad(c.Nomb_cliente,cadmax);
	printf("\n Ingrese el domicilio del cliente: ");
	fflush(stdin);
	leeCad(c.Dom_cliente,cadmax);
	printf("\n El saldo del cliente:   ");
	fflush(stdin);
	scanf("%f",&c.saldo);
	return c;
}
void muestra_cliente(t_cliente c)
{
	printf("\n Numero de cliente: %d",c.Num_cuenta);
	printf("\n Nombre del cliente: %s",c.Nomb_cliente);
	printf("\n Domicilio del cliente: %s",c.Dom_cliente);
	printf("\n Saldo del cliente: $ %.2f\n\n",c.saldo);
}
void muestra_lista(t_lista lis,int tam)
{
	int i;
	for(i = 1; i <= tam; i++)
		muestra_cliente(lis[i]);
}
int menu(void)
{
	int rta,band;
	band=0;
	do
	{
		if(band == 1)
			printf("\n * !! Opcion fuera de rango !! * \n");
		printf("\n\n >1< Realizar un deposito\n");
		printf(" >2< Realizar una extraccion\n" );
		printf(" -- - - -- - -- \n");
		printf(" <0> SALIR\n");
		printf(" Elija su opcion: ");
		scanf("%d",&rta);
		band = 1;
	}while(rta < 0 || rta > 2);
	return rta;
}
void ordena_selex(t_lista lis,int cantc)
{           /* busqueda por seleccion, la mas simple */
	t_cliente aux;
	int i,j;
	for(i=1; i<=cantc-1; i++)
	{
		for(j=i+1; j<=cantc; j++)
		{
			if(lis[j].Num_cuenta < lis[i].Num_cuenta)
			{
				aux=lis[j];
				lis[j]=lis[i];
				lis[i]=aux;
			}
		}
	}
}
void depositando(t_lista list, int tam)
{
	int entrd, busc;
	float deposito;
	printf("\n Ingrese el numero de cliente para hacer el deposito:  ");
	fflush(stdin);
	scanf("%d",&entrd);
	busc = busca_cliente(list,tam,entrd);
	if(busc)
	{
		printf("\n Ingrese el mondo a depositar:   $ ");
		fflush(stdin);
		scanf("%f",&deposito);
		deposito = fabs(deposito); /* se hace uso de fabs porque entrar un signo seria catastrofico */
		list[busc].saldo = list[busc].saldo + deposito;
		printf("    ======= Deposito exitoso =======");
		muestra_cliente(list[busc]);
		printf("    ================================");
	}	
	else
	{
		muestra_lista(list,tam);
		printf("\n El numero de cliente no existe en la lista.!!!\n");
	}
}
void extrayendo(t_lista list,int tam)
{
	int entrd, busc;
	float extraccion;
	printf("\n Ingrese el numero de cliente para hacer la extraccion:  ");
	fflush(stdin);
	scanf("%d",&entrd);
	busc = busca_cliente(list,tam,entrd);
	if(busc)
	{
		printf("\n Ingrese el mondo a extraer:   $ ");
		fflush(stdin);
		scanf("%f",&extraccion);
		extraccion = fabs(extraccion);
		list[busc].saldo = list[busc].saldo - extraccion;
		printf("    ======= Extraccion exitosa =======");
		muestra_cliente(list[busc]);
		printf("    ==================================");
	}	
	else
	{
		muestra_lista(list,tam);
		printf("\n El numero de cliente no existe en la lista.!!!\n");
	}
}
int busca_cliente(t_lista list, int tam, int buscado)
{        /* como la lista esta ordenada por num de cuenta, se usa busqueda binaria */
	int ini = 1,m;
	m = (ini + tam) / 2;
	while(ini <= tam && list[m].Num_cuenta != buscado)
	{
		if(list[m].Num_cuenta > buscado)
			tam--;
		else
			ini++;
		m = (ini + tam) / 2;
	}
	if(ini <= tam)
		return m;
	return 0;
}
