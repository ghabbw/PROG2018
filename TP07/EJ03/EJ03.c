/** TP07-EJ3 - Implementar un programa correctamente modularizado que permita administrar una
lista de elementos. Los elementos de la lista son números enteros.
	Se debe permitir realizar con cada elemento:
	a) Crear un nuevo elemento (tener en cuenta la posibilidad de crear elementos pidiendo 
	el número al usuario por teclado y pasando el número por parámetro)
	b) Mostrar un elemento
	c) Comparar dos elementos
	d) Saber si un elemento es par
	e) Saber si un elemento es impar
	f) Saber si un elemento es primo
	g) Sucesor de un elemento

	La lista de números enteros debe estar implementada con una lista simplemente enlazada y debe
	permitir realizar las siguientes operaciones con la lista:
	a) Crear una nueva lista (se crea una lista vacía)
	b) Mostrar la lista
	c) Retornar el tamaño de la lista
	d) Verificar si la lista es vacía
	e) Agregar un número al inicio de la lista
	f) Agregar un número al final de la lista
	g) Agregar un número en una determinada ubicación
	h) Dado un número indicar si el número pertenece a la lista
	i) Retornar el elemento que se encuentra en una determinada ubicación
	j) Eliminar el primer elemento de la lista
	k) Eliminar el último elemento de la lista
	l) Eliminar la primera aparición de un número dentro de la lista
	m) Eliminar todas las apariciones de un número dentro de la lista
	n) Eliminar un número en una determinada ubicación
	o) Dado un número eliminar la primera ocurrencia del número dentro de la lista
	p) Dado un número eliminar todas las ocurrencias de ese número de la lista
	q) Agregar una lista al final de otra lista
	r) Hacer una copia de la lista
	s) Hacer una copia invertida de la lista
	Nota: Implementar un menú que permita ejecutar todas las operaciones excepto iniciar lista que se
	debe realizar una única vez al inicio del programa. **/
#include <stdio.h>
#include <stdlib.h>
#define CHAR_SALIDA 48  /* ascii corresponde al cero */
#define CHAR_ERR 666  /* entrada interna, cod interno */
typedef struct Nodo
{
	int ent;
	struct Nodo * sig;
}t_nodo;
typedef t_nodo * pntrNodo;
int menu(void);
/* Operaciones con elementos */
pntrNodo nuevo_elem(int);
void muestra_elemento(pntrNodo);
int compara_nodos(pntrNodo,pntrNodo);
int check_par(pntrNodo);
int check_impar(pntrNodo);
int check_primo(pntrNodo);

int sucesor_elem(pntrNodo);
/* Operaciones con lista */
pntrNodo nueva_lista(void);
pntrNodo copia_lista(pntrNodo);
pntrNodo copia_lista_invertida(pntrNodo);
pntrNodo crea_lista_copia(pntrNodo);
int ret_elem_pos(pntrNodo,int);
int tam_lista(pntrNodo);
int check_vacia_lista(pntrNodo);
int check_elem_lista(pntrNodo,int);
void muestra_lista(pntrNodo);
void muestra_nodo_pos(pntrNodo,int);
void carga_lista(pntrNodo*);
void agrega_elem_ini(pntrNodo*,pntrNodo);
void agrega_elem_fin(pntrNodo*,pntrNodo);
void agrega_ini(pntrNodo*,int);
void agrega_fin(pntrNodo*,int);
void agrega_elem_pos(pntrNodo*,pntrNodo,int);
void elimina_ini(pntrNodo*);
void elimina_fin(pntrNodo*);
void elimina_frst_aparicion(pntrNodo*,pntrNodo); /* no sabia si recibir un entero o recibir un puntero a un nodo */
void purga_lista(pntrNodo*,int);
void elimina_pos(pntrNodo*,int);
void elimina_frst_ocurrencia(pntrNodo,int);
void elimina_all_ocurrencias(pntrNodo,int);
void une_listas_ini_fin(pntrNodo*,pntrNodo);
void muestra_dato_x_pos(pntrNodo,int);
void elimina_all(pntrNodo*);

int main(void)
{
	pntrNodo aux1, aux2, lista = NULL, lista02 = NULL;
	int opc, nuevo, pos, buscado, tam;
	do
	{
		opc = menu();
		system("CLS");
		fflush(stdin);
		switch(opc)
		{
			case 1:	/* a */
				printf("\n Ingrese dato de un nuevo elemento: ");
				scanf("%d",&nuevo);
				aux1 = nuevo_elem(nuevo);
				printf("\n Mostrando el elemento creado:   ");
				muestra_elemento(aux1);
			break;
			
			case 2:	/* b */
				printf("\n Mostrando elemento unico:   ");
				muestra_elemento(aux1);
			break;
			
			case 3:	/* c */
				printf("\n Comparando dos elementos, ingrese primero  : ");
				scanf("%d",&nuevo);
				aux1 = nuevo_elem(nuevo);
				
				printf("\n Comparando dos elementos, ingrese segundo  : ");
				scanf("%d",&nuevo);
				aux2 = nuevo_elem(nuevo);
				
				if(compara_nodos(aux1,aux2) == 1)
					printf("\n El primero es mayor al segundo ingresado.");
				if(compara_nodos(aux1,aux2) == -1)
					printf("\n El primero es menor al segundo ingresado.");
				if(compara_nodos(aux1,aux2) == 0)
					printf("\n Son iguales ambos.");
			break;
			
			case 4:	/* d */
				printf("\n Ingrese dato para ver si es par:   ");
				scanf("%d",&nuevo);
				aux1 = nuevo_elem(nuevo);
				if(check_par(aux1) == 1)
					printf("\n El dato es par.");
				else
					printf("\n El dato NO es par");
			break;
			
			case 5:	/* e */
				printf("\n Ingrese dato para ver si es impar:   ");
				scanf("%d",&nuevo);
				aux1 = nuevo_elem(nuevo);
				if(check_impar(aux1) == 1)
					printf("\n El dato es impar.");
				else
					printf("\n El dato NO es impar");
			break;
			
			case 6:	/* f */
				printf("\n Ingrese dato para ver si es primo:   ");
				scanf("%d",&nuevo);
				aux1 = nuevo_elem(nuevo);
				if(check_primo(aux1) == 1)
					printf("\n El dato es primo.");
				else
					printf("\n El dato NO es primo");
			break;
			
			case 7:	/* g */
				printf("\n Ingrese dato para mostrar su sucesor:   ");
				scanf("%d",&nuevo);
				aux1 = nuevo_elem(nuevo);
				printf("\n El sucesor es: %d",sucesor_elem(aux1));
			break;
			
			case 8:	/* A */
				printf("\n Nueva lista inicializada.");
				lista = nueva_lista();
				lista02 = nueva_lista();
			break;
			
			case 9:	/* B */
				printf("\n Mostrando lista:");
				muestra_lista(lista);
			break;
			
			case 10:	/* C */
				printf("\n Tamanho de la lista:  %d",tam_lista(lista));
			break;
			
			case 11:	/* D */
				printf("\n Chequeando que si la lista esta vacia.");
				if(check_vacia_lista(lista))
					printf("\n La lista esta vacia.");
				else
					printf("\n La lista NO esta vacia.");
			break;
			
			case 12:	/* E */
				printf("\n Ingrese dato de un nuevo elemento a insertar desde el inicio: ");
				scanf("%d",&nuevo);
				aux1 = nuevo_elem(nuevo);
				agrega_elem_ini(&lista,aux1);
			break;
			
			case 13:	/* F */
				printf("\n Ingrese dato de un nuevo elemento a insertar desde el final: ");
				scanf("%d",&nuevo);
				aux1 = nuevo_elem(nuevo);
				agrega_fin(&lista,aux1->ent);
			break;
			
			case 14:	/* G */
				printf("\n Ingrese dato para insertar:  ");
				scanf("%d",&nuevo);
				aux1 = nuevo_elem(nuevo);
				printf("\n Ingrese posición donde insertar:  ");
				scanf("%d",&pos);
				agrega_elem_pos(&lista,aux1,pos);
			break;
			
			case 15:	/* H */
				printf("\n Ingrese dato para buscar en la lista:  ");
				scanf("%d",&buscado);
				if(check_elem_lista(lista,buscado))
					printf("\n El numero %d, si pertenece a la lista.",buscado);
				else
					printf("\n El numero %d, NO pertenece a la lista.",buscado);
			break;
			
			case 16:	/* I */
				printf("\n Ingrese la posicion en la lista a mostrar:   ");
				scanf("%d",&pos);
				tam = tam_lista(lista);
				if(tam >= pos && tam <= 0)
					muestra_dato_x_pos(lista,pos);
				else
					printf("\n La posicion buscada NO esta fuera de rango.");
			break;
			
			case 17:	/* J */
				printf("\n Eliminando primer elemento de la lista.");
				elimina_ini(&lista);
			break;
			
			case 18:	/* K */
				printf("\n Eliminando ultimo elemento de la lista.");
				elimina_fin(&lista);
			break;
			
			case 19:	/* L */
				printf("\n Ingrese dato para eliminar su primera aparicion de la lista: ");
				scanf("%d",&nuevo);
				aux1 = nuevo_elem(nuevo);
				elimina_frst_aparicion(&lista,aux1);
			break;
			
			case 20:	/* M */
				printf("\n Ingrese dato para purgar la lista: ");
				scanf("%d",&nuevo);
				purga_lista(&lista,nuevo);
				if(lista == NULL)
					printf("\n La lista quedo vacia.");
			break;
			
			case 21:	/* N */
				printf("\n Ingrese posicion a eliminar en la lista: ");
				scanf("%d",&pos);
				elimina_pos(&lista,pos);
			break;
			
			case 22:	/* O */
				printf("\n Ingrese dato para eliminar su primera ocurrencia en lista. ");
				scanf("%d",&buscado);
				elimina_frst_ocurrencia(lista,buscado);
			break;
			
			case 23:	/* P */
				printf("\n Ingrese dato para eliminar todas sus ocurrencias en lista. ");
				scanf("%d",&buscado);
				elimina_all_ocurrencias(lista,buscado);
			break;
			
			case 24:	/* Q */
				elimina_all(&lista);
				elimina_all(&lista02);
				printf("\n Cargue la lista 1: ");
				carga_lista(&lista);
				printf("\n Cargue la lista 2: ");
				carga_lista(&lista02);
				une_listas_ini_fin(&lista,lista02);
				printf("\n Mostrando la lista 2 unida a la cola de la lista 1: ");
				muestra_lista(lista);
			break;
			
			case 25:	/* R */
				elimina_all(&lista);
				elimina_all(&lista02);
				lista = nueva_lista();
				printf("\n Para hacer una copia cargue la lista origen: ");
				carga_lista(&lista);
				printf("\n mostrando lista a copiar");
				muestra_lista(lista);
				lista02 = crea_lista_copia(lista);
				printf("\n Copia de la lista: ");
				muestra_lista(lista02);
			break;
			
			case 26:	/* S */
				elimina_all(&lista);
				elimina_all(&lista02);
				lista = nueva_lista();
				printf("\n Para hacer una copia cargue la lista origen: ");
				carga_lista(&lista);
				lista02 = copia_lista_invertida(lista);
				printf("\n Copia invertida de la lista: ");
				muestra_lista(lista02);
			break;
			
			case CHAR_SALIDA:
				printf("\n  Fin del Programa.\n");
			break;
			default:
				printf("\n Entrada fuera de rango\n");
		}
	}while(opc != CHAR_SALIDA);
	//free(lista02);
	elimina_all(&lista);
	elimina_all(&lista02);
	free(aux1);
	free(aux2);
	return 0;
}
pntrNodo nuevo_elem(int entrada)
{
	pntrNodo nuevo = NULL;
	nuevo = (t_nodo*) malloc(sizeof(t_nodo));
	if(nuevo != NULL)
	{
		nuevo->ent = entrada;
		nuevo->sig = NULL;
	}
	return nuevo;
}	/* Usado en a c d e f g E F G L */
void muestra_elemento(pntrNodo list)
{
	printf("\n Entero: %d",list->ent);
}	/* Usado en  a b*/
int compara_nodos(pntrNodo list01, pntrNodo list02)
{
	if(list01->ent > list02->ent)
		return 1;
	if(list01->ent < list02->ent)
		return -1;
	return 0;
}	/* Usado en c */
int check_par(pntrNodo list)
{	/* devuelve 1 si es par */
	return (list->ent % 2) + 1;
}	/* Usado en d */
int check_impar(pntrNodo list)
{
	return list->ent % 2;
}	/* Usado en e */
int check_primo(pntrNodo list)
{
	int pd = 2;
	while(pd <= (list->ent / 2) && (list->ent % pd) != 0)
	{
		pd++;
	}
	if(pd > (list->ent / 2) && list->ent != 1	)
		return 1;
	return 0;
}	/* Usado en f */
int sucesor_elem(pntrNodo list)
{
	return list->ent + 1;
}	/* Usado en g */
pntrNodo nueva_lista(void)
{
	pntrNodo nuevo = NULL;
	return nuevo;
}	/* Usado en A R S */
void muestra_lista(pntrNodo list)
{
	if(list != NULL)
	{
		while(list != NULL)
		{
			printf("\n Dato = %d",list->ent);
			list = list->sig;
		}
	}
	else
	{
		printf("\n Lista vacia.");
	}
}	/* Usado en B Q R S */
int tam_lista(pntrNodo list)
{
	int cont = 0;
	while(list != NULL)
	{
		list = list->sig;
		cont++;
	}
	return cont;
}	/* Usado en C I */
int check_vacia_lista(pntrNodo list)
{	/* si la lista esta vacia retorna 1 */
	if(list == NULL)
		return 1;
	return 0;
}	/* Usado en D */
int ret_elem_pos(pntrNodo list, int bus)
{	/* uso bandera o dejo asi? */
	int pos = 0;
	while(list != NULL)
	{
		pos++;
		if(bus == list->ent)
			return pos;
		list = list->sig;
	}
	return pos;
}
pntrNodo copia_lista(pntrNodo list)
{
	pntrNodo cop = NULL, aux = NULL;
	int band = 0;
	while(list != NULL)
	{
		aux = nuevo_elem(list->ent);
		if(aux != NULL)
		{
			if(band == 0)
			{
				cop = aux;
				band = 1;
			}
			else
			{
				while(list != NULL)
				{
					agrega_fin(&cop,list->ent);
					list = list->sig;
				}
			}
			aux->sig = list->sig;
			
		}
		else
		{
			printf("\n Error al reservar memoria.");
		}
	}
	return cop;
}	/* No usado */
void carga_lista(pntrNodo *list)
{
	int i, cant;
	pntrNodo aux = NULL;
	printf("\n Ingrese la cantidad de elementos a agregar (por el final): ");
	fflush(stdin);
	scanf("%d",&cant);
	for(i = 1; i <= cant; i++)
	{
		fflush(stdin);
		aux = NULL;
		printf("\n Elemento num %d a agregar:  ",i);
		aux = (t_nodo*) malloc(sizeof(t_nodo));
		if(aux != NULL)
		{
			scanf("%d",&aux->ent);
			aux->sig = NULL;
			agrega_elem_fin(list,aux);
		}
		else
		{
			printf("\n Error al reservar memoria en carga...");
		}
	}
	printf("\n fin de ingresos");
}	/* Usado en Q R S */
pntrNodo copia_lista_invertida(pntrNodo list)
{
	pntrNodo copia_inv = NULL;
	while(list != NULL)
	{
		agrega_ini(&copia_inv,list->ent);
		list = list->sig;
	}
	return copia_inv;
}	/* Usado en S */
void agrega_ini(pntrNodo * lis, int dato)
{	/* el orden es importante */
	pntrNodo aux = NULL;
	aux = nuevo_elem(dato);
	if(*lis != NULL && aux != NULL)
	{
		aux->sig = *lis;
		*lis = aux;
	}
	if(*lis == NULL && aux != NULL)
	{
		*lis = aux;
	}
}
void agrega_fin(pntrNodo * lis, int dato)
{
	pntrNodo aux = NULL, aux2 = NULL;
	aux = nuevo_elem(dato);
	if(*lis != NULL && aux != NULL)
	{
		for(aux2 = *lis; aux2->sig != NULL; aux2 = aux2->sig)
		{
			;
		}
		aux2->sig = aux;
	}
	if(*lis == NULL && aux != NULL)
	{
		*lis = aux;
	}
}	/* Usado en F */
void agrega_elem_ini(pntrNodo *list, pntrNodo aux)
{
	if(*list == NULL)
	{
		printf("\n La lista estaba vacia");
		*list = aux;
	}
	else
	{
		aux->sig = *list;
		*list = aux;
	}
}
void agrega_elem_fin(pntrNodo *list, pntrNodo aux)
{
	pntrNodo ant;
	if(*list == NULL)
	{
		*list = aux;
	}
	else
	{
		ant = *list;
		while(ant->sig != NULL)
		{
			ant = ant->sig;
		}
		ant->sig = aux;
	}
}
void agrega_elem_pos(pntrNodo * list, pntrNodo nod, int pos)
{
	pntrNodo aux, ANT;
	int cont = 0, dif;
	aux = * list;
	while(aux != NULL)
	{
		cont++;
		aux = aux->sig;
	}
	dif = cont - pos;
	if(dif == 0 || dif == -1 || dif == cont || dif == (cont - 1))
	{
		if(dif == 0)
		{
			aux = * list;
			while(aux->sig != NULL)
			{
				ANT = aux;
				aux = aux->sig;
			}
			ANT->sig = nod;
			nod->sig = aux;
		}
		if(dif == -1)
		{
			aux = * list;
			while(aux != NULL)
			{
				ANT = aux;
				aux = aux->sig;
			}
			ANT->sig = nod;
			nod->sig = NULL;
		}
		if(dif == cont || dif == (cont - 1))
		{
			nod->sig = * list;
			* list = nod;
		}
	}
	else
	{
		cont = 1;
		aux = *list;
		if(aux != NULL)
		{
			while(aux->sig != NULL && cont < pos)
			{
				ANT = aux;
				aux = aux->sig;
				cont++;
			}
			if(aux->sig != NULL)
			{
				ANT->sig = nod;
				nod->sig = aux;
			}
			else
			{
				printf("\n Posicion fuera de rango.");
			}
		}
		else
		{
			printf("\n Lista vacia.");
		}
	}
}	/* Usado en G */
int check_elem_lista(pntrNodo list, int bus)
{	/* devuelve 1 si el numero ingresado esta en la lista */
	int band = 0;
	while(list != NULL && band == 0)
	{
		if(list->ent == bus)
		{
			band = 1;
		}
		list = list->sig;
	}
	return band;
}	/* Usado en H */
void elimina_ini(pntrNodo * lis)
{
	pntrNodo aux;
	if(*lis == NULL)
	{
		printf("\nLa lista estaba vacia");
	}
	else
	{
		if((*lis)->sig == NULL)
		{
			free(*lis);
			*lis = NULL;
		}
		else
		{
			aux = *lis;
			*lis = (*lis)->sig;
			free(aux);
		}
	}
}	/* Usado en J */
void elimina_fin(pntrNodo * lis)
{
	pntrNodo  aux, ant;
	if(*lis == NULL)
	{
		printf("\nLa lista estaba vacia.");
	}
	else
	{
		if((*lis)->sig == NULL)
		{
			free(*lis);
			*lis = NULL;
		}
		else
		{
			for(aux = (*lis)->sig; aux->sig != NULL; aux = aux->sig)
			{
				ant = aux;
			}
			free(aux);
			ant->sig = NULL;
		}
	}
}	/* Usado en K */
void elimina_frst_aparicion(pntrNodo * list, pntrNodo bus)
{
	pntrNodo aux = *list, anterior;
	if(*list != NULL)
	{
		if(compara_nodos((*list),bus) == 0)
		{	/* cero si son iguales */
			(*list) = (*list)->sig;
			free(aux);
		}
		else
		{
			while(aux != NULL && (compara_nodos(aux,bus) != 0))
			{
				anterior = aux;
				aux = aux->sig;
			}
			if(aux != NULL)
			{
				anterior->sig = aux->sig;
				free(aux);
			}
			else
			{
				printf("\n Elemento no encontrado.");
			}
		}
	}
	else
	{
		printf("\n La lista estaba vacia.");
	}
}	/* Usado en L */
pntrNodo crea_lista_copia(pntrNodo list)
{
	int band = 0;
	pntrNodo nuevo = NULL, aux = NULL, copia = NULL;
	while(list != NULL)	
	{	/* el orden de los ifs es importante */
		if(band > 1)
		{
			nuevo = nuevo_elem(list->ent);
			if(nuevo != NULL)
			{
				aux = copia;
				while(aux->sig != NULL)
				{
					aux = aux->sig;
				}
				aux->sig = nuevo;
			}
			else
			{
				printf("\n Error en asignacion de memoria------");
			}
		}
		if(band == 1)
		{
			nuevo = nuevo_elem(list->ent);
			if(nuevo != NULL)
			{
				copia->sig = nuevo;
				band++;
			}
			else
			{
				printf("\n Error en asignacion de memoria----");
			}
		}
		if(band == 0)
		{
			nuevo = nuevo_elem(list->ent);
			if(nuevo != NULL)
			{
				copia = nuevo;
				band++;
			}
			else
			{
				printf("\n Error en asignacion de memoria--");
			}
		}
		list = list->sig;
	}
	return copia;
}	/* Usado en R */
void muestra_nodo_pos(pntrNodo list, int pos)
{
	int cont = 0;
	while(cont <= pos)
	{
		cont++;
		list = list->sig;
	}
	muestra_elemento(list);
}
void purga_lista(pntrNodo * lis, int prg)
{
	pntrNodo ant = * lis, aux, Aux2;
	if(* lis == NULL)
	{
		printf("\n La lista estaba vacia.");
	}
	else
	{
		if((*lis)->sig == NULL && (*lis)->ent == prg)
		{
			printf("\n lista de 1 elem y se elimina por coincidir.");
			aux = *lis;
			*lis = NULL;
			free(aux);
		}
		else
		{
			pntrNodo aux = (*lis)->sig;
			while(aux != NULL)
			{	/* si hay un solo elemen y no es prg, aux = NULL al inicializrl */
				if(aux->ent != prg)
				{
					ant = aux;
					aux = aux->sig;
				}
				else
				{
					ant->sig = aux->sig;
					Aux2 = aux;
					free(Aux2);
					aux = ant->sig;
				}
			}
			if((*lis)->ent == prg)
			{
				  elimina_ini(lis);
			}
		}
	}
}	/* Se usa en M */
void elimina_pos(pntrNodo * lis, int pos)
{
	pntrNodo aux, ANT = NULL, Aux2;
	aux = *lis;
	int cont = 1;
	while((aux != NULL) && (cont != pos))
	{
		ANT = aux;
		aux = aux->sig;
		cont++;
	}
	if(ANT != NULL)
	{
		if(aux == NULL)
		{
			printf("\n posicion no encontrada");
		}
		else
		{
			Aux2 = aux;
			free(Aux2);
			ANT->sig = aux->sig;
		}
	}
	else
	{
		Aux2 = *lis;
		free(Aux2);
		*lis = (*lis)->sig;
	}
}	/* Se usa en N */
void elimina_frst_ocurrencia(pntrNodo list, int prg)
{
	int cont = 0;
	pntrNodo anterior;
	while(list != NULL && cont < 2)
	{
		if(list->ent == prg)
			cont++;
		anterior = list;
		list = list->sig;
	}
	if(cont == 2)
		anterior->sig = list->sig;
	else
		printf("\n No se reunen las condiciones necesarias para una eliminacion.");
	if(list == NULL)
		printf("\n Lista vacia.");
}	/* Se usa en O */
void elimina_all_ocurrencias(pntrNodo list, int prg)
{
	int cont = 0;
	pntrNodo anterior, Ant2;
	while(list != NULL && cont < 2)
	{
		if(list->ent == prg)
			cont++;
		anterior = list;
		list = list->sig;
	}
	if(cont >= 2)
		while(list != NULL)
		{
			if(list->ent == prg)
			{
				anterior->sig = list->sig;
				Ant2 = list;
				free(Ant2);
			}
			anterior = list;
			list = list->sig;
		}
	else
		printf("\n No hubo eliminaciones.");
}	/* Se usa en P */
void une_listas_ini_fin(pntrNodo * lis01, pntrNodo lis02)
{
	if(*lis01 != NULL)
	{
		(*lis01)->sig = lis02;
	}
	else
	{
		*lis01 = lis02;
	}
}	/* Se usa en Q */
void muestra_dato_x_pos(pntrNodo lista,int pos)
{
	int i;
	for(i = 1; i < pos; i++)
	{
		lista = lista->sig;
	}
	muestra_elemento(lista);
}	/* Se usa en I */
void elimina_all(pntrNodo * lista)
{
	pntrNodo aux = * lista, Aux2;
	if(aux != NULL)
	{
		while(aux != NULL)
		{
			Aux2 = aux;
			aux = aux->sig;
			free(Aux2);
		}
	}
	else
	{
		printf("\n La lista estaba vacia.");
	}
}	/* Se usa al final del programa */
int menu(void)
{
	char c;
	printf("\n ____OPERACIONES CON ELEMENTOS____");
	printf("\n a- Crear nuevo elemento                 b- Mostrar un elemento");
	printf("\n c- Comparar dos elementos               d- Saber si un elemento es par");
	printf("\n e- Saber si un elemento es impar        f- Saber si un elemento es primo");
	printf("\n g- Sucesor de un elemento");
	printf("\n ____OPERACIONES CON LISTAS____");
	printf("\n A- Crear lista (una sola vez)                 B- Mostrar lista");
	printf("\n C- Retorna tamanio de la lista                D- Verificar si la lista esta vacia");
	printf("\n E- Agregar numero al inicio de la lista       F- Agregar numero al final de la lista");
	printf("\n G- Agregar numero en una posicion             H- Indicar si un ingreso pertenece a la lista");
	printf("\n I- Retornan un elemento que se encuentra en una ubicacion ingresada");
	printf("\n J- Eliminar el primer elemento de la lista");
	printf("\n K- Eliminar el ultimo elemento de la lista");
	printf("\n L- Eliminar la primera aparicion de un numero dentro de la lista");
	printf("\n M- Eliminar todas las apariciones de un numero dentro de la lista");
	printf("\n N- Eliminar un numero en determinada ubicacion");
	printf("\n O- Ingresar un numero para eliminar su primera ocurrencia");
	printf("\n P- Ingresar un numero para eliminar todas sus ocurrencias");
	printf("\n Q- Agregar una lista al final de otra lista");
	printf("\n R- Hacer una copia de la lista");
	printf("\n S- Hacer una copia inversa de la lista");
	printf("\n INGRESE CERO PARA SALIR.");
	printf("\n Ingrese opcion segun el menu:  ");
	fflush(stdin);
	c = getchar();
	if(c >= 97 && c <= 103)
		return c - 96;
	if(c >= 65 && c <= 83)
		return c - 57;
	if(c == CHAR_SALIDA)
		return 48;
	return CHAR_ERR;
}
