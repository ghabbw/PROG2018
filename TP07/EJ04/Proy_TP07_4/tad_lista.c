#include "tad_lista.h"
pntrNodo nueva_lista(void)
{
	pntrNodo nuevo = NULL;
	return nuevo;
}
void muestra_lista(pntrNodo list)
{
	if(list != NULL)
	{
		while(list != NULL)
		{
			printf("\n Dato = %d",list->ent);
			list = list->sig;
		}
	}
	else
	{
		printf("\n Lista vacia.");
	}
}
int tam_lista(pntrNodo list)
{
	int cont = 0;
	while(list != NULL)
	{
		list = list->sig;
		cont++;
	}
	return cont;
}
int check_vacia_lista(pntrNodo list)
{	/* si la lista esta vacia retorna 1 */
	if(list == NULL)
		return 1;
	return 0;
}
int ret_elem_pos(pntrNodo list, int bus)
{	/* uso bandera o dejo asi? */
	int pos = 0;
	while(list != NULL)
	{
		pos++;
		if(bus == list->ent)
			return pos;
		list = list->sig;
	}
	return pos;
}
pntrNodo copia_lista(pntrNodo list)
{
	pntrNodo cop = NULL, aux = NULL;
	int band = 0;
	while(list != NULL)
	{
		aux = nuevo_elem(list->ent);
		if(aux != NULL)
		{
			if(band == 0)
			{
				cop = aux;
				band = 1;
			}
			else
			{
				while(list != NULL)
				{
					agrega_fin(&cop,list->ent);
					list = list->sig;
				}
			}
			aux->sig = list->sig;
			
		}
		else
		{
			printf("\n Error al reservar memoria.");
		}
	}
	return cop;
}	/* No usado */
void carga_lista(pntrNodo *list)
{
	int i, cant;
	pntrNodo aux = NULL;
	printf("\n Ingrese la cantidad de elementos a agregar (por el final): ");
	fflush(stdin);
	scanf("%d",&cant);
	for(i = 1; i <= cant; i++)
	{
		fflush(stdin);
		aux = NULL;
		printf("\n Elemento num %d a agregar:  ",i);
		aux = (t_nodo*) malloc(sizeof(t_nodo));
		if(aux != NULL)
		{
			scanf("%d",&aux->ent);
			aux->sig = NULL;
			agrega_elem_fin(list,aux);
		}
		else
		{
			printf("\n Error al reservar memoria en carga...");
		}
	}
	printf("\n fin de ingresos");
}
pntrNodo copia_lista_invertida(pntrNodo list)
{
	pntrNodo copia_inv = NULL;
	while(list != NULL)
	{
		agrega_ini(&copia_inv,list->ent);
		list = list->sig;
	}
	return copia_inv;
}
void agrega_ini(pntrNodo * lis, int dato)
{	/* el orden es importante */
	pntrNodo aux = NULL;
	aux = nuevo_elem(dato);
	if(*lis != NULL && aux != NULL)
	{
		aux->sig = *lis;
		*lis = aux;
	}
	if(*lis == NULL && aux != NULL)
	{
		*lis = aux;
	}
}
void agrega_fin(pntrNodo * lis, int dato)
{
	pntrNodo aux = NULL, aux2 = NULL;
	aux = nuevo_elem(dato);
	if(*lis != NULL && aux != NULL)
	{
		for(aux2 = *lis; aux2->sig != NULL; aux2 = aux2->sig)
		{
			;
		}
		aux2->sig = aux;
	}
	if(*lis == NULL && aux != NULL)
	{
		*lis = aux;
	}
}
void agrega_elem_ini(pntrNodo *list, pntrNodo aux)
{
	if(*list == NULL)
	{
		printf("\n La lista estaba vacia");
		*list = aux;
	}
	else
	{
		aux->sig = *list;
		*list = aux;
	}
}
void agrega_elem_fin(pntrNodo *list, pntrNodo aux)
{
	pntrNodo ant;
	if(*list == NULL)
	{
		*list = aux;
	}
	else
	{
		ant = *list;
		while(ant->sig != NULL)
		{
			ant = ant->sig;
		}
		ant->sig = aux;
	}
}
void agrega_elem_pos(pntrNodo * list, pntrNodo nod, int pos)
{
	pntrNodo aux, ANT;
	int cont = 0, dif;
	aux = * list;
	while(aux != NULL)
	{
		cont++;
		aux = aux->sig;
	}
	dif = cont - pos;
	if(dif == 0 || dif == -1 || dif == cont || dif == (cont - 1))
	{
		if(dif == 0)
		{
			aux = * list;
			while(aux->sig != NULL)
			{
				ANT = aux;
				aux = aux->sig;
			}
			ANT->sig = nod;
			nod->sig = aux;
		}
		if(dif == -1)
		{
			aux = * list;
			while(aux != NULL)
			{
				ANT = aux;
				aux = aux->sig;
			}
			ANT->sig = nod;
			nod->sig = NULL;
		}
		if(dif == cont || dif == (cont - 1))
		{
			nod->sig = * list;
			* list = nod;
		}
	}
	else
	{
		cont = 1;
		aux = *list;
		if(aux != NULL)
		{
			while(aux->sig != NULL && cont < pos)
			{
				ANT = aux;
				aux = aux->sig;
				cont++;
			}
			if(aux->sig != NULL)
			{
				ANT->sig = nod;
				nod->sig = aux;
			}
			else
			{
				printf("\n Posicion fuera de rango.");
			}
		}
		else
		{
			printf("\n Lista vacia.");
		}
	}
}
int check_elem_lista(pntrNodo list, int bus)
{	/* devuelve 1 si el numero ingresado esta en la lista */
	int band = 0;
	while(list != NULL && band == 0)
	{
		if(list->ent == bus)
		{
			band = 1;
		}
		list = list->sig;
	}
	return band;
}
void elimina_ini(pntrNodo * lis)
{
	pntrNodo aux;
	if(*lis == NULL)
	{
		printf("\nLa lista estaba vacia");
	}
	else
	{
		if((*lis)->sig == NULL)
		{
			free(*lis);
			*lis = NULL;
		}
		else
		{
			aux = *lis;
			*lis = (*lis)->sig;
			free(aux);
		}
	}
}
void elimina_fin(pntrNodo * lis)
{
	pntrNodo  aux, ant;
	if(*lis == NULL)
	{
		printf("\nLa lista estaba vacia.");
	}
	else
	{
		if((*lis)->sig == NULL)
		{
			free(*lis);
			*lis = NULL;
		}
		else
		{
			for(aux = (*lis)->sig; aux->sig != NULL; aux = aux->sig)
			{
				ant = aux;
			}
			free(aux);
			ant->sig = NULL;
		}
	}
}
void elimina_frst_aparicion(pntrNodo * list, pntrNodo bus)
{
	pntrNodo aux = *list, anterior;
	if(*list != NULL)
	{
		if(compara_nodos((*list),bus) == 0)
		{	/* cero si son iguales */
			(*list) = (*list)->sig;
			free(aux);
		}
		else
		{
			while(aux != NULL && (compara_nodos(aux,bus) != 0))
			{
				anterior = aux;
				aux = aux->sig;
			}
			if(aux != NULL)
			{
				anterior->sig = aux->sig;
				free(aux);
			}
			else
			{
				printf("\n Elemento no encontrado.");
			}
		}
	}
	else
	{
		printf("\n La lista estaba vacia.");
	}
}
pntrNodo crea_lista_copia(pntrNodo list)
{
	int band = 0;
	pntrNodo nuevo = NULL, aux = NULL, copia = NULL;
	while(list != NULL)	
	{	/* el orden de los ifs es importante */
		if(band > 1)
		{
			nuevo = nuevo_elem(list->ent);
			if(nuevo != NULL)
			{
				aux = copia;
				while(aux->sig != NULL)
				{
					aux = aux->sig;
				}
				aux->sig = nuevo;
			}
			else
			{
				printf("\n Error en asignacion de memoria------");
			}
		}
		if(band == 1)
		{
			nuevo = nuevo_elem(list->ent);
			if(nuevo != NULL)
			{
				copia->sig = nuevo;
				band++;
			}
			else
			{
				printf("\n Error en asignacion de memoria----");
			}
		}
		if(band == 0)
		{
			nuevo = nuevo_elem(list->ent);
			if(nuevo != NULL)
			{
				copia = nuevo;
				band++;
			}
			else
			{
				printf("\n Error en asignacion de memoria--");
			}
		}
		list = list->sig;
	}
	return copia;
}
void muestra_nodo_pos(pntrNodo list, int pos)
{
	int cont = 0;
	while(cont <= pos)
	{
		cont++;
		list = list->sig;
	}
	muestra_elemento(list);
}
void purga_lista(pntrNodo * lis, int prg)
{
	pntrNodo ant = * lis, aux, Aux2;
	if(* lis == NULL)
	{
		printf("\n La lista estaba vacia.");
	}
	else
	{
		if((*lis)->sig == NULL && (*lis)->ent == prg)
		{
			printf("\n lista de 1 elem y se elimina por coincidir.");
			aux = *lis;
			*lis = NULL;
			free(aux);
		}
		else
		{
			pntrNodo aux = (*lis)->sig;
			while(aux != NULL)
			{	/* si hay un solo elemen y no es prg, aux = NULL al inicializrl */
				if(aux->ent != prg)
				{
					ant = aux;
					aux = aux->sig;
				}
				else
				{
					ant->sig = aux->sig;
					Aux2 = aux;
					free(Aux2);
					aux = ant->sig;
				}
			}
			if((*lis)->ent == prg)
			{
				  elimina_ini(lis);
			}
		}
	}
}
void elimina_pos(pntrNodo * lis, int pos)
{
	pntrNodo aux, ANT = NULL, Aux2;
	aux = *lis;
	int cont = 1;
	while((aux != NULL) && (cont != pos))
	{
		ANT = aux;
		aux = aux->sig;
		cont++;
	}
	if(ANT != NULL)
	{
		if(aux == NULL)
		{
			printf("\n posicion no encontrada");
		}
		else
		{
			Aux2 = aux;
			free(Aux2);
			ANT->sig = aux->sig;
		}
	}
	else
	{
		Aux2 = *lis;
		free(Aux2);
		*lis = (*lis)->sig;
	}
}
void elimina_frst_ocurrencia(pntrNodo list, int prg)
{
	int cont = 0;
	pntrNodo anterior;
	while(list != NULL && cont < 2)
	{
		if(list->ent == prg)
			cont++;
		anterior = list;
		list = list->sig;
	}
	if(cont == 2)
		anterior->sig = list->sig;
	else
		printf("\n No se reunen las condiciones necesarias para una eliminacion.");
	if(list == NULL)
		printf("\n Lista vacia.");
}
void elimina_all_ocurrencias(pntrNodo list, int prg)
{
	int cont = 0;
	pntrNodo anterior, Ant2;
	while(list != NULL && cont < 2)
	{
		if(list->ent == prg)
			cont++;
		anterior = list;
		list = list->sig;
	}
	if(cont >= 2)
		while(list != NULL)
		{
			if(list->ent == prg)
			{
				anterior->sig = list->sig;
				Ant2 = list;
				free(Ant2);
			}
			anterior = list;
			list = list->sig;
		}
	else
		printf("\n No hubo eliminaciones.");
}
void une_listas_ini_fin(pntrNodo * lis01, pntrNodo lis02)
{
	if(*lis01 != NULL)
	{
		(*lis01)->sig = lis02;
	}
	else
	{
		*lis01 = lis02;
	}
}
void muestra_dato_x_pos(pntrNodo lista,int pos)
{
	int i;
	for(i = 1; i < pos; i++)
	{
		lista = lista->sig;
	}
	muestra_elemento(lista);
}
void elimina_all(pntrNodo * lista)
{
	pntrNodo aux = * lista, Aux2;
	if(aux != NULL)
	{
		while(aux != NULL)
		{
			Aux2 = aux;
			aux = aux->sig;
			free(Aux2);
		}
	}
	else
	{
		printf("\n La lista estaba vacia.");
	}
}

