#ifndef TAD_ENTEROS_H
#define TAD_ENTEROS_H
#include <stdio.h>
#include <stdlib.h>
typedef struct Nodo
{
	int ent;
	struct Nodo * sig;
}t_nodo;
typedef t_nodo * pntrNodo;
pntrNodo nuevo_elem(int);
void muestra_elemento(pntrNodo);
int compara_nodos(pntrNodo,pntrNodo);
int check_par(pntrNodo);
int check_impar(pntrNodo);
int check_primo(pntrNodo);
int sucesor_elem(pntrNodo);
#endif
