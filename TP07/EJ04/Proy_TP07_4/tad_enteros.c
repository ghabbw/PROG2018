#include "tad_enteros.h"
pntrNodo nuevo_elem(int entrada)
{
	pntrNodo nuevo = NULL;
	nuevo = (t_nodo*) malloc(sizeof(t_nodo));
	if(nuevo != NULL)
	{
		nuevo->ent = entrada;
		nuevo->sig = NULL;
	}
	return nuevo;
}
void muestra_elemento(pntrNodo list)
{
	printf("\n Entero: %d",list->ent);
}
int compara_nodos(pntrNodo list01, pntrNodo list02)
{
	if(list01->ent > list02->ent)
		return 1;
	if(list01->ent < list02->ent)
		return -1;
	return 0;
}
int check_par(pntrNodo list)
{	/* devuelve 1 si es par */
	return (list->ent % 2) + 1;
}
int check_impar(pntrNodo list)
{
	return list->ent % 2;
}
int check_primo(pntrNodo list)
{
	int pd = 2;
	while(pd <= (list->ent / 2) && (list->ent % pd) != 0)
	{
		pd++;
	}
	if(pd > (list->ent / 2) && list->ent != 1	)
		return 1;
	return 0;
}
int sucesor_elem(pntrNodo list)
{
	return list->ent + 1;
}
