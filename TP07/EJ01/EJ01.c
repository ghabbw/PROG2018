/** TP7-EJ1- Un distribuidor de chips de memoria usb registra los siguiente datos de los tres
fabricantes que le proveen chips: IdFabricante, Nombre, Cantidad, Precio Unitario. Crear un
programa que muestre los datos del o de los fabricantes, a quienes se les compró la mayor
cantidad de chips de memoria usb. Para desarrollar el programa se debe utilizar punteros
dinámicos a una estructura de datos registro. **/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define CANTMAX 999
#define CADMAX 100
#define CANTFABR 3
typedef char t_cad[CADMAX];
typedef struct
{
	int ent;
	int dec;
}t_precio;
typedef struct
{
	int IdFabricante;
	t_cad Nombre;
	int Cantidad;
	t_precio PrecioUnitario;
}t_fabricante;
typedef struct Nodo
{
	t_fabricante fbrcnt;
	struct Nodo * sig;
}t_nodo;
typedef t_nodo * puntNodo;
typedef t_fabricante * puntFab;
void leeCad(t_cad,int);
void carga_lista(puntNodo*);
void muestra_nodo(puntNodo);
void muestra_fab(t_fabricante*);
void muestra_lista(puntNodo);
void agrega_fin(puntNodo*,puntNodo);
void muestra_mays(puntNodo,int);
void muestra_fab_may(t_fabricante*,int);
int det_mayorCant(puntNodo);
int det_mayCant(t_fabricante*,int);
t_fabricante * carga_fab(void);
/** VERSION CON PUNTEROS A REGISTRO SIN LISTA **/
int main(void)
{
	srand(time(0));
	int may = 0;
	t_fabricante * fab01 = NULL, * fab02 = NULL, * fab03 = NULL;

	fab01 = carga_fab();
	muestra_fab(fab01);
	fab02 = carga_fab();
	muestra_fab(fab02);
	fab03 = carga_fab();
	muestra_fab(fab03);
	
	may = det_mayCant(fab01,may);
	may = det_mayCant(fab02,may);
	may = det_mayCant(fab03,may);
	
	printf("\nMostrando fabricantes a los que mas se compro chips:\n //////////");
	muestra_fab_may(fab01,may);
	muestra_fab_may(fab02,may);
	muestra_fab_may(fab03,may);
	
	free(fab01);
	free(fab02);
	free(fab03);
	return 0;
}
/**    VERSION CON LISTA ENLAZADA:
int main(void)
{
	puntNodo lista = NULL;
	int may = 0;
	srand(time(0));
	carga_lista(&lista);
	muestra_lista(lista);
	may = det_mayorCant(lista);
	
	muestra_mays(lista,may);
	free(lista);
	lista = NULL;
	return 0;
} **/
void leeCad(t_cad cad, int tam)
{
	int j = 0, m;
	while(j < tam - 1 && (m = getchar())!=EOF && m != '\n')
	{
		cad[j] = m;  /* cada tecla pulsada se guarda en el vector */
		j++;
	}
	cad[j] = '\0';
	if(m != EOF && m != '\n')    /* limpia el buffer */
		while((m = getchar()) != EOF && m != '\n');
}
void carga_lista(puntNodo *list)
{
	int i;
	puntNodo aux = NULL;
	for(i = 1; i <= CANTFABR; i++)
	{
		aux = (t_nodo*) malloc(sizeof(t_nodo));
		if(aux != NULL)
		{
			aux->fbrcnt.Cantidad = rand() % (CANTMAX - 0 + 1) + 0;
			aux->fbrcnt.IdFabricante = rand() % (99 - 1 + 1) + 1;
			aux->fbrcnt.PrecioUnitario.ent = rand() % (9999 - 1 + 1) + 1;
			aux->fbrcnt.PrecioUnitario.dec = rand() % (999 - 0 + 1) + 0;
			printf("\n Ingrese nombre del fabricante:\n");
			leeCad(aux->fbrcnt.Nombre,CADMAX);
			aux->sig = NULL;
			agrega_fin(list,aux);
		}
	}
}
void muestra_nodo(puntNodo list)
{
	printf("\n_________________________");
	printf("\n Nombre del fabricante: %s",list->fbrcnt.Nombre);
	printf("\n# id %d ",list->fbrcnt.IdFabricante);
	printf("\n# Cant %d ",list->fbrcnt.Cantidad);
	printf("\n# Precio $ %d,%d ",list->fbrcnt.PrecioUnitario.ent,list->fbrcnt.PrecioUnitario.dec);
}
void agrega_fin(puntNodo * lis, puntNodo aux)
{
	puntNodo aux2;
	if(*lis == NULL)
		*lis = aux;
	else
	{
		aux2 = *lis;
		while(aux2->sig != NULL)
		{
			aux2 = aux2->sig;
		}
		aux2->sig = aux;
	}
}
void muestra_lista(puntNodo list)
{
	if(list == NULL)
		printf(" \n Lista vacia .....");
	else
	{
		int i = 0;
		while(list != NULL)
		{
			i++;
			printf("\n<%d> Nombre: %s",i,list->fbrcnt.Nombre);
			printf("\nCantidad de chips comprados al fabricante %d ",list->fbrcnt.Cantidad);
			printf("\nid %d ",list->fbrcnt.IdFabricante);
			printf("\nPrecio Unit. %d,%d",list->fbrcnt.PrecioUnitario.ent,list->fbrcnt.PrecioUnitario.dec);
			list = list->sig;
		}
	}
}
int det_mayorCant(puntNodo list)
{
	int may = 0;
	while(list != NULL)
	{
		if(list->fbrcnt.Cantidad > may)
			may = list->fbrcnt.Cantidad;
		list = list->sig;
	}
	return may;
}
void muestra_mays(puntNodo list, int may)
{
	if(list != NULL)
	{
		while(list != NULL)
		{
			if(list->fbrcnt.Cantidad == may)
				muestra_nodo(list);
			list = list->sig;
		}
	}
	else
		printf("\n Lista sin coincidencias");
}
t_fabricante * carga_fab(void)
{
	t_fabricante * fab = NULL;
	fab = malloc(sizeof(t_fabricante));
	
	fab->Cantidad = rand() % (CANTMAX - 0 + 1) + 0;
	fab->IdFabricante = rand() % (99 - 1 + 1) + 1;
	fab->PrecioUnitario.ent = rand() % (9999 - 1 + 1) + 1;
	fab->PrecioUnitario.dec = rand() % (999 - 0 + 1) + 0;
	printf("\n Ingrese nombre del fabricante:\n");
	leeCad(fab->Nombre,CADMAX);
	return fab;
}
void muestra_fab(t_fabricante * fab)
{
	printf("\n------------------");
	printf("\n Nombre del fab: %s",fab->Nombre);
	printf("\n Cantidad: %d",fab->Cantidad);
	printf("\n Id del fabricante: %d",fab->IdFabricante);
	printf("\n Precio unitario: %d,%d",fab->PrecioUnitario.ent,fab->PrecioUnitario.dec);
}
int det_mayCant(t_fabricante * fab, int may)
{
	if(fab->Cantidad > may)
		may = fab->Cantidad;
	return may;
}
void muestra_fab_may(t_fabricante * fab, int may)
{
	if(fab->Cantidad == may)
		muestra_fab(fab);
}
