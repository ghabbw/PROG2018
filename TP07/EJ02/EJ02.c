/** TP7-EJ2- Crear un programa que permita a trav�s de un men� trabajar con una lista de
n�meros enteros realizando las siguientes operaciones:
a) Agregar elementos a la lista
b) Eliminar el primer n�mero par
c) Calcular el promedio de los n�meros pares
d) Insertar despu�s del primer n�mero primo, el promedio de los n�meros pares
e) Mostrar la lista **/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MULTPL 2
typedef struct Nodo
{
	int ent;
	struct Nodo * sig;
}t_nodo;
typedef t_nodo * puntNodo;
void carga_lista(puntNodo*);
void muestra_nodo(puntNodo);
void muestra_lista(puntNodo);
void agrega_fin(puntNodo*,puntNodo);
void muestra_mays(puntNodo,int);
void agregarListaFin(puntNodo*);
void purga(puntNodo*,int);
void elimina_ini(puntNodo*);
void ins_prom_aftr_primo(puntNodo,int);
int det_mayorCant(puntNodo);
int menu(void);
int obt_first_par(puntNodo);
int check_multpl(int,int);
int check_primo(int);
float prom_pares(puntNodo);
puntNodo crearNodo(int);
int main(void)
{
	puntNodo lista = NULL;
	int op, elim;
	float prom = 0;
	do
	{
		op = menu();
		switch(op)
		{
		case 1:
			agregarListaFin(&lista);
			break;
		case 2:
			elim = obt_first_par(lista);
			if(check_multpl(elim,MULTPL))
				purga(&lista,elim);
			break;
		case 3:
			prom = prom_pares(lista);
			printf("\n El promedio de numeros enteros pares es %.2f\n",prom);
			break;
		case 4:
			ins_prom_aftr_primo(lista,MULTPL);
			break;
		case 5:
			muestra_lista(lista);
			break;
		case 0:
			printf("\n  Fin del Programa\n");
		default:
			printf(" Opcion Invalida");
		}
	}while(op != 0);
	free(lista);
	lista = NULL;
	return 0;
}
int menu(void)
{
	int op;
	printf("\n Ingrese opcion:");
	printf("\n 1- Agregar entero");
	printf("\n 2- Eliminar el primer par");
	printf("\n 3- Calcular promedio de los pares");
	printf("\n 4- Insertar despues del primer primo el prom de los pares");
	printf("\n 5- Mostrar Lista");
	printf("\n 0-SALIR------\n ");
	fflush(stdin);
	scanf("%d",&op);
	return op;
}
void agregarListaFin(puntNodo * list)
{
	int entrada;
	puntNodo nuevo,aux;
	printf("\n   Ingrese dato:");
	fflush(stdin);
	scanf("%d",&entrada);
	if(*list == NULL)
	{
		printf(",,,,, la lista estaba vacia");
		*list = crearNodo(entrada);
	}
	else
	{
		nuevo = crearNodo(entrada);
		for(aux=*list; aux->sig != NULL; aux = aux->sig);
		aux->sig = nuevo;
	}
}
void purga(puntNodo *lis, int prg)
{
	puntNodo  ant = *lis, aux = (*lis)->sig;
	if((*lis) == NULL)
		printf("\n La lista estaba vacia");
	else
	{
		if((*lis)->sig == NULL && (*lis)->ent == prg)
		{
			printf("\n lista de 1 elem y se elimina");
			*lis = NULL;
		}
		else
		{
			while(aux != NULL)    
			{	/* si hay un solo elemen y no es prg, aux = NULL al inicializrl */
				if(aux->ent != prg)
				{
					ant = aux;
					aux = aux->sig;
				}
				else
				{
					ant->sig = aux->sig;
					aux = ant->sig;
				}
			}
			if((*lis)->ent == prg)
				  elimina_ini(lis);
		}
	}
}
int obt_first_par(puntNodo lis)
{
	int check = 1, band = 0;
	while(lis != NULL && band == 0)
	{
		if(check_multpl(lis->ent,MULTPL))
		{
			check = lis->ent;
			band = 1;
		}
		lis = lis->sig;
	}
	return check;
}
int check_multpl(int dat, int multpl)
{
	if(dat % multpl == 0)
		return 1;
	return 0;
}
puntNodo crearNodo(int entrada)
{
	puntNodo nuevo = NULL;
	nuevo = malloc(sizeof(t_nodo));
	if(nuevo != NULL)
	{
		nuevo->ent = entrada;
		nuevo->sig = NULL;
	}
	return nuevo;
}
void elimina_ini(puntNodo *lis)
{
	if(*lis == NULL)
		printf("\nLa lista estaba vacia");
	else
	{
		if((*lis)->sig == NULL)
			*lis = NULL;
		else
			*lis = (*lis)->sig;
	}
}
float prom_pares(puntNodo list)
{
	int acum = 0, cont = 0;
	if(list == NULL)
		printf(" \n Lista vacia .....");
	else
	{
		while(list != NULL)
		{
			if(list->ent % 2 == 0)
			{
				acum = acum + list->ent;
				cont++;
			}
			list = list->sig;
		}
	}
	if(acum == 0)
		return 0;
	return (float) acum/cont;
}
void muestra_lista(puntNodo list)
{
	if(list == NULL)
		printf(" \n Lista vacia .....");
	else
	{
		int i = 0;
		while(list != NULL)
		{
			printf("\n# %d__ dato %d ",++i,list->ent);
			list = list->sig;
		}
	}
}
void ins_prom_aftr_primo(puntNodo lis, int multpl)
{
	int band = 0, cont = 0;
	float acum = 0;
	if(lis == NULL)
		printf(" \n Lista vacia.....");
	else
	{
		puntNodo aux = lis, ant, nuevo;
		while(aux != NULL && band == 0)
		{
			if(check_multpl(aux->ent,multpl) == 1)
			{
				acum = acum + aux->ent;
				cont++;
			}
			if((check_primo(aux->ent)) == 1)
			   band = 1;
			ant = aux;
			aux = aux->sig;
		}
		if(band == 1)
		{
			if(acum == 0)
				printf("\n No habian pares antes del num primo");
			if(acum != 0)
			{
				nuevo = (t_nodo*) malloc(sizeof(t_nodo));
				printf("\n %f # %d # %d",acum,cont,(int) acum / cont);
				nuevo->ent = (int) acum / cont;
				ant->sig = nuevo;
				nuevo->sig = aux;
			}
		}
		else
		   printf("\n lista sin nums primos");
		
	}
}
int check_primo(int pp)
{
	int pd = 2;
	while(pd <= (pp / 2) && (pp % pd) != 0)
		pd++;
	if(pd > (pp / 2) && pp != 1)
		return 1;
	return 0;
}
